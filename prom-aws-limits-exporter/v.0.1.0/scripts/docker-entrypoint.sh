#!/usr/local/bin/dumb-init /bin/bash

if [ -z "$AWS_ACCESS_KEY_ID" ]; then
  echo "Please double check AWS creds."
  echo "AWS_ACCESS_KEY_ID is missing."
  exit 1
fi

if [ -z "$AWS_SECRET_ACCESS_KEY" ]; then
  echo "Please double check AWS creds."
  echo "AWS_SECRET_ACCESS_KEY is missing."
  exit 1
fi

if [ -z "$AWS_DEFAULT_REGION" ]; then
  echo "Please double check AWS creds."
  echo "AWS_DEFAULT_REGION is missing."
  exit 1
fi

python /scripts/prom-metric-serve.py
