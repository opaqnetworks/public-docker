import logging
import time
import os
import requests

from awslimitchecker.checker import AwsLimitChecker
from prometheus_client import start_http_server, Summary, Gauge


REQUEST_TIME = Summary(
                        'update_processing_seconds',
                        'Time spent querying aws for limits'
                    )
gauges = {}
trantab = str.maketrans(' -.()', '_____')


def main():
    """
    Example of metrics:
    # HELP ec2_running_on_demand_t2_medium_instances
    # TYPE ec2_running_on_demand_t2_medium_instances gauge
    ec2_running_on_demand_t2_medium_instances{region="us-east-1",type="current"} 26.0

    Metrics are collected for the following aws resources:
    apigateway_*
    autoscaling_*
    cloudformation_*
    directory_*
    dynamodb_*
    ebs_*
    ec2_*
    ecs_*
    efs_*
    elasticache_*
    elasticbeanstalk_*
    elb_*
    firehose_*
    iam_*
    rds_*
    redshift_*
    s3_*
    ses_*
    vpc_*

    NOTE: We don't export the "limit" values because those are defaults
          and we don't use defaults.
    """
    port = int(os.environ.get('ALC_PORT', '8080'))
    interval = int(os.environ.get('ALC_INTERVAL', '60'))
    region = os.environ.get('AWS_DEFAULT_REGION')
    c4_2xlarge_limit = int(os.environ.get('C4_2XLARGE_LIMIT', '20'))
    spot_limit = int(os.environ.get('SPOT_LIMIT', '50'))
    ebs_limit = int(os.environ.get('EBS_LIMIT', '102400'))
    logger = logging.getLogger()
    logger.setLevel(logging.ERROR)
    checker = AwsLimitChecker(region=region)

    start_http_server(port)
    # Creating custom metrics
    ec2_c4_2x_large_usage_in_percent_metric = create_custom_metric("ec2_c4_2x_large_usage_in_percent", "Current usage in percent for EC2 c4_2x_large")
    ec2_spot_usage_in_percent_metric = create_custom_metric("ec2_spot_usage_in_percent", "Current usage in percent for EC2 spot instances")
    ebs_usage_in_percent_metric = create_custom_metric("ebs_usage_in_percent", "Current usage in percent for EBS GP2")
    while True:
        update(checker, region)
        set_custom_metric_value(region, ec2_c4_2x_large_usage_in_percent_metric, "ec2_running_on_demand_c4_2xlarge_instances", c4_2xlarge_limit)
        set_custom_metric_value(region, ec2_spot_usage_in_percent_metric, "ec2_max_spot_instance_requests_per_region", spot_limit)
        set_custom_metric_value(region, ebs_usage_in_percent_metric, "ebs_general_purpose__ssd__volume_storage__gib", ebs_limit)
        time.sleep(interval)


@REQUEST_TIME.time()
def update(checker, region):
    try:
        checker.find_usage()
        for service, svc_limits in sorted(checker.get_limits().items()):
            for limit_name, limit in sorted(svc_limits.items()):
                path = '.'.join([service, limit_name])
                usage = limit.get_current_usage()
                update_service(path, usage, region)
    except Exception:
        logging.exception("message")


def update_service(path, usage, region):
    g = gauge(path)
    for resource in usage:
        metric_type = 'current'
        if resource.resource_id:
            metric_type = resource.resource_id
        g.labels(region=region, type=metric_type).set(resource.get_value())


def current_usage_per_metric_name(metric_name):
    awslimitchecker_url_local = "http://localhost:8080"
    metrics = requests.get(awslimitchecker_url_local).text
    for line in metrics.split("\n"):
        if metric_name in line and line[0] != "#":
            value = int(float(line.split(" ")[-1]))
            return value


def current_usage_in_percent(metric_name, limit):
    usage_in_percent = (current_usage_per_metric_name(metric_name) / limit)
    value = usage_in_percent * 100
    return value


def create_custom_metric(metric_name, metric_description):
    return Gauge(metric_name, metric_description, ['region'])


def set_custom_metric_value(region, var_name, pattern, limit_name):
    var_name.labels(region).set(current_usage_in_percent(pattern, limit_name))


def gauge(path):
    path = path.lower().translate(trantab)
    g = gauges.get(path, None)
    if g is None:
        g = Gauge(path, '', ['region', 'type'])
        gauges[path] = g
    return g


if __name__ == "__main__":
    main()
