FROM quay.io/opaqnetworks/alpine:3.5-df5132f

LABEL maintainer "FourV Systems Inc <admin@fourv.com>"

LABEL revision "6c6022e"

EXPOSE 9200 9300

ENV ELASTIC_VERSION 5.4.0

ENV PATH /usr/share/elasticsearch/bin:$PATH
ENV JAVA_HOME /usr/lib/jvm/java-1.8-openjdk

RUN apk add --no-cache bash openjdk8 ca-certificates wget openssl su-exec && \
    update-ca-certificates

RUN addgroup -g 1000 -S elasticsearch && adduser -S -u 1000 -G elasticsearch -h /usr/share/elasticsearch elasticsearch

WORKDIR /usr/share/elasticsearch

ENV ELASTIC_TARBALL elasticsearch-$ELASTIC_VERSION.tar.gz
ENV ELASTIC_URL https://artifacts.elastic.co/downloads/elasticsearch/$ELASTIC_TARBALL

RUN wget "$ELASTIC_URL" && \
    tar zxf "$ELASTIC_TARBALL" && \
    chown -R elasticsearch:elasticsearch "elasticsearch-$ELASTIC_VERSION" && \
    mv elasticsearch-${ELASTIC_VERSION}/* . && \
    rmdir "elasticsearch-$ELASTIC_VERSION" && \
    rm "elasticsearch-$ELASTIC_VERSION.tar.gz"

COPY elasticsearch.yml config/
COPY log4j2.properties config/
COPY entrypoint.sh bin/entrypoint.sh

RUN chown elasticsearch:elasticsearch . config/elasticsearch.yml config/log4j2.properties bin/entrypoint.sh && \
    chmod 0750 bin/entrypoint.sh

RUN set -ex && for esdirs in config data logs; do \
        mkdir -p "$esdirs"; \
        chown -R elasticsearch:elasticsearch "$esdirs"; \
    done

CMD ["/bin/bash", "bin/entrypoint.sh"]
