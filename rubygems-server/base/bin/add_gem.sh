#!/bin/bash

MANIFEST=$1
if [ -z "$MANIFEST" ]; then
  MANIFEST="/tmp/MANIFEST"
fi

echo "Reading manifest $MANIFEST"

while read -r line || [[ -n "$line" ]]; do
  echo "Installing ${line} ... "
  gem install ${line} --no-rdoc --no-ri;
done < ${MANIFEST}
