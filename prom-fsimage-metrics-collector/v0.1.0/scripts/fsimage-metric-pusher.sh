#!/bin/bash
K8S_ENDPOINT="https://$KUBERNETES_SERVICE_HOST:$KUBERNETES_PORT_443_TCP_PORT"
TOKEN=$(</var/run/secrets/kubernetes.io/serviceaccount/token)
AUTH_HEADER="Authorization: Bearer ${TOKEN}"
CURRENT_TIME_UNIX_TIMESTAMP=$(date +%s%3N)
#CHECKPOINT_PERIOD=$(hdfs getconf -confKey dfs.namenode.checkpoint.period) <- can be used only directly on namenode
: ${CHECKPOINT_PERIOD:=3600}
CHECKPOINT_PERIOD_IN_MICROSEC="${CHECKPOINT_PERIOD}000"

function log(){
  echo "[ $(date "+%Y-%m-%d %H:%M:%S") ] --> $@"
}

function jmx_call(){
  local namespace=${1}
  local filter=${2}
  curl -s hadoop-namenode.${namespace}.svc.cluster.local:50070/jmx?qry=Hadoop:service=NameNode,name=FSNamesystem | jq ''"${filter}"''
}

function get_last_checkpoint_time(){
  local namespace=$1
  jmx_call ${namespace} .beans[].LastCheckpointTime
}

function get_transactions_since_last_checkpoint_time(){
  local namespace=$1
  jmx_call ${namespace} .beans[].TransactionsSinceLastCheckpoint
}

function get_safemode_state(){
  local namespace=$1
  local state=$(curl -s hadoop-namenode.${namespace}.svc.cluster.local:50070/jmx?qry=Hadoop:service=NameNode,name=FSNamesystemState .beans[] | jq '.beans[].FSState' | tr -d '"')
  case $state in
    Operational)
      echo 0
      ;;
    safeMode)
      echo 1
      ;;
  esac
}

function get_batch_data(){
  local namespace=$1
  jmx_call ${namespace} .beans[] | grep -E 'Capacity|Blocks|DataNodes' | tr -d '",:' | sed -e 's/^  //g' | sort
}

function k8s_api_call(){
  local curl_action=$1
  local url=$2
  curl -X ${curl_action} -sSk -H "${AUTH_HEADER}" ${K8S_ENDPOINT}$url
}

function get_svc_for_hadoop_namenode(){
  k8s_api_call GET /api/v1/services | jq -e -r '.items[].metadata | select(.selfLink | contains("hadoop-namenode")) | .namespace'
}

function get_check_interval(){
  local namespace=$1
  local last_chck_point=$(get_last_checkpoint_time $namespace)
  echo $((CHECKPOINT_PERIOD_IN_MICROSEC-(CURRENT_TIME_UNIX_TIMESTAMP-last_chck_point)))
}

function push_metric(){
  local namespace=$1
  local metric_name="$2"
  local metric_type="$3"
  local metric_help_description="$4"
  local value=$5
cat <<EOF | curl -sv --data-binary @- http://pushgateway.monitoring.svc.cluster.local:9091/metrics/jobs/hdfs_fsimage_stats/instances/${namespace}
  # TYPE ${metric_name} ${metric_type}
  # HELP ${metric_name} ${metric_help_description}
  ${metric_name}{namespace="${namespace}",cluster_name="${K8S_CLUSTER_NAME}",exporter_name="${APP_REPORTER}"} ${value}
EOF
}

function push_batch_data(){
  local namespace=$1
  local oldifs=$IFS
  IFS=$'\n'
  for line in $(get_batch_data ${namespace}); do
    local key=$(echo "$line" | cut -f1 -d' ')
    local value=$(echo "$line" | cut -f2 -d' ')
    push_metric "${namespace}" "hdfs_${key}" "counter" "${key}" ${value}
  done
  IFS=$oldifs
}

function push_metric_hdfs_fsimage_status(){
  local namespace=$1
  local value=$2
  push_metric "${namespace}" "hdfs_fsimage_status" "counter" "Returns bool value for fsimage status. 0 - ok, 1 - fsimage is outdated." ${value}
}

function get_number_of_transactions(){
  local namespace=$1
  local transactions=$(get_transactions_since_last_checkpoint_time ${namespace})
  echo "Checking transactions ... "
  if [ ${transactions} -gt 0 ]; then
    push_metric_hdfs_fsimage_status "${namespace}" 1
  else
    push_metric_hdfs_fsimage_status "${namespace}" 0
  fi
}

function check_availability(){
  local namespace=${1}
  local url="hadoop-namenode.${namespace}.svc.cluster.local:50070/jmx"
  curl -sIf ${url}
  if [ $? -ne 0 ]; then
    log "Unable to reach ${url}"
    return 1
  fi
}

for i in $(get_svc_for_hadoop_namenode); do
  namespace=${i}
  log "Checking jmx for hadoop-namenode in namespace: ${namespace}"
  check_availability ${namespace} || continue
  log "OK"
  gtslct=$(get_transactions_since_last_checkpoint_time ${namespace})
  log "Number of new transactions in edits log expecting next checkpoint: ${gtslct}"
  glct=$(get_last_checkpoint_time ${namespace})
  log "Last checkpoint time: ${glct}"
  gci=$(get_check_interval ${namespace})
  log "Countdown before expected next checkpoint: ${gci}"
  log "Start pushing metrics ... "
  log "hdfs_fsimage_expected_checkpoint_in_seconds"
  push_metric "${namespace}" "hdfs_fsimage_expected_checkpoint_in_seconds" "counter" "Countdown before expected next checkpoint" ${gci}
  log "hdfs_fsimage_new_transactions_count"
  push_metric "${namespace}" "hdfs_fsimage_new_transactions_count" "counter" "Number of new transactions in edits log expecting next checkpoint." ${gtslct}
  log "hdfs_fsimage_last_checkpoint_time"
  push_metric "${namespace}" "hdfs_fsimage_last_checkpoint_time" "counter" "Time of the last checkpoint in unix timestamp format" ${glct}
  log "hdfs_fsimage_status"
  if [ $gci -ge 0 ]; then
    push_metric_hdfs_fsimage_status "${namespace}" 0
  else
    get_number_of_transactions "${namespace}"
  fi
  gss=$(get_safemode_state ${namespace})
  log "hdfs_fs_safemode"
  push_metric "${namespace}" "hdfs_fs_safemode" "counter" "0 - means fs is in operational state, meanwhile 1 means safemode." ${gss}
  log "Preparing set of metrics related to Capacity, Blocks info and DataNodes"
  push_batch_data "${namespace}"
done
