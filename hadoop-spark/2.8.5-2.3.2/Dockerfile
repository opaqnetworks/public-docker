FROM quay.io/opaqnetworks/ubuntu:16.04-201801160412 as builder

# This lib is needed to install
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
                       curl="7.47.0-1ubuntu2.9" \
                       gnupg="1.4.20-1ubuntu3.3" \
                       tar="1.28-2.1ubuntu0.1" \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Install Hadoop
RUN curl -fSL https://dist.apache.org/repos/dist/release/hadoop/common/KEYS -o /tmp/KEYS
RUN gpg --import /tmp/KEYS

ENV HADOOP_VERSION 2.8.5
ENV HADOOP_INSTALL_DIR /usr/local
ENV HADOOP_PREFIX=$HADOOP_INSTALL_DIR/hadoop-$HADOOP_VERSION
ENV HADOOP_CONF_DIR=/etc/hadoop
ENV HADOOP_DOC_DIR=/share/doc/

ENV HADOOP_URL "https://www.apache.org/dist/hadoop/common/hadoop-$HADOOP_VERSION/hadoop-$HADOOP_VERSION.tar.gz"
RUN set -x \
    && curl -fSL "$HADOOP_URL" -o /tmp/hadoop.tar.gz \
    && curl -fSL "$HADOOP_URL.asc" -o /tmp/hadoop.tar.gz.asc \
    && gpg --verify /tmp/hadoop.tar.gz.asc \
    && tar -xvf /tmp/hadoop.tar.gz -C "$HADOOP_INSTALL_DIR" \
    && rm -rf "${HADOOP_HOME}/share/doc" \
    && for dir in common hdfs mapreduce tools yarn; do \
         rm -rf "${HADOOP_HOME}/share/hadoop/${dir}/sources"; \
       done \
    && rm -rf "${HADOOP_HOME}/share/hadoop/common/jdiff" \
    && rm -rf "${HADOOP_HOME}/share/hadoop/mapreduce/lib-examples" \
    && rm -rf "${HADOOP_HOME}/share/hadoop/yarn/test" \
    && find "${HADOOP_HOME}/share/hadoop" -name "*test*.jar" -print0 | xargs -0 rm -rf \
    && rm /tmp/hadoop.tar.gz* \
    && chown -R root:root /usr/local/*


FROM quay.io/opaqnetworks/ubuntu:16.04-openjdk8-201802081025
LABEL maintainer "FourV Systems <admin@fourv.com>"
LABEL revision "201810300415"

## NOTE! Any changes to apk installed packages needs to be coordinated with the airflow-1.8.2 and gs-cyber images to ensure
## that the artifacts are compatible
RUN apt-get update \
    && apt-get install -y --no-install-recommends bash="4.3-14ubuntu1.2" \
                                               curl="7.47.0-1ubuntu2.9" \
                                               perl="5.22.1-9ubuntu0.5" \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

ENV HADOOP_VERSION 2.8.5
ENV HADOOP_INSTALL_DIR /usr/local
ENV HADOOP_PREFIX=$HADOOP_INSTALL_DIR/hadoop-$HADOOP_VERSION
ENV HADOOP_CONF_DIR=/etc/hadoop
ENV HADOOP_DOC_DIR=/share/doc/

ENV USER=root
# Install Hadoop
COPY --from=builder "$HADOOP_PREFIX" "$HADOOP_PREFIX"
RUN ln -s "$HADOOP_INSTALL_DIR/hadoop-$HADOOP_VERSION/etc/hadoop" "$HADOOP_CONF_DIR"
RUN ln -s "$HADOOP_INSTALL_DIR/hadoop-$HADOOP_VERSION" "$HADOOP_INSTALL_DIR/hadoop"
RUN cp "$HADOOP_CONF_DIR/mapred-site.xml.template" "$HADOOP_CONF_DIR/mapred-site.xml"
RUN mkdir "$HADOOP_PREFIX/logs"
RUN mkdir /hadoop-data

ENV SPARK_YARN_DYNAMIC_ALLOCATION=1

ENV PATH $HADOOP_PREFIX/bin/:$PATH

# install spark
ENV SPARK_VERSION 2.3.2
ENV SPARK_HADOOP_PROFILE 2.7
ENV AWS_CLI_VERSION 1.16.43

ENV SPARK_BIN_URL "https://www.apache.org/dyn/mirrors/mirrors.cgi?action=download&filename=/spark/spark-$SPARK_VERSION/spark-$SPARK_VERSION-bin-hadoop2.7.tgz"

ENV SPARK_HOME=$HADOOP_INSTALL_DIR/spark

RUN set -x \
    && curl -fSL "$SPARK_BIN_URL" -o /tmp/spark.tar.gz \
    && tar -xvf /tmp/spark.tar.gz -C "$HADOOP_INSTALL_DIR" \
    && mv "$HADOOP_INSTALL_DIR"/spark-"$SPARK_VERSION"-* "$SPARK_HOME" \
    && rm -f /tmp/spark.tar.gz

RUN \
	mkdir -p /aws && \
    apt-get update && \
    apt-get install -y --no-install-recommends \
        groff="1.22.3-7" \
        less="481-2.1" \
        python="2.7.12-1~16.04" \
        python-setuptools="20.7.0-1" \
        python-pip="8.1.1-2ubuntu0.4" && \
    pip install --upgrade pip==9.0.3 && \
	pip install awscli=="$AWS_CLI_VERSION" && \
    apt-get -y autoremove && \
    apt-get -y remove python-pip && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

WORKDIR $SPARK_HOME
ENV PATH $SPARK_HOME/bin:$PATH

RUN mv "$SPARK_HOME/yarn/spark-$SPARK_VERSION-yarn-shuffle.jar" "$HADOOP_INSTALL_DIR/hadoop/share/hadoop/yarn/" \
    && mkdir /tmp/spark-events \
    && mkdir -p /usr/local/greyspark-cyber/logs

COPY entrypoint.sh /entrypoint.sh
RUN chmod a+x /entrypoint.sh

COPY airflow-webserver-config.sh /
COPY greyspark-master-config.sh /
COPY greyspark-worker-config.sh /
COPY hadoop-datanode.sh /
COPY hadoop-namenode.sh /
COPY hadoop-secondary-namenode.sh /
COPY hadoop-nodemanager.sh /
COPY hadoop-resourcemanager.sh /
COPY hadoop-timelineserver.sh /
COPY hadoop-liveness-probe.sh /
COPY spark-entrypoint.sh /
COPY spark-historyserver.sh /
COPY spark-master.sh /
COPY spark-slave.sh /

RUN chmod a+x \
    /airflow-webserver-config.sh \
    /greyspark-master-config.sh \
    /greyspark-worker-config.sh \
    /hadoop-datanode.sh \
    /hadoop-namenode.sh \
    /hadoop-secondary-namenode.sh \
    /hadoop-nodemanager.sh \
    /hadoop-resourcemanager.sh \
    /hadoop-timelineserver.sh \
    /hadoop-liveness-probe.sh \
    /spark-entrypoint.sh \
    /spark-historyserver.sh \
    /spark-master.sh \
    /spark-slave.sh

RUN echo "export SPARK_DIST_CLASSPATH=$(hadoop classpath)" >> "$SPARK_HOME/conf/spark-env.sh"

# Force cacerts update.
RUN /scripts/ca_root_certs_updater.sh

ENTRYPOINT ["/spark-entrypoint.sh"]
