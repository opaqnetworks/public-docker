#!/bin/bash

checkpointdir="$(echo $HDFS_CONF_dfs_namenode_checkpoint_dir | perl -pe 's#file://##')"
if [ ! -d checkpointdir ]; then
  echo "Namenode checkpoint directory not found: $checkpointdir. Creating it now."
  mkdir -p $checkpointdir
fi

if [ -z "$CLUSTER_NAME" ]; then
  echo "Cluster name not specified"
  exit 2
fi

if [ "$(ls -A $checkpointdir)" == "" ]; then
  echo "Formatting checkpoint directory: $checkpointdir"
  "$HADOOP_PREFIX/bin/hdfs" secondarynamenode -format
fi

"$HADOOP_PREFIX/bin/hdfs" secondarynamenode
