#!/bin/bash

if hdfs dfsadmin -safemode get > /tmp/out.txt
then
  grep 'Safe mode is ON' /tmp/out.txt && hdfs dfsadmin -safemode leave || echo "Safemode is OFF"
else
  echo "Namenode is unavailable"
  exit 1
fi
