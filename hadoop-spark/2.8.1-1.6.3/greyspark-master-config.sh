#!/bin/bash
hadoop fs -mkdir -p /config
hadoop fs -copyFromLocal -f /tmp/application-config/application.conf /config
#GreySpark-alerting config
cp /tmp/application-config/application.conf $GREYSPARK_HOME/config
# start airflow services
if [ $AIRFLOW_INIT_DB -eq 1 ]; then
  airflow initdb
fi

HDFS_DAGS_LOCATION=/airflow/dags
echo "creating DAGs folder on HDFS: $HDFS_DAGS_LOCATION"
hadoop fs -mkdir -p $HDFS_DAGS_LOCATION

#Restart airflow webserver
kill -9 < "$AIRFLOW__CORE__AIRFLOW_HOME/airflow-webserver.pid"
airflow webserver &
airflow flower &
airflow scheduler -n "$AIRFLOW_SCHEDULER_NUM_RUNS"
