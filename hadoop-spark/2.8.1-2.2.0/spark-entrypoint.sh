#!/usr/bin/dumb-init /bin/bash

for c in $(printenv | perl -sne 'print "$1 " if m/^SPARK_CONF_(.+?)=.*/'); do
    name=$(echo ${c} | perl -pe 's/___/-/g; s/__/_/g; s/_/./g')
    var="SPARK_CONF_${c}"
    value=${!var}
    echo "Setting SPARK property $name=$value"
    echo $name $value >> $SPARK_HOME/conf/spark-defaults.conf
done

TYPE=$1
shift
case $TYPE in
  historyserver)
    exec /entrypoint.sh /spark-historyserver.sh $@
    ;;
  master)
    exec /entrypoint.sh /spark-master.sh $@
    ;;
  datanode)
    exec /entrypoint.sh /hadoop-datanode.sh $@
    ;;
  timelineserver)
    exec /entrypoint.sh /hadoop-timelineserver.sh %@
    ;;
  namenode)
    exec /entrypoint.sh /hadoop-namenode.sh $@
    ;;
  nodemanager)
    exec /entrypoint.sh /hadoop-nodemanager.sh $@
    ;;
  resourcemanager)
    exec /entrypoint.sh /hadoop-resourcemanager.sh $@
    ;;
  slave)
    exec /entrypoint.sh /spark-slave.sh $@
    ;;
  submit)
    exec /entrypoint.sh spark-submit $@
    ;;
  greyspark-master)
    exec /entrypoint.sh /greyspark-master-config.sh $@
    ;;
  greyspark-worker)
    exec /entrypoint.sh /greyspark-worker-config.sh $@
    ;;
  *)
    export CLASSPATH="$(hadoop classpath):${SPARK_HOME}/jars/*"
    exec /entrypoint.sh $TYPE $@
    ;;
esac
