#!/bin/bash
cp /tmp/application-config/HackabilityIndexChain.prop $GREYSPARK_HOME/config/jobs

hadoop fs -mkdir -p /config
hadoop fs -copyFromLocal -f /tmp/application-config/application.conf /config
#GreySpark-alerting config
cp /tmp/application-config/application.conf $GREYSPARK_HOME/config
# start airflow services
if [ "$AIRFLOW_INIT_DB" -eq 1 ]; then
  airflow initdb
fi

#Restart airflow webserver
cat $AIRFLOW__CORE__AIRFLOW_HOME/airflow-webserver.pid | xargs kill -9
airflow webserver &
airflow scheduler