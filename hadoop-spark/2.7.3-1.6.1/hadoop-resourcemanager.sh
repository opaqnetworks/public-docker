#!/bin/bash
#Copy Capacity-schedler configuration from /tmp/capacity-scheduler mapped within config-map
cp /tmp/capacity-config/capacity-scheduler.xml /etc/hadoop/
$HADOOP_PREFIX/sbin/mr-jobhistory-daemon.sh start historyserver &
$HADOOP_PREFIX/bin/yarn --config $HADOOP_CONF_DIR resourcemanager