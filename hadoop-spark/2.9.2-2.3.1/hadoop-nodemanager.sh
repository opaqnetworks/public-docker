#!/bin/bash
#Copy Capacity-schedler configuration from /tmp directory
cp /usr/local/capacity-config/capacity-scheduler.xml /etc/hadoop/
$HADOOP_PREFIX/bin/yarn --config $HADOOP_CONF_DIR nodemanager

YARN_LOGS_DIR=$(grep "yarn.nodemanager.remote-app-log-dir" /etc/hadoop/yarn-site.xml | awk '{gsub("<property><name>yarn.nodemanager.remote-app-log-dir</name><value>", "");print}' | awk '{gsub("</value></property>", "");print}')
hadoop fs -mkdir -p $YARN_LOGS_DIR