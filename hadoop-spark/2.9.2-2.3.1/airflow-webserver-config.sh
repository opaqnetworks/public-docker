#!/bin/bash

#Restart airflow webserver
kill -9 < "$AIRFLOW__CORE__AIRFLOW_HOME/airflow-webserver.pid"
airflow webserver