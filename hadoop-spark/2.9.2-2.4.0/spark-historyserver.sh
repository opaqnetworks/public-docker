#!/bin/sh
# create log directory for Spark history service if not exists
SPARK_LOGS_DIR=$(sed '7!d' /usr/local/spark/conf/spark-defaults.conf | awk '{print $2}')
hadoop fs -mkdir -p $SPARK_LOGS_DIR

spark-class org.apache.spark.deploy.history.HistoryServer