#!/bin/bash

SCRIPT_PATH=/auto-cleaning-job.sh

function add_cron(){
  touch "/var/log/crontasks.log" &&\
  touch crontab.tmp &&\
  echo '*/10 * * * * '${SCRIPT_PATH}' >> /var/log/crontasks.log' >> crontab.tmp &&\
  crontab crontab.tmp &&\
  rm -rf crontab.tmp
  cron
}

function generate_env_file_for_cron() {
  echo "PATH=$PATH" > /etc/.env
  echo "export JAVA_HOME=$JAVA_HOME" >> /etc/.env
  env | grep -E "CORE_CONF_fs.*hdfs" >> /etc/.env
}

#GreySpark-alerting config
cp /tmp/application-config/application.conf $GREYSPARK_HOME/config

if [ -z "$AIRFLOW_LOG_CLEAR_LOOKBACK_DAYS" ]; then
  # default to 7 days
  AIRFLOW_LOG_CLEAR_LOOKBACK_DAYS=7
fi

if [ -z "$AIRFLOW_HOME" ]; then
  # default airflow home
  AIRFLOW_HOME="/usr/local/airflow"
fi

find "${AIRFLOW_HOME}/logs" -type f -mtime +"${AIRFLOW_LOG_CLEAR_LOOKBACK_DAYS}" | xargs rm -rf

if [ -f ${SCRIPT_PATH} ]; then
  generate_env_file_for_cron
  add_cron
fi

# start airflow worker services
airflow worker
