#!/bin/bash

LOG_DIR=/usr/local/airflow/logs
PATTERN='^.+BlockMissingException.+?file\=(.+?)\/partitionDay.+$'
AVRO_LIST=($(grep -rE ${PATTERN} ${LOG_DIR} | awk -F'file=' '{print$2}' | awk -F'/partitionDay=' '{print$1}' | sort | uniq ))
test -f /etc/.env && source /etc/.env

function loginfo(){
  local message=$@
  echo "[$(date +"%F %R:%S")] --> ${message}"
}

function hdfs_fsck_delete(){
  local path=$1
  hdfs fsck -delete ${path}
}

if [ ! ${#AVRO_LIST[@]} -gt 0 ]; then
  loginfo "List is empty. Nothing to do here this time. Exiting ..."
  exit 0
fi

loginfo "Corrupted files have been found."
loginfo "Starting cleaning task ..."

for i in ${AVRO_LIST[@]}; do
  loginfo "$i"
  hdfs fsck -delete $i
done

