#!/bin/sh
# recreate log directory for Spark history service
hadoop fs -rm -r /spark-logs
hadoop fs -mkdir -p /spark-logs
spark-class org.apache.spark.deploy.history.HistoryServer
