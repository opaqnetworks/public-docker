#!/bin/bash
cp /tmp/application-config/HackabilityIndexChain.prop $GREYSPARK_HOME/config/jobs

hadoop fs -mkdir -p /config
hadoop fs -copyFromLocal -f /tmp/application-config/application.conf /config
#GreySpark-alerting config
cp /tmp/application-config/application.conf $GREYSPARK_HOME/config
# start airflow services
if [ $AIRFLOW_INIT_DB -eq 1 ]; then
  airflow initdb
fi

echo "copy cron files"
CRON_LOCATION=/var/spool/cron/crontabs/
cat /tmp/cron-config/root > $CRON_LOCATION/root

HDFS_DAGS_LOCATION=/airflow/dags
echo "creating DAGs folder on HDFS: $HDFS_DAGS_LOCATION"
hadoop fs -mkdir -p $HDFS_DAGS_LOCATION

echo "starting cron daemon"
crond -f &

#Restart airflow webserver
cat $AIRFLOW__CORE__AIRFLOW_HOME/airflow-webserver.pid | xargs kill -9
airflow webserver &
airflow flower &
airflow scheduler -n "$AIRFLOW_SCHEDULER_NUM_RUNS"
