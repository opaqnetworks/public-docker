FROM quay.io/opaqnetworks/hadoop:2.8.0-alpine-4cf0ef5

LABEL maintainer "FourV Systems <admin@fourv.com>"
LABEL revision "3db37a1"

ENV SPARK_VERSION 1.6.3
ENV SPARK_HADOOP_PROFILE 2.7

ENV SPARK_BIN_URL https://s3.amazonaws.com/fourv-artifacts/spark/spark-$SPARK_VERSION-bin-scala-2.11-hadoop2.6.tgz

ENV SPARK_HOME=$HADOOP_INSTALL_DIR/spark

RUN set -x \
    && curl -fSL "$SPARK_BIN_URL" -o /tmp/spark.tar.gz \
    && tar -xvf /tmp/spark.tar.gz -C $HADOOP_INSTALL_DIR \
    && mv $HADOOP_INSTALL_DIR/spark-$SPARK_VERSION-* $SPARK_HOME \
    && rm -f /tmp/spark.tar.gz

RUN \
	mkdir -p /aws && \
	apk -Uuv add groff less python py-pip && \
	pip install awscli && \
	apk --purge -v del py-pip && \
	rm /var/cache/apk/*

WORKDIR $SPARK_HOME
ENV PATH $SPARK_HOME/bin:$PATH

RUN mv $SPARK_HOME/lib/spark-$SPARK_VERSION-yarn-shuffle.jar $HADOOP_INSTALL_DIR/hadoop/share/hadoop/yarn/
RUN mkdir /tmp/spark-events
RUN mkdir -p /usr/local/greyspark-cyber/logs

COPY greyspark-master-config.sh /
COPY greyspark-worker-config.sh /
COPY hadoop-nodemanager.sh /
COPY hadoop-resourcemanager.sh /
COPY spark-entrypoint.sh /
COPY spark-historyserver.sh /
COPY spark-master.sh /
COPY spark-slave.sh /

RUN chmod a+x \
    /greyspark-master-config.sh \
    /greyspark-worker-config.sh \
    /hadoop-nodemanager.sh \
    /hadoop-resourcemanager.sh \
    /spark-entrypoint.sh \
    /spark-historyserver.sh \
    /spark-master.sh \
    /spark-slave.sh

RUN echo "export SPARK_DIST_CLASSPATH=$(hadoop classpath)" >> $SPARK_HOME/conf/spark-env.sh
ENTRYPOINT ["/spark-entrypoint.sh"]
