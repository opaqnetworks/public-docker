#!/bin/bash
cp /tmp/application-config/HackabilityIndexChain.prop $GREYSPARK_HOME/config/jobs

#GreySpark-alerting config
cp /tmp/application-config/application.conf $GREYSPARK_HOME/config

echo "copy cron files"
CRON_LOCATION=/var/spool/cron/crontabs
cat /tmp/cron-config/root  > $CRON_LOCATION/root

echo "starting cron daemon"
crond -f &

# start airflow worker services
airflow worker
