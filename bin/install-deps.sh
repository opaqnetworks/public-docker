#/bin/sh

# remove edge repo since it causes issues.
sed -i "s|http://dl-cdn.alpinelinux.org/alpine/edge/main||g" /etc/apk/repositories

if ! apk add --no-cache curl docker git openssh jq; then
  echo "failed to install some packages"
  which curl && which docker && which git || exit 1
  echo "packages were found, ignoring error"
fi

git fetch --unshallow || git fetch