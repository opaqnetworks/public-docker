#!/usr/bin/env bash

function debug(){
  if [ -n "$DEBUG" ]; then
    echo "DEBUG: $@" >&2
  fi
}

# Allow us to surpress user warning for production builds
function inform_dev(){
  if [ -z "$PRODUCTION" ]; then
    echo $@ >&2
  fi
}


function find_docker_versions(){
  find . -name Dockerfile | xargs -n 1 dirname
}

function exists_in_quay() {
  debug "exists_in_quay $@"
  local image=$(echo $1 | cut -d : -f 1 | sed 's|quay.io/opaqnetworks/||')
  local tag=$(echo $1 | cut -d : -f 2)
  debug "$image $tag"

  local manifest_digest=$(curl -s "https://quay.io/api/v1/repository/opaqnetworks/${image}/tag/?specificTag=${tag}" | jq '.tags[].manifest_digest')

  if [[ -z ${manifest_digest} ]]; then
    debug "tag not found in Quay"
    return 1
  else
    debug "tag found in Quay"
    return 0
  fi

}

function exists_locally() {
  if [ -z "${1}" ]; then
    return 1
  fi

  local images=$(docker images -q "${1}")
  [ -n "${images}" ]
}

function upload_docker() {
  echo "=== Uploading $1"
  cd $1

  local version=$($APP_DIR/bin/version.sh)

  echo "--- $version"
  if exists_locally "${version}"; then
    docker push $version
  else
    echo "--- $version not found, skipping"
  fi
}
