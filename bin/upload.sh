#!/bin/bash

: ${SCAN:=false}
APP_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"
source $APP_DIR/bin/common.sh

function upload_all() {
  local dirs=$(find_docker_versions)

  for path in $dirs; do
    $APP_DIR/bin/upload.sh $APP_DIR/$path
    echo $path
  done
}

if [ -z "$1" ]; then
  upload_all
else
  upload_docker $1
fi
