#!/bin/bash

: ${RETRIES:=1}
: ${PUSH:=false}
APP_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"
source $APP_DIR/bin/common.sh

set -e

function push(){
  if [ "$PUSH" = "true" ]; then
    upload_docker $1
  else
    inform_dev "--- Skipping push"
  fi
}

function build_all() {
  local count=0
  local anyfailures=""
  local dirs=$(find_docker_versions)

  while [ "$count" -lt "$RETRIES" ]; do
    count=$((count+1))
    anyfailures=""
    for path in $dirs; do
      $APP_DIR/bin/build.sh $APP_DIR/$path \
        && push $APP_DIR/$path \
        || anyfailures="$anyfailures $path"
    done

    if [ -n "$anyfailures" ]; then
      echo "=== The following pods failed: $anyfailures"
    else
      break
    fi
  done

  [ -z "$anyfailures" ]
  return $?
}

function build_docker() {
  debug "build_docker $@"
  cd $1

  local version=$($APP_DIR/bin/version.sh)
  debug "$version"

  if [ -z "$FORCE_BUILD" ] && exists_in_quay "$version" ; then
    echo "===== Skipping $version, already built ====="
    inform_dev "use FORCE_BUILD=true to force a build"
    return 0
  fi

  if [ -n "$PRODUCTION" -a -f "WIP" ]; then
    return 0
  fi

  echo "===== Building $version ====="
  docker build -t ${version} . || exit 1
  echo "--- Built $version"
}

if [ -z "$1" ]; then
  build_all
else
  build_docker $1
fi
