#!/bin/bash

APP_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"
source $APP_DIR/bin/common.sh
: ${UPLOAD_REPO:="quay.io/opaqnetworks"}
# Version is based on the path of the docker image
#  public-docker/<image>/<version>

CURR_DIR=$(pwd)
if [ -n "$1" ]; then
  CURR_DIR="$1"
fi

debug "version.sh $@"
debug "app dir: $APP_DIR"
debug "current dir: $CURR_DIR"

function revision_qualifier(){
  cd $CURR_DIR
  if [ ! -f Dockerfile ]; then
    return 0
  fi

  grep "revision" Dockerfile | head -n 1 | awk '{print $NF}' | tr -d '"'
}
function git_qualifier(){
  cd $APP_DIR
  git log --oneline -- "$CURR_DIR" | head -1 | awk '{print $1}'
}


if [ -n "$2" ]; then
  qualifier="$2"
else
  revq=$(revision_qualifier)
  gitq=$(git_qualifier)

  # Pick the first of the revision or the git hash
  : ${qualifier:=$revq}
  : ${qualifier:=$gitq}
fi


debug "qualifier: $qualifier"

PARENT="$(cd "${CURR_DIR}/.." && pwd)"

version=$(basename "$CURR_DIR")
image=$(basename "$PARENT")


if [ -z "$PRODUCTION" ]; then
  inform_dev "Non-production, adding $(id -u -n) to qualifier"
  # this code is not yet ready to production
  qualifier="${qualifier}-$(id -u -n)"
  
  # non-prod images go to dev-test
  echo "$UPLOAD_REPO/dev-test:$image-$version-${qualifier}"
else
  echo "$UPLOAD_REPO/$image:$version-${qualifier}"
fi
