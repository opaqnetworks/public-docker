package main

import (
	"os"
	"io"
	"net/http"
)

func indexHandler(res http.ResponseWriter, req *http.Request) {
	io.WriteString(res, "Hello from Fourv Health Checker!")
}

func healthHandler(res http.ResponseWriter, req *http.Request) {
	ok := true
	errMsg := ""

	_, err := http.Get(os.Getenv("NAMENODE_URL"))
	if err != nil {
		ok = false
		errMsg += "No connection to HDFS...\n"
	}

	_, err1 := http.Get(os.Getenv("RM_URL"))
	if err1 != nil {
		ok = false
		errMsg += "No connection to YARN...\n"
	}

	_, err2 := http.Get(os.Getenv("SPARK_URL"))
	if err2 != nil {
		ok = false
		errMsg += "No connection to Spark History Service...\n"
	}

	if ok {
		res.Write([]byte("Greyspark product is healthy"))
	} else {
		http.Error(res, errMsg, http.StatusServiceUnavailable)
	}

}

func readinessHandler(res http.ResponseWriter, req *http.Request) {
	ok := true	
	errMsg := ""	
	_, err := http.Get(os.Getenv("NAMENODE_URL"))
	if err != nil {
	    ok = false
	    errMsg += "No connection to HDFS...\n"
	}
	
	_, err1 := http.Get(os.Getenv("RM_URL"))
	if err1 != nil {
	    ok = false
	    errMsg += "No connection to YARN...\n"
	}

	_, err2 := http.Get(os.Getenv("SPARK_URL"))
	if err2 != nil {
		ok = false
		errMsg += "No connection to Spark History Service...\n"
	}
	
	if ok {
  	  res.Write([]byte("Greyspark product is ready"))
  	} else {
	 http.Error(res, errMsg, http.StatusServiceUnavailable)
       }
	
}

func main() {
	http.HandleFunc("/index", indexHandler)
	http.HandleFunc("/healthz", healthHandler)
	http.HandleFunc("/readiness", readinessHandler)
	http.ListenAndServe(":8080", nil)
}
