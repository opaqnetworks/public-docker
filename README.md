# Public Docker

[![Codacy Badge](https://codacy.opaq.com/project/badge/Grade/e217994d3b004904abadac7fb6eb90b3)](https://codacy.opaq.com/bb/opaqnetworks/public-docker?utm_source=codacy-automation@bitbucket.org&amp;utm_medium=referral&amp;utm_content=opaqnetworks/public-docker&amp;utm_campaign=Badge_Grade)

Opaq's repo of public docker containers.  It contains directories of
docker container definitions.

## Directory Structure

Docker uses an intelligent name made of parts:
`[<repo>/]<name>[:<version>]`.  The repo will always be Opaq's
Quay.io account `quay.io/opaqnetworks`.  Name is the base name of the image like
elasticsearch, kibana, etc...  Version is more complex and explained
below.  Everything is mapped to a directory:

``` bash
 - .
 +- elasticsearch
  +- 2.3.4 <- Builds quay.io/opaqnetworks/elasticsearch:2.3.4-...
  +- 5.0   <- Builds quay.io/opaqnetworks/elasticsearch:5.0-...
 +- kibana
  +- 4.3.2 <- Builds quay.io/opaqnetworks/kibana:4.3.2-...
  +- 5.0   <- Builds quay.io/opaqnetworks/kibana:5.0-...
```

## Version

A version is always a base version (usually taken from the base image)
and a qualifier which is based on build or label (see Revisions).

If the docker images is a base image (build entirely internally) then
the version is based on the roadmap of features, or the version of
major components.  For example: quay.io/opaqnetworks/gradle will be versioned 
after the java version and the gradle version.

If the docker image is uses an existing docker container as a base
then the version should match the version of the base image, and the
qualifier will cover any corrections that need to be made by Opaq.

### Revisions

In addition to the base version number (taken from the directory) each container will also have a revision of the build which is taken in one of two ways:

1. Adding `LABEL revision "number"` to the top of Dockerfile
2. The git commit hash of the directory

If the "revision" label is used then "number" is used blindly.  Ideal "number" would be the date of the revision in YYYYMMDDHHMM format (UTC time).  It should be near the top of the Dockerfile so that it can be used to completely rebuild the docker image even if none of the content changes (i.e., as a cache buster).

## Install and Build

You will need docker and bash locally.

Run `bin/build.sh` to build all docker images.  `bin/build.sh
<directory>` to build only that version.

Run `bin/upload.sh` to copy all docker images to FourV's DockerHub
account. `bin/upload.sh <directory>` will only upload that verion.

To upload you will need a quay.io account and be added to the
Opaq org.  `docker login quay.io` will associate your daemon with Quay.

## Work In Progress (WIP)

Sometimes it is useful to add and merge in files that aren't yet ready for production.  To allow for this add a `WIP` file next to the `Dockerfile` and the builder will skip building it.

For example:

``` bash
 - .
 +- elasticsearch
  +- 2.3.4
   +- Dockerfile
  +- 5.0
   +- Dockerfile
   +- WIP
```

`elasticsearch:5.0` in this case would be a WIP image and wouldn't be built in production.

# General Notes

In general, don't use ENTRYPOINT anymore. Use CMD so that the dumb-init process is the first process executed by docker.