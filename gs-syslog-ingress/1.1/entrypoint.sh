#!/usr/bin/dumb-init /bin/bash

SSL_DIR=/etc/syslog-ng/ssl
KEY_FILE=${SSL_DIR}/tls.key
CRT_FILE=${SSL_DIR}/tls.crt
SYSLOG_NG_CONF="${SYSLOG_NG_CONF:-/etc/syslog-ng/syslog-ng.conf}"
DISK_BUFFER_DIR="/data/disk-buffer"
CRT_FILE_HASH="${DISK_BUFFER_DIR}/.tls_crt_hash"
KEY_FILE_HASH="${DISK_BUFFER_DIR}/.tls_key_hash"

function gen_self_signed_cert_and_tls_secret(){
  local common_name=$1
  local key=$2
  local cert=$3
  openssl req -subj '/CN='"${common_name}"'/O=Opaq Unknown Cert./C=US' \
    -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout ${key} -out ${cert}
}

function get_public_ip(){
  wget -qO- ipecho.net/plain | tr -d '/n'
}

ERROR_COUNT=0
if [ -f "${KEY_FILE}" ]; then
  echo "${KEY_FILE} has been found"
else
  echo "Unable to find ssl private key file."
  ERROR_COUNT=$((ERROR_COUNT + 1))
fi

if [ -f "${CRT_FILE}" ]; then
  echo "${CRT_FILE} has been found"
else
  echo "Unable to find ssl cert file."
  ERROR_COUNT=$((ERROR_COUNT + 1))
fi

case ${ERROR_COUNT} in
  0) echo "Using found certificate and key files."
  ;;
  1) echo "Please double check mounted SSL files or remove them to allow container generate self-signed certificate."
     exit 1
  ;;
  2) echo "Start generating key and certifiacate ... "
     gen_self_signed_cert_and_tls_secret $(get_public_ip) ${KEY_FILE} ${CRT_FILE}
  ;;
esac

if [ ! -z "${DEBUG_LOG_FILE_PATH}" ]; then
  tail -F ${DEBUG_LOG_FILE_PATH} &
fi

/usr/sbin/syslog-ng -F -f ${SYSLOG_NG_CONF} -e --no-caps
