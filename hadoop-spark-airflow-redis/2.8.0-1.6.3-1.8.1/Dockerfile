FROM quay.io/opaqnetworks/hadoop-spark:2.8.0-1.6.3-3db37a1
LABEL maintainer "FourV Systems <admin@fourv.com>"
LABEL revision "0ddc0fa"

EXPOSE 8080 5555 8793

ENV AIRFLOW_VERSION 1.8.1
ENV NUMPY_VERSION 1.12.0
ENV PANDAS_VERSION 0.16.2
ENV CELERY_VERSION 3.1.25

RUN apk add --no-cache \
        python \
        py-pip \
        py-psycopg2 \
    && apk add -t build --no-cache \
           build-base \
           ca-certificates \
           musl \
           linux-headers \
           libxml2-dev \
           libxslt-dev \
           python-dev \
           postgresql-dev \
    && ln -s /usr/include/locale.h /usr/include/xlocale.h \
    && pip install --upgrade pip \
    && pip install numpy==$NUMPY_VERSION \
    && pip install pandas==$PANDAS_VERSION \
    && pip install apache-airflow[postgres]==$AIRFLOW_VERSION \
    && pip install celery[redis]==$CELERY_VERSION \
    && pip install flower \
    && apk del build

ENV AIRFLOW_HOME "$HADOOP_INSTALL_DIR/airflow"

ENV AIRFLOW_REST_VERSION 1.0.2
ENV AIRFLOW_REST_API_URL "https://github.com/teamclairvoyant/airflow-rest-api-plugin/archive/v${AIRFLOW_REST_VERSION}.tar.gz"
ENV AIRFLOW_REST_API_PLUGIN_NAME "airflow-rest-api-plugin-${AIRFLOW_REST_VERSION}"

# Airflow uses a specific ENV naming convention, which we follow
# https://airflow.incubator.apache.org/configuration.html
ENV AIRFLOW__CORE__DAGS_FOLDER "$AIRFLOW_HOME/dags"
ENV AIRFLOW__CORE__PLUGINS_FOLDER "$AIRFLOW_HOME/plugins"

RUN mkdir -p \
    "$AIRFLOW_HOME" \
    "$AIRFLOW__CORE__PLUGINS_FOLDER" \
    "$AIRFLOW__CORE__DAGS_FOLDER"

RUN set -x \
    && curl -fSL "$AIRFLOW_REST_API_URL" -o /tmp/api.tar.gz \
    && tar -xzvf /tmp/api.tar.gz -C /tmp \
    && ls -la /tmp \
    && ls -la "/tmp/$AIRFLOW_REST_API_PLUGIN_NAME" \
    && ls -la "/tmp/$AIRFLOW_REST_API_PLUGIN_NAME/plugins" \
    && ls -la "$AIRFLOW__CORE__PLUGINS_FOLDER" \
    && mv "/tmp/$AIRFLOW_REST_API_PLUGIN_NAME/plugins"/* "$AIRFLOW__CORE__PLUGINS_FOLDER" \
    && rm -f /tmp/api.tar.gz \
    && rm -rf "/tmp/$AIRFLOW_REST_API_PLUGIN_NAME"
