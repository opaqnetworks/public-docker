FROM alpine:3.6
LABEL maintainer "Opaq <admin@opaq.com>"
LABEL revision "202001201108"

ARG ARCH=linux-amd64
ARG VERSION=0.18.1

ENV SCRIPT_PATH /scripts
ENV PATH $SCRIPT_PATH/bin:$PATH
ENV TEXTFILES_PATH /textfiles

RUN apk --no-cache add --update --virtual .build-dependencies wget ca-certificates

RUN apk add --no-cache --update libc6-compat bash curl jq dumb-init \
    && mkdir -p /tmp/install \
    && wget -O /tmp/install/node_exporter.tar.gz "https://github.com/prometheus/node_exporter/releases/download/v$VERSION/node_exporter-$VERSION.$ARCH.tar.gz" \
    && cd /tmp/install \
    && tar --strip-components=1 -xzf node_exporter.tar.gz \
    && mv node_exporter /bin/node_exporter \
    && rm -rf /tmp/install \
    && apk del .build-dependencies

RUN mkdir "$SCRIPT_PATH" "$TEXTFILES_PATH"
COPY scripts $SCRIPT_PATH

RUN touch crontab.tmp &&\
    echo '*/5 * * * * $SCRIPT_PATH/disk_usage_and_inodes_metrics_generator.sh >> /var/log/crontasks.log' >> crontab.tmp &&\
    crontab crontab.tmp &&\
    rm -rf crontab.tmp

EXPOSE 9100

ENTRYPOINT ["/scripts/docker-entrypoint.sh"]
