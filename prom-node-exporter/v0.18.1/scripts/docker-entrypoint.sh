#!/usr/bin/dumb-init /bin/bash

touch /var/spool/cron/crontabs/root
ln -s /dev/stdout /var/log/crontasks.log
crond -f -d0 &
/bin/node_exporter \
  --collector.textfile.directory=/textfiles \
  --no-collector.arp \
  --no-collector.bcache \
  --no-collector.bonding \
  --no-collector.conntrack \
  --no-collector.cpu \
  --no-collector.cpufreq \
  --no-collector.diskstats \
  --no-collector.edac \
  --no-collector.entropy \
  --no-collector.filefd \
  --no-collector.filesystem \
  --no-collector.hwmon \
  --no-collector.infiniband \
  --no-collector.ipvs \
  --no-collector.loadavg \
  --no-collector.mdadm \
  --no-collector.meminfo \
  --no-collector.netclass \
  --no-collector.netdev \
  --no-collector.netstat \
  --no-collector.nfs \
  --no-collector.nfsd \
  --no-collector.pressure \
  --no-collector.sockstat \
  --no-collector.stat \
  --no-collector.time \
  --no-collector.timex \
  --no-collector.uname \
  --no-collector.vmstat \
  --no-collector.xfs \
  --no-collector.zfs \
  $@
