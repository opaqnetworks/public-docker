#!/bin/bash

CA_CERT="/var/run/secrets/kubernetes.io/serviceaccount/ca.crt"
TOKEN=$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)
HEADER="Authorization: Bearer $TOKEN"
K8S_API_URL=https://$KUBERNETES_SERVICE_HOST/api/v1/nodes
TEXTFILES_DIR=/textfiles
TMP_TEXTFILES_DIR=/tmp_textfiles
DISK_USAGE_METRICS_FILE=disk_usage.prom
DISK_INODES_METRICS_FILE=inodes_usage.prom
GKE_METADATA_URL="http://metadata/computeMetadata/v1"

function log(){
  echo "[$(date)] --> $@"
}

function get_gke_metadata(){
  local url=$1
  curl -s -H "Metadata-Flavor: Google" ${GKE_METADATA_URL}/${url}
}

function api_call(){
  local url=$1
  curl -k -s --cacert "$CA_CERT" --header "$HEADER" -H "Content-Type: application/json" ${K8S_API_URL}/$url
}

function get_nodes(){
  api_call | jq -r '.items[].metadata.name'
}

function get_pvc_meta_data(){
  local node=$1
  api_call $node/proxy/stats/summary | jq -s '[flatten | .[].pods[].volume[]? | select(has("pvcRef")) | '\
'{namespace: .pvcRef.namespace, name: .pvcRef.name, percentageUsed: (.usedBytes / .capacityBytes * 100), '\
'percentageInodesUsed: (.inodesUsed / .inodes * 100) }] | sort_by(.namespace)' | jq -c '. | map([.namespace, .name, .percentageUsed, .percentageInodesUsed])' | jq -c '.[]' | tr -d '["]' | tr ',' ' '
}

function create_metric_file_and_header(){
  local filename=$1
  local metricname=$2
  echo "# HELP ${metricname} metric from node_exporter - shows current ${metricname} in percentage for attached volume." > ${filename}
  echo "# TYPE ${metricname} counter" >> ${filename}
  log "${filename} has been created"
}

function fetch_metrics(){
  for node in ${NODES_LIST[@]}; do
    get_pvc_meta_data $node
  done
}

mkdir -p $TMP_TEXTFILES_DIR
create_metric_file_and_header "${TMP_TEXTFILES_DIR}/${DISK_USAGE_METRICS_FILE}" "disk_usage"
create_metric_file_and_header "${TMP_TEXTFILES_DIR}/${DISK_INODES_METRICS_FILE}" "inodes_usage"

log "Attempting to generate list of nodes ..."
NODES_LIST=($(get_nodes))
if [ ${#NODES_LIST[@]} -ne 0 ]; then
  log "${#NODES_LIST[@]} node(s) have been found!"
else
  log "List of nodes is empty, nothing to do this time. Exiting ..."
  exit 0
fi

PROJECT_ID=$(get_gke_metadata /project/project-id)
CLUSTER_NAME=$(get_gke_metadata /instance/attributes/cluster-name)

fetch_metrics | while read ns n du iu; do
  echo "gke_pv_disk_usage{project_id=\"${PROJECT_ID}\",custer_name=\"${CLUSTER_NAME}\",namespace=\"${ns}\",name=\"${n}\"} $(printf "%.0f" ${du})" >> ${TMP_TEXTFILES_DIR}/${DISK_USAGE_METRICS_FILE};
  log "Disk usage for volume \"${n}\" from namespace \"${ns}\" has been added into ${DISK_USAGE_METRICS_FILE}. Current value: $(printf "%.0f" ${du})"
  echo "gke_pv_inode_usage{project_id=\"${PROJECT_ID}\",custer_name=\"${CLUSTER_NAME}\",namespace=\"${ns}\",name=\"${n}\"} $(printf "%.0f" ${iu})" >> ${TMP_TEXTFILES_DIR}/${DISK_INODES_METRICS_FILE};
  log "Inodes usage for volume \"${n}\" from namespace \"${ns}\" has been added into ${DISK_INODES_METRICS_FILE}. Current value: $(printf "%.0f" ${iu})"
done

mv ${TMP_TEXTFILES_DIR}/* ${TEXTFILES_DIR}/ || log "Failed to move metric files"
