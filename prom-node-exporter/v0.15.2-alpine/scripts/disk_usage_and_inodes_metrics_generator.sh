#!/bin/bash
K8S_ENDPOINT="https://$KUBERNETES_SERVICE_HOST:$KUBERNETES_PORT_443_TCP_PORT"
TOKEN=$(</var/run/secrets/kubernetes.io/serviceaccount/token)
AUTH_HEADER="Authorization: Bearer ${TOKEN}"
TEXTFILES_DIR=/textfiles
TMP_TEXTFILES_DIR=/tmp_textfiles
EBS_DISK_USAGE_METRICS_FILE=ebs_disk_usage.prom
EBS_DISK_INODES_METRICS_FILE=ebs_inodes_usage.prom

function log(){
  echo "[$(date)] -- $@"
}

function k8s_api_call(){
  local curl_action=$1
  local url=$2
  curl -X ${curl_action} -sSk -H "${AUTH_HEADER}" ${K8S_ENDPOINT}$url
}

function get_volume_list(){
  df | grep "vol-" | grep -v "rkt" | tr -s ' ' | cut -f1,6 -d ' ' | tr ' ' '_'
}

function create_metric_file_and_header(){
  local filename=$1
  echo "# HELP ebs_disk_usage metric from node_exporter - shows current disk usage in percentage for attached ebs volume." > ${filename}
  echo "# TYPE ebs_disk_usage counter" >> ${filename}
  log "${filename} has been created"
}

function get_pv_info_by_volume_id(){
  local aws_region=$1
  local volume_id=$2
  if [ -z ${aws_region} ]; then
    log "\$aws_region is not defined!"
    return 1
  fi
  if [ -z ${volume_id} ]; then
    log "\$volume_id is not defined!"
    return 1
  fi
  if [ $? -eq 0 ]; then
    k8s_api_call GET /api/v1/persistentvolumes/ | jq -r -e '.items[] | select(.spec.awsElasticBlockStore.volumeID == "aws://'"${aws_region}"'/'"${volume_id}"'") | .metadata.name, (.spec.claimRef | .namespace, .name)' | tr '\n' ' '
    if [ ${PIPESTATUS[1]} -ne 0 ]; then
      log "Unable to get info about persistentvolume from k8s API using the following details:"
      log "AWS Region: ${aws_region}"
      log "Volime_ID: ${volume_id}"
      log "Skipping metrics push ... "
      return 1
    fi
  fi
}

function get_pod_name_by_namespace_and_pv_claim_name(){
  local namespace=$1
  local pv_claim_name=$2
  if [ -z ${namespace} ]; then
    log "\$namespace is not defined!"
    return 1
  fi
  if [ -z ${pv_claim_name} ]; then
    log "\$pv_claim_name is not defined!"
    return 1
  fi
  if [ $? -eq 0 ]; then
    k8s_api_call GET /api/v1/namespaces/${namespace}/pods | jq -r -e '.items[] | select(.spec.volumes[].persistentVolumeClaim.claimName=='"\"${pv_claim_name}\""') | .metadata.name'
    if [ $? -ne 0 ]; then
      log "Unable to get info about pod from k8s API using the following details:"
      log "Namespace: ${namespace}"
      log "PVC name: ${pv_claim_name}"
      log "Skipping metrics push ... "
      return 1
    fi
  fi
}

VOLUME_LIST=($(get_volume_list))
if [ ${#VOLUME_LIST[@]} -eq 0 ]; then
  log "VOLUME_LIST is empty!"
  log "Skipping metrics push ... "
  exit 1
else
  log "EBS volumes: ${#VOLUME_LIST[@]}."
fi

mkdir -p $TMP_TEXTFILES_DIR
for filename in ${EBS_DISK_USAGE_METRICS_FILE} ${EBS_DISK_INODES_METRICS_FILE}; do
  create_metric_file_and_header "${TMP_TEXTFILES_DIR}/${filename}"
done

PUBLIC_IP=$(curl -s http://169.254.169.254/latest/meta-data/public-ipv4)

for volume in ${VOLUME_LIST[@]}; do
  mounted_device=$(echo $volume | cut -f1 -d '_')
  raw_ebs_volume_info=$(echo $volume | cut -f2 -d '_')
  disk_usage=$(df -h $raw_ebs_volume_info | awk '{print$5}' | tail -n+2 | tr -d '%')
  inodes_usage=$(df -i $raw_ebs_volume_info | awk '{print$5}' | tail -n+2 | tr -d '%')
  trimmed_ebs_volume_region=$(echo ${raw_ebs_volume_info##*/aws/} | cut -f1 -d '/')
  trimmed_ebs_volume_id=$(echo ${raw_ebs_volume_info##*/aws/} | cut -f2 -d '/')
  raw_pv_info=$(get_pv_info_by_volume_id $trimmed_ebs_volume_region $trimmed_ebs_volume_id || echo 'null')
  if [[ $raw_pv_info != *null ]]; then
    pv_name=$(echo $raw_pv_info | cut -f1 -d ' ')
    namespace=$(echo $raw_pv_info | cut -f2 -d ' ')
    pvc_name=$(echo $raw_pv_info | cut -f3 -d ' ')
    pod_name=$(get_pod_name_by_namespace_and_pv_claim_name $namespace $pvc_name)

    echo "ebs_disk_usage{public_ip=\"${PUBLIC_IP}\",device=\"${mounted_device}\",aws_region=\"${trimmed_ebs_volume_region}\",aws_volume_id=\"${trimmed_ebs_volume_id}\",pv_namespace=\"${namespace}\",pv_name=\"${pv_name}\",pv_pvc_name=\"${pvc_name}\",pv_pod_name=\"${pod_name}\"} ${disk_usage}" >> $TMP_TEXTFILES_DIR/$EBS_DISK_USAGE_METRICS_FILE
    log "Disk usage for \"${mounted_device} - ${trimmed_ebs_volume_id}\" has been added into $EBS_DISK_USAGE_METRICS_FILE"
    echo "ebs_inodes_usage{public_ip=\"${PUBLIC_IP}\",device=\"${mounted_device}\",aws_region=\"${trimmed_ebs_volume_region}\",aws_volume_id=\"${trimmed_ebs_volume_id}\",pv_namespace=\"${namespace}\",pv_name=\"${pv_name}\",pv_pvc_name=\"${pvc_name}\",pv_pod_name=\"${pod_name}\"} ${inodes_usage}" >> $TMP_TEXTFILES_DIR/$EBS_DISK_INODES_METRICS_FILE
    log "Inodes usage for \"${mounted_device} - ${trimmed_ebs_volume_id}\" has been added into $EBS_DISK_INODES_METRICS_FILE"
  fi
done

mv ${TMP_TEXTFILES_DIR}/* ${TEXTFILES_DIR}/ || log "Failed to move metric files"
