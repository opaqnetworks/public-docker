#!/bin/bash

TENANT_DIR_PATH_DEFAULT=/usr/local/data/staging
PLUGINS_DIR_PATH=/usr/libexec/netdata/charts.d
FC_CHART_NAME_DEFAULT="filecount.files"
FC_FILE_NAME_DEFAULT="filecount.chart.sh"
FC_TEMP_TEMPLATE=/tmp/filecount.chart.tmpl

if [ ${PGID+x} ]; then
  echo "Adding user netdata to group with id ${PGID}"
  addgroup -g "${PGID}" -S hostgroup 2>/dev/null
  sed -i "s/${PGID}:$/${PGID}:netdata/g" /etc/group
fi

if [ -f /etc/netdata/health.d/filecount.conf ]; then
  chgrp netdata /etc/netdata/health_alarm_notify.conf
fi

(update-ca-certificates || true)&

if [ -d "${TENANT_DIR_PATH_DEFAULT}/tenant" ]; then
  TENANT_DIR_PATH_DEFAULT="${TENANT_DIR_PATH_DEFAULT}/tenant"
  cd ${TENANT_DIR_PATH_DEFAULT}
  TENANTS_LIST=($(find * -type d))
    if [ ${#TENANTS_LIST[@]} -gt 0 ]; then
      for tenant in ${TENANTS_LIST[@]}; do
        export FC_CHART_NAME="${FC_CHART_NAME_DEFAULT}.${tenant}"
        export TENANT_DIR_PATH="${TENANT_DIR_PATH_DEFAULT}/${tenant}"
        export FC_FILE_NAME="fc${tenant}"
        /usr/bin/envsubst '${TENANT_DIR_PATH} ${FC_CHART_NAME} ${FC_FILE_NAME}' < ${FC_TEMP_TEMPLATE} > ${PLUGINS_DIR_PATH}/${FC_FILE_NAME}.chart.sh
        chmod +x ${PLUGINS_DIR_PATH}/${FC_FILE_NAME}.chart.sh
        unset FC_CHART_NAME TENANT_DIR_PATH FC_FILE_NAME
      done
    fi
  else
    export TENANT_DIR_PATH=$TENANT_DIR_PATH_DEFAULT
    export FC_CHART_NAME=$FC_CHART_NAME_DEFAULT
    export FC_FILE_NAME=${FC_FILE_NAME_DEFAULT}
    /usr/bin/envsubst '${TENANT_DIR_PATH} ${FC_CHART_NAME} ${FC_FILE_NAME}' < ${FC_TEMP_TEMPLATE} > ${PLUGINS_DIR_PATH}/${FC_FILE_NAME}
    chmod +x ${PLUGINS_DIR_PATH}/${FC_FILE_NAME}
fi

exec /usr/sbin/netdata -u netdata -D -s /host -p "${NETDATA_PORT}" "$@"
