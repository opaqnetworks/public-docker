# shellcheck shell=bash
# no need for shebang - this file is loaded from charts.d.plugin
# netdata
# real-time performance and health monitoring, done right!

filecount_target_dir=/usr/local/data/staging
filecount_update_every=5
filecount_priority=60000

filecount_check() {
	require_cmd find || return 1
	require_cmd wc || return 1
	test -d ${filecount_target_dir} || return 1
	return 0
}

filecount_get() {
	find ${filecount_target_dir} -type f | wc -l
}

filecount_create() {
	cat <<EOF
CHART filecount.files '' "Filecount for ${filecount_target_dir}" "file(s)" "${filecount_target_dir}" "${filecount_target_dir}" line "$((filecount_priority + 1))" "$filecount_update_every"
DIMENSION current '' absolute 1 1
DIMENSION warning '' absolute 1 1
DIMENSION critical '' absolute 1 1
EOF
	return 0
}

filecount_update() {
	echo "BEGIN filecount.files $1"
	echo "SET current = $(filecount_get)"
	echo "SET warning = 5"
	echo "SET critical = 10"
	echo "END"
	return 0
}
