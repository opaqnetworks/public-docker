#!/bin/sh

if [ ${PGID+x} ]; then
  echo "Adding user netdata to group with id ${PGID}"
  addgroup -g "${PGID}" -S hostgroup 2>/dev/null
  sed -i "s/${PGID}:$/${PGID}:netdata/g" /etc/group
fi

if [ -f /etc/netdata/health.d/filecount.conf ]; then
  chgrp netdata /etc/netdata/health_alarm_notify.conf
fi

(update-ca-certificates || true)&

exec /usr/sbin/netdata -u netdata -D -s /host -p "${NETDATA_PORT}" "$@"
