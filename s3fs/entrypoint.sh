#!/bin/bash

echo "$AWS_ACCESS_KEY_ID:$AWS_SECRET_ACCESS_KEY" > ~/.s3fs_passwd &&\
  chmod 600 ~/.s3fs_passwd &&\
  s3fs $S3_BUCKET $AIRFLOW__CORE__DAGS_FOLDER -o use_path_request_style -o passwd_file=~/.s3fs_passwd
