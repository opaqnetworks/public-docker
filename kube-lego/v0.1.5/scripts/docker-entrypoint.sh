#!/bin/bash
COUNTER=1
CHECK_DNS_SCRIPT_PATH=/scripts/check_dns.sh

while ! ${CHECK_DNS_SCRIPT_PATH}; do
  echo "[${COUNTER}/50] Will try again in 60s"
  if [ $COUNTER -ge 50 ]; then
    echo "Timeout limit has been exceeded."
    echo "Please ensure that DNS records have been updated properly!"
    echo "Exiting ..."
    exit 1
  fi
  (( COUNTER++ ))
  sleep 60
done

/kube-lego
