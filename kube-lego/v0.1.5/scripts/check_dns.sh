#!/bin/bash
K8S_ENDPOINT="https://$KUBERNETES_SERVICE_HOST:$KUBERNETES_PORT_443_TCP_PORT"
TOKEN=$(</var/run/secrets/kubernetes.io/serviceaccount/token)
AUTH_HEADER="Authorization: Bearer ${TOKEN}"
NAMESPACE=$(</var/run/secrets/kubernetes.io/serviceaccount/namespace)
PUSH_METRICS_SCRIPT_PATH=/scripts/push_metrics.sh

function log(){
  echo "[ $(date "+%Y-%m-%d %H:%M:%S") ] --> $@"
}

function k8s_api_call(){
  local curl_action=$1
  local url=$2
  curl -X ${curl_action} -f -sSk -H "${AUTH_HEADER}" ${K8S_ENDPOINT}$url
}

function get_hosts_from_kube_lego_ingress(){
  local ingress_name=$1
  k8s_api_call GET /apis/extensions/v1beta1/namespaces/${NAMESPACE}/ingresses/${ingress_name} 2>/dev/null | jq -r -e '.spec.rules[].host'
  if [ $? -ne 0 ]; then
    return 2
  fi
}

function array_data(){
  for i in nginx-ingress-central nginx-ingress-greyspark; do
    get_hosts_from_kube_lego_ingress ${i}
    if [ $? -eq 2 ]; then
      echo ''
    fi
  done
}

DOMAIN_LIST=($(array_data))

if [ ${#DOMAIN_LIST[@]} -eq 0 ]; then
  log "Domain list is empty ... "
  exit 1
fi

ANY_FAILED=0
for domain in ${DOMAIN_LIST[@]}; do
  host $domain 1>/dev/null 2>&1
  if [ $? -ne 0 ]; then
    ANY_FAILED=1
    log "failed to resolve ${domain}"
    $PUSH_METRICS_SCRIPT_PATH ${domain} failure
  else
    $PUSH_METRICS_SCRIPT_PATH ${domain} success
  fi
done

exit $ANY_FAILED
