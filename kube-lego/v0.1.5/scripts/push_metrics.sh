#!/bin/bash
NAMESPACE=$(</var/run/secrets/kubernetes.io/serviceaccount/namespace)
DOMAIN=$1
STATUS=$2

function check_vars(){
  if [ -z ${DOMAIN} ];
  then
    echo "DOMAIN is missing!"
    return 1
  fi
  if [ -z ${PGW_URL} ];
  then
    echo "PGW_URL is missing!"
    return 1
  fi
  if [ -z ${PGW_PORT} ];
  then
    echo "PGW_PORT is missing!"
    return 1
  fi
}

function check_pushgw(){
  local host=$(echo ${PGW_URL} | awk -F'//' '{print$2}')
  nslookup ${host} 1>/dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo "PushGateway not found. Skipping Metric push."
    return 1
  fi
}

function define_status(){
  case $STATUS in
    success)
      echo '1 0'
    ;;
    failure)
      echo '0 1'
    ;;
    *)
      echo "STATUS has not been defained."
      return 1
    ;;
  esac
}

function push_metrics(){
  local status_success=$(echo $(define_status) | cut -f1 -d' ')
  local status_failure=$(echo $(define_status) | cut -f2 -d' ')
cat <<EOF | curl -sv --data-binary @- ${PGW_URL}:${PGW_PORT}/metrics/jobs/${NAMESPACE}/instances/${DOMAIN}
# TYPE check_dns_for_letcrt counter
# HELP check_dns_for_letcrt Indicates the status of DNS record resolvability
check_dns_for_letcrt{job="check_dns", instance="${DOMAIN}", namespace="${NAMESPACE}", status="resolved"} ${status_success}
check_dns_for_letcrt{job="check_dns", instance="${DOMAIN}", namespace="${NAMESPACE}", status="failed"} ${status_failure}
EOF
}

check_vars &&\
  check_pushgw &&\
  define_status &&\
  push_metrics
