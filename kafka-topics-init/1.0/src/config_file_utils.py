import json

import os

topics_config_path = os.environ.get('TOPICS_CONFIG_FILE', "/specs/kafka-topics-init.json")


def get_topics_configs() -> list[dict]:
    result = list()
    configs = load_config_file(topics_config_path)
    topic_defaults = configs[0]["topic_defaults"]

    for i in range(1, len(configs)):
        topic_config = configs[i]["topic"]
        last_topic_config = topic_defaults.copy()
        last_topic_config.update(topic_config)

        formatted_configs = replace_str_in_keys(last_topic_config, '_', '.')

        result.append({"topic": (formatted_configs.pop("name")),
                       "partitions": int(formatted_configs.pop("partitions")),
                       "replication.factor": int(formatted_configs.pop('replication.factor')),
                       "configs": formatted_configs,
                       })
    return result


def replace_str_in_keys(input_dict: dict, old: str, new: str):
    return {k.replace(old, new): v for k, v in input_dict.items()}


def load_config_file(file_name: str):
    return json.loads(open(file_name).read())
