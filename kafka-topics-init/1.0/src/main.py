import logging
from typing import NamedTuple

from confluent_kafka.admin import ConfigResource, ResourceType
from confluent_kafka.cimpl import NewTopic, NewPartitions

import config_file_utils
import kafka_api_utils

log = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s,%(msecs)d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                    datefmt='%Y-%m-%d:%H:%M:%S',
                    level=logging.DEBUG)


class ConfigUpdates(NamedTuple):
    topics_to_create: list
    topics_to_alter: list
    topics_to_increase_partitions: list


def get_config_updates() -> ConfigUpdates:
    topics_to_create = list()
    topics_to_alter = list()
    topics_to_increase_partitions = list()

    for topic_configs in config_file_utils.get_topics_configs():
        configs_from_file = topic_configs["configs"]
        name = topic_configs["topic"]
        partitions = topic_configs["partitions"]
        replication_factor = topic_configs["replication.factor"]

        if kafka_api_utils.topic_exists(name):
            if is_topic_altered(configs_from_file, name):
                topics_to_alter.append(ConfigResource(ResourceType.TOPIC, name, set_config=configs_from_file))
            if is_partition_count_changed(partitions, name):
                topics_to_increase_partitions.append(NewPartitions(name, partitions))
            if replication_factor != kafka_api_utils.get_replication_factor(name):
                raise Exception("Changing replication factor is not supported by this job")
        else:
            topics_to_create.append(NewTopic(name, partitions, replication_factor, config=configs_from_file))
    return ConfigUpdates(topics_to_create, topics_to_alter, topics_to_increase_partitions)


def is_topic_altered(configs_to_compare: dict, topic_name: str) -> bool:
    for conf_key, conf_value in configs_to_compare.items():
        configs_from_kafka = kafka_api_utils.get_configs(topic_name)
        if conf_key not in configs_from_kafka.keys() \
                or configs_from_kafka[conf_key] != conf_value:
            return True
    return False


def is_partition_count_changed(partitions_to_compare, name):
    partitions_from_kafka = kafka_api_utils.get_partitions(name)
    if partitions_to_compare > partitions_from_kafka:
        return True
    elif partitions_to_compare < partitions_from_kafka:
        raise Exception("New partition count cannot be less than current value")
    return False


def main():
    config_updates = get_config_updates()
    kafka_api_utils.create_topics(config_updates.topics_to_create)
    kafka_api_utils.alter_topics(config_updates.topics_to_alter)
    kafka_api_utils.increase_partitions(config_updates.topics_to_increase_partitions)


if __name__ == '__main__':
    log.info("Starting job ...")
    main()
    log.info('Finished job')
