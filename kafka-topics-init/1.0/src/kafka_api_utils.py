import logging
import os

from confluent_kafka.admin import AdminClient, ConfigResource, ResourceType

client = AdminClient({'bootstrap.servers': os.environ.get('KAFKA_BOOTSTRAP_SERVERS',
                                                          'cp-kafka-headless:9092')})

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)


def topic_exists(name: str) -> bool:
    return name in list_topics()


def list_topics() -> set[str]:
    return set(client.list_topics().topics)


def get_configs(topic: str):
    response = client.describe_configs([ConfigResource(ResourceType.TOPIC, topic)])
    return {k: v.value for k, v in list(response.values())[0].result().items()}


def get_partitions(topic_name) -> int:
    return len(client.list_topics().topics[topic_name].partitions)


def get_replication_factor(topic_name) -> int:
    return len(client.list_topics().topics[topic_name].partitions[0].replicas)


def alter_topics(configs: list) -> None:
    if not configs:
        log.debug("No topics to alter.")
        return

    res = client.alter_configs(configs)
    for k, v in res.items():
        v.result()

    for config in configs:
        log.info('Topic altered [name={} configs={}]'.format(config.name, config.set_config_dict))


def create_topics(configs: list) -> None:
    if not configs:
        log.debug("No topics to create.")
        return

    req = client.create_topics(configs)
    for k, v in req.items():
        v.result()
    for config in configs:
        log.info('Topic created [name={} partitions={} replication.factor={} replica.assignment={} configs={}]'.format(
            config.topic, config.num_partitions, config.replication_factor, config.replica_assignment, config.config))


def increase_partitions(configs: list):
    if not configs:
        log.debug("No topics to increase partitions.")
        return
    req = client.create_partitions(configs)
    for k, v in req.items():
        v.result()
    for config in configs:
        log.info('Topic partitions increased [name={} partitions={}]'.format(config.topic, config.new_total_count))
