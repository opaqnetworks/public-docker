# What is Apache Knox?

The Apache Knox Gateway is a REST API Gateway for interacting with Apache Hadoop clusters.
The Knox Gateway provides a single access point for all REST interactions with Apache Hadoop clusters.
For more info about Apache Knox see [Quick Start](https://knox.apache.org/books/knox-0-11-0/user-guide.html#Quick+Start)

This image provides simple setup for Apache Knox and access to WebHDFS by leveraging custom cluster topology and embedded LDAP server.
It allows to create custom LDAP user that can be used to access cluster.

# How to use this image

## Start a knox image
```
docker run --name my-knox \
       -e LDAP_USER=test \
       -e LDAP_PASSWORD=test-password \
       -e HADOOP_USER=hdfs \
       -e NAMENODE_SERVICE=namenode \
       -e NAMENODE_PORT=50070
       -d fourv/knox
```

## Access webhdfs
```
curl -i -k -u test:test-password -X GET 'https://localhost:8443/gateway/hadoop/webhdfs/v1/?op=LISTSTATUS'
```

## Envirnment variables

- LDAP_USER - LDAP user to create
- LDAP_PASSWORD - LDAP user password
- HADOOP_USER - Hadoop user that has access to HDFS
- NAMENODE_SERVICE - Hadoop Namenode hostname
- NAMENODE_PORT - Hadoop Namenode port
