#!/bin/bash
set -e

TOPOLOGY="conf/topologies/hadoop.xml"
LDAP_USERS="conf/users.ldif"

function replace() {
  local path=$1
  local my_regex=$2
  local my_replacement=$3

  sed -i "s/$my_regex/$my_replacement/" $path
}

function change_param_value() {
  local path=$1
  local name=$2
  local value=$3

  local param_regex="\(<param><name>$name<\/name><value>\)\(.*\)\(<\/value><\/param>\)"
  local new_param_value="\1$value\3"

  replace $path $param_regex $new_param_value
}

function change_role_value() {
 local path=$1
 local name=$2
 local value=$3

 local role_regex="\(<role>$name<\/role><url>\)\(.*\)\(<\/url>\)"
 local new_url_value="\1$value\3"

 replace $path $role_regex $new_url_value
}

function create_user() {
  local path=$1
  local username=$2
  local password=$3

  # LDAP entry for user
  cat >> $path <<EOF
# user
dn: uid=$username,ou=people,dc=hadoop,dc=apache,dc=org
objectclass:top
objectclass:person
objectclass:organizationalPerson
objectclass:inetOrgPerson
cn: $username
sn: $username
uid: $username
userPassword:$password
EOF
}

function config_ldap() {
  create_user "$LDAP_USERS" $LDAP_USER $LDAP_PASSWORD
}

function start_ldap() {
  bin/ldap.sh start
}

function config_gateway() {
  # create master secret
  # read "Persisting the Master Secret" paragraph: http://knox.apache.org/books/knox-0-11-0/user-guide.html
  # This is a test/development container - change this in production
  mkdir -p data/security
  echo "$MASTER_SECRET_LN1" > data/security/master
  echo "$MASTER_SECRET_LN2" >> data/security/master

  change_param_value "$TOPOLOGY" "principal.mapping" "$LDAP_USER=$HADOOP_USER;"
  change_role_value "$TOPOLOGY" "WEBHDFS" "http\:\/\/$NAMENODE_SERVICE\:$NAMENODE_PORT\/webhdfs"

  cd $GATEWAY_CONF_DIR
  for file in "$(ls)"
  do
    echo "Copying config: $file"
    cp $file "$KNOX_INSTALL/conf/$file"
  done
  cd $KNOX_INSTALL
}

function start_gateway() {
  bin/gateway.sh start > /tmp/gateway.log
  tail -F /tmp/gateway.log  # prevent container exit
}

function main() {

  cd $KNOX_INSTALL

  # ldap
  config_ldap
  start_ldap

  # gateway
  config_gateway
  start_gateway
}

main
