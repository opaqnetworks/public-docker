import _ from 'lodash';
// import d3 from 'd3';
// import $ from 'jquery';
import moment from 'moment';
import VislibVisualizationsPointSeriesProvider from './_point_series';
import getColor from 'ui/vislib/components/color/heatmap_color';

export default function HeatmapChartFactory(Private) {

  var legendColor = new Object();

  legendColor['0'] = `rgb(128, 128, 128)`;
  legendColor['1'] = `rgb(88, 88, 88)`;
  legendColor['2'] = `rgb(64, 153, 192)`;
  legendColor['3'] = `rgb(77, 116, 38)`;
  legendColor['4'] = `rgb(133, 156, 35)`;
  legendColor['5'] = `rgb(205, 184, 0)`;
  legendColor['6'] = `rgb(205, 123, 0)`;
  legendColor['7'] = `rgb(205, 51, 51)`;

  //
  // Original CDM Colors
  //

  // legendColor['0'] = `rgb(89, 90, 95)`;
  // legendColor['1'] = `rgb(31, 68, 92)`;
  // legendColor['2'] = `rgb(57, 75, 34)`;
  // legendColor['3'] = `rgb(57, 75, 34)`;
  // legendColor['4'] = `rgb(28, 90, 88)`;
  // legendColor['5'] = `rgb(28, 90, 88)`;

  var legendText = new Object();
  legendText['1 - 2'] = 'Not deployed, activity, not required for compliance';
  legendText['2 - 3'] = 'Not deployed, no activity, not required for compliance';
  legendText['3 - 4'] = 'Deployed, activity, not required for compliance';
  legendText['4 - 5'] = 'Deployed, no activity, not required for compliance';
  legendText['5 - 6'] = 'Deployed, activity, required for compliance';
  legendText['6 - 7'] = 'Deployed, no activity, required for compliance';
  legendText['7 - 8'] = 'Not deployed, activity, required for compliance';
  legendText['8 - 9'] = 'Not deployed, no activity, required for compliance';

  var cellText = new Object();
  cellText['detect-applications']   = 'RASP';
  cellText['detect-data']           = 'Deep Web';
  cellText['detect-devices']        = 'HIDS, Vul Scan';
  cellText['detect-networks']       = 'DDOSD, Gateway, NIDS,\nWID';
  cellText['detect-users']          = 'Behavorial';
  cellText['identify-applications'] = 'App Config';
  cellText['identify-data']         = 'Data Tag';
  cellText['identify-devices']      = 'Asset Scan, Conf Scan,\nSys Conf';
  cellText['identify-networks']     = 'NW Config, Netflow';
  cellText['identify-users']        = 'IA';
  cellText['protect-applications']  = 'AST, App Control, SEG,\nUA Apps, WAF';
  cellText['protect-data']          = 'DBMS, DLP, Encrypt,\nMDP, UA Data';
  cellText['protect-devices']       = 'AV, Dyn MW, HFW, HIPS,\nSWG, UA Dev';
  cellText['protect-networks']      = 'NAC, NAV, NFW, NIPS,\nProxy, Router, UA NW, Web Proxy';
  cellText['protect-users']         = 'EMA, Phishing, SMA';
  cellText['recover-data']          = 'Backups';
  cellText['respond-applications']  = 'Foren Apps';
  cellText['respond-data']          = 'DRM, Foren Data';
  cellText['respond-devices']       = 'Endpoint, Foren Dev,\nMDM, Stat MW';
  cellText['respond-networks']      = 'DDOSM, Foren NW, PCAP,\nPacket';

  const PointSeries = Private(VislibVisualizationsPointSeriesProvider);

  const defaults = {
    color: undefined, // todo
    fillColor: undefined // todo
  };
  /**
   * Line Chart Visualization
   *
   * @class HeatmapChart
   * @constructor
   * @extends Chart
   * @param handler {Object} Reference to the Handler Class Constructor
   * @param el {HTMLElement} HTML element to which the chart will be appended
   * @param chartData {Object} Elasticsearch query results for this specific chart
   */
  class HeatmapChart extends PointSeries {
    constructor(handler, chartEl, chartData, seriesConfigArgs) {
      super(handler, chartEl, chartData, seriesConfigArgs);
      this.seriesConfig = _.defaults(seriesConfigArgs || {}, defaults);

      this.handler.visConfig.set('legend', {
        labels: this.getHeatmapLabels(this.handler.visConfig),
        colors: this.getHeatmapColors(this.handler.visConfig)
      });

      const colors = this.handler.visConfig.get('legend.colors', null);
      if (colors) {
        this.handler.vis.uiState.setSilent('vis.defaultColors', null);
        this.handler.vis.uiState.setSilent('vis.defaultColors', colors);
      }
    }

    getHeatmapLabels(cfg) {
      const percentageMode = cfg.get('percentageMode');
      const colorsNumber = cfg.get('colorsNumber');
      const colorsRange = cfg.get('colorsRange');
      const zAxisConfig = this.getValueAxis().axisConfig;
      const zAxisFormatter = zAxisConfig.get('labels.axisFormatter');
      const zScale = this.getValueAxis().getScale();
      const [min, max] = zScale.domain();
      const labels = [];
      if (cfg.get('setColorRange')) {
        colorsRange.forEach(range => {
          // const from = range.from;
          // const to = range.to;
          const from = isFinite(range.from) ? zAxisFormatter(range.from) : range.from;
          const to = isFinite(range.to) ? zAxisFormatter(range.to) : range.to;

          var lookupID    = from + ' - ' + to;
          var displayText = "Unknown";

          if(legendText.hasOwnProperty(lookupID)) {
            displayText = legendText[lookupID];
          }

          // labels.push(`${from} - ${to}`);
          labels.push(`${displayText}`);
        });
      } else {
        for (let i = 0; i < colorsNumber; i++) {
          let label;
          let val = i / colorsNumber;
          let nextVal = (i + 1) / colorsNumber;
          if (percentageMode) {
            val = Math.ceil(val * 100);
            nextVal = Math.ceil(nextVal * 100);
            label = `${val}% - ${nextVal}%`;
          } else {
            val = val * (max - min) + min;
            nextVal = nextVal * (max - min) + min;
            if (max > 1) {
              val = Math.ceil(val);
              nextVal = Math.ceil(nextVal);
            }
            if (isFinite(val)) val = zAxisFormatter(val);
            if (isFinite(nextVal)) nextVal = zAxisFormatter(nextVal);
            label = `${val} - ${nextVal}`;
          }

          labels.push(label);
        }
      }

      return labels;
    }

    getHeatmapColors(cfg) {
      const colorsNumber = cfg.get('colorsNumber');
      const invertColors = cfg.get('invertColors');
      const colorSchema = cfg.get('colorSchema');
      const labels = this.getHeatmapLabels(cfg);
      const colors = {};
      for (const i in labels) {
        if (labels[i]) {
          const val = invertColors ? 1 - i / colorsNumber : i / colorsNumber;
          // colors[labels[i]] = getColor(val, colorSchema);

          var lookupID     = i;
          var displayColor = `rgb(255, 0, 0)`;

          if(legendColor.hasOwnProperty(lookupID)) {
            displayColor = legendColor[lookupID];
          }

          colors[labels[i]] = displayColor;
        }
      }
      return colors;
    }

    addSquares(svg, data) {
      const xScale = this.getCategoryAxis().getScale();
      const yScale = this.handler.valueAxes[1].getScale();
      const zScale = this.getValueAxis().getScale();
      // const ordered = this.handler.data.get('ordered');
      const tooltip = this.baseChart.tooltip;
      const isTooltip = this.handler.visConfig.get('tooltip.show');
      const isHorizontal = this.getCategoryAxis().axisConfig.isHorizontal();
      const colorsNumber = this.handler.visConfig.get('colorsNumber');
      const setColorRange = this.handler.visConfig.get('setColorRange');
      const colorsRange = this.handler.visConfig.get('colorsRange');
      const color = this.handler.data.getColorFunc();
      const labels = this.handler.visConfig.get('legend.labels');
      const zAxisConfig = this.getValueAxis().axisConfig;
      const zAxisFormatter = zAxisConfig.get('labels.axisFormatter');
      const showLabels = zAxisConfig.get('labels.show');

      const layer = svg.append('g')
        .attr('class', 'series');

      const squares = layer
        .selectAll('g.square')
        .data(data.values);

      squares
        .exit()
        .remove();

      let barWidth;
      if (this.getCategoryAxis().axisConfig.isTimeDomain()) {
        const { min, interval } = this.handler.data.get('ordered');
        const start = min;
        const end = moment(min).add(interval).valueOf();

        barWidth = xScale(end) - xScale(start);
        if (!isHorizontal) barWidth *= -1;
      }

      function x(d) {
        return xScale(d.x) + 2;
      }

      function y(d) {
        return yScale(d.series) + 2;
      }

      const [min, max] = zScale.domain();
      function getColorBucket(d) {
        let val = 0;
        if (setColorRange && colorsRange.length) {
          const bucket = _.find(colorsRange, range => {
            return range.from <= d.y && range.to > d.y;
          });
          return bucket ? colorsRange.indexOf(bucket) : -1;
        } else {
          if (isNaN(min) || isNaN(max)) {
            val = colorsNumber - 1;
          } else {
            val = (d.y - min) / (max - min); /* get val from 0 - 1 */
            val = Math.min(colorsNumber - 1, Math.floor(val * colorsNumber));
          }
        }
        return val;
      }

      function label(d) {
        const colorBucket = getColorBucket(d);
        if (colorBucket === -1) d.hide = true;
        return labels[colorBucket];
      }

      function z(d) {
        if (label(d) === '') return 'transparent';
        return color(label(d));
      }

      function z2(d) {
        if (label(d) === '') return 'transparent';

        // if(label(d) === 'Deployed, activity, required for compliance'
        // || label(d) === 'Not deployed, required for compliance') {
          // return 'url(#diagonalHatch)';
        // }

        return color(label(d));
      }

      function zBrighter(d) {
        if (label(d) === '') return 'transparent';
        return d3.rgb(color(label(d))).brighter(4);
      }

      var squareWidth = barWidth || xScale.rangeBand();
      var squareHeight = yScale.rangeBand();

      squares
        .enter()
        .append('g')
        .attr('class', 'square');

      squareWidth = squareWidth - 4;
      squareHeight = squareHeight - 4;

      squares.append('rect')
        .attr('x', x)
        .attr('rx', 5)
        .attr('width', squareWidth)
        .attr('y', y)
        .attr('ry', 5)
        .attr('height', squareHeight)
        .attr('data-label', label)
        .attr('fill', z)
        .attr('style', 'cursor: pointer;')
        .style('stroke', zBrighter)
        .style('stroke-width', '1px')
        .style('display', d => {
          return d.hide ? 'none' : 'initial';
        });

      squares.append('rect')
        .attr('x', x)
        .attr('rx', 5)
        .attr('width', squareWidth)
        .attr('y', y)
        .attr('ry', 5)
        .attr('height', squareHeight)
        .attr('data-label', label)
        .attr('fill', z2)
        .attr('style', 'cursor: pointer;')
        .style('stroke', zBrighter)
        .style('stroke-width', '1px')
        .style('display', d => {
          return d.hide ? 'none' : 'initial';
        });

      // todo: verify that longest label is not longer than the barwidth
      // or barwidth is not smaller than textheight (and vice versa)
      //
      if (showLabels) {
        const rotate = zAxisConfig.get('labels.rotate');
        const rotateRad = rotate * Math.PI / 180;
        const cellPadding = 5;
        const maxLength = Math.min(
          Math.abs(squareWidth / Math.cos(rotateRad)),
          Math.abs(squareHeight / Math.sin(rotateRad))
        ) - cellPadding;
        const maxHeight = Math.min(
            Math.abs(squareWidth / Math.sin(rotateRad)),
            Math.abs(squareHeight / Math.cos(rotateRad))
        ) - cellPadding;

        let hiddenLabels = false;

        squares.append('text')
          .style('font-size', '12px')
          // .text(d => zAxisFormatter(d.y))
          .text(function (d) {
            var lookupX     = d.x;
            var lookupY     = d.series;
            var lookupID    = lookupX.toLowerCase() + '-' + lookupY.toLowerCase();
            var displayText = "";

            if(cellText.hasOwnProperty(lookupID)) {
              displayText = cellText[lookupID];
              displayText = displayText.split("\n");
              displayText = displayText[0];
            }

            return displayText;
          })
          .style('display', function (d) {
            const textLength = this.getBBox().width;
            const textHeight = this.getBBox().height;
            const textTooLong = textLength > maxLength;
            const textTooWide = textHeight > maxHeight;
            if (!d.hide && (textTooLong || textTooWide)) {
              hiddenLabels = true;
            }
            return d.hide || textTooLong || textTooWide ? 'none' : 'initial';
          })
          .style('dominant-baseline', 'central')
          .style('text-anchor', 'middle')
          // .style('fill', zAxisConfig.get('labels.color'))
          .style('fill', 'white')
          .attr('x', function (d) {
            const center = x(d) + squareWidth / 2;
            return center;
          })
          .attr('y', function (d) {
            // const center = y(d) + squareHeight / 2;
            const center = y(d) + 11;
            return center;
          })
          .attr('transform', function (d) {
            const horizontalCenter = x(d) + squareWidth / 2;
            // const verticalCenter = y(d) + squareHeight / 2;
            const verticalCenter = y(d) + 11;
            return `rotate(${rotate},${horizontalCenter},${verticalCenter})`;
          });

        squares.append('text')
          .style('font-size', '12px')
          // .text(d => zAxisFormatter(d.y))
          .text(function (d) {
            var lookupX     = d.x;
            var lookupY     = d.series;
            var lookupID    = lookupX.toLowerCase() + '-' + lookupY.toLowerCase();
            var displayText = "";

            if(cellText.hasOwnProperty(lookupID)) {
              displayText = cellText[lookupID];
              displayText = displayText.split("\n");

              if(displayText.length > 1) {
                displayText = displayText[1];
              } else {
                displayText = "";
              }
            }

            return displayText;
          })
          .style('display', function (d) {
            const textLength = this.getBBox().width;
            const textHeight = this.getBBox().height;
            const textTooLong = textLength > maxLength;
            const textTooWide = textHeight > maxHeight;
            if (!d.hide && (textTooLong || textTooWide)) {
              hiddenLabels = true;
            }
            return d.hide || textTooLong || textTooWide ? 'none' : 'initial';
          })
          .style('dominant-baseline', 'central')
          .style('text-anchor', 'middle')
          // .style('fill', zAxisConfig.get('labels.color'))
          .style('fill', 'white')
          .attr('x', function (d) {
            const center = x(d) + squareWidth / 2;
            return center;
          })
          .attr('y', function (d) {
            // const center = y(d) + squareHeight / 2;
            const center = y(d) + this.getBBox().height + 10;
            return center;
          })
          .attr('transform', function (d) {
            const horizontalCenter = x(d) + squareWidth / 2;
            // const verticalCenter = y(d) + squareHeight / 2;
            const verticalCenter = y(d) + this.getBBox().height + 10;
            return `rotate(${rotate},${horizontalCenter},${verticalCenter})`;
          });

        if (hiddenLabels) {
          this.baseChart.handler.alerts.show('Some labels were hidden due to size constrains');
        }
      }

      if (isTooltip) {
        squares.call(tooltip.render());
      }

      return squares.selectAll('rect');
    }

    /**
     * Renders d3 visualization
     *
     * @method draw
     * @returns {Function} Creates the line chart
     */
    draw() {
      const self = this;

      return function (selection) {
        selection.each(function () {
          const svg = self.chartEl.append('g');
          svg.data([self.chartData]);

          // svg.append('defs')
            // .append('pattern')
            // .attr('id', 'diagonalHatch')
            // .attr('patternUnits', 'userSpaceOnUse')
            // .attr('width', 4)
            // .attr('height', 4)
            // .append('path')
            // .attr('d', 'M-1,1 l2,-2 M0,4 l4,-4 M3,5 l2,-2')
            // .attr('stroke', '#272727')
            // .attr('stroke-width', 1);

          const squares = self.addSquares(svg, self.chartData);
          self.addCircleEvents(squares);

          self.events.emit('rendered', {
            chart: self.chartData
          });

          return svg;
        });
      };
    }
  }

  return HeatmapChart;
}
