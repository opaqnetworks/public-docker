This container runs two cronjobs, that are used two bash scripts:

aws_elb_tagger.sh - adds tags for Elastic LoadBalancer
aws_volume_tagger.sh - adds tags for Elastic Block storages

Common idea for both scripts:
It looks for "AWS_TAG_*" vars in environmental variables and applies them in given isolated namespace.

E.g:
======================================================================
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: "k8s-aws-tagger"
spec:
  replicas: 1
  template:
    spec:
      containers:
      - name: k8s-aws-tagger
        image: fourv/k8s-aws-tagger
        imagePullPolicy: IfNotPresent
        env:
        - name: K8S_CLUSTER_NAME
          value: "{{ .Values.global.k8s_cluster_name }}"
        - name: NAMESPACE
          value: "{{ .Values.global.namespace }}"
        {{- range $key, $value := .Values.global.aws_custom_tags }}
        - name: "AWS_TAG_{{ $key }}"
          value: "{{ $value }}"
        {{- end }}
      restartPolicy: Always
======================================================================
