#!/bin/bash

function log(){
  echo "[ $(date "+%Y-%m-%d %H:%M:%S") ] --> $@"
}

function aws_call(){
  aws ec2 $@
}

function get_vol_list(){
  aws_call describe-volumes --filters "Name=tag:kubernetes.io/cluster/${K8S_CLUSTER_NAME},Values=owned" \
    "Name=tag:kubernetes.io/created-for/pvc/namespace,Values=${NAMESPACE}" \
    --query "Volumes[].VolumeId" --output text | tr '\t' '\n'
}

function create_vol_tag(){
  local vol_id=$1
  local key=$2
  local value=$3

  if [ -z "${vol_id}" -o -z "${key}" -o -z "${value}" ]; then
    log "========================================================="
    log "volume_id: ${vol_id}"
    log "Tag name: ${key}"
    log "Tag value: ${value}"
    log "skipping ... since not all input params have been passed."
    exit 0
  fi

  aws_call create-tags --resource ${vol_id} --tags Key=${key},Value=${value}
  log "Resource ${vol_id} has been tagged with ${key}: ${value}"
}

function create_vol_tags_based_on_env_vars(){
  local vol_id=$1
  for tag in "${VOL_TAGS[@]}"; do
    key=$(echo $tag | awk -F= '{print$1}' | sed -e 's/AWS_TAG_//')
    value=$(echo $tag | awk -F= '{print$2}')
    create_vol_tag ${vol_id} ${key} ${value}
  done
}

if [ -z "$AWS_ACCESS_KEY_ID" ]; then
  echo "Please double check AWS creds."
  echo "AWS_ACCESS_KEY_ID is missing."
  exit 1
fi

if [ -z "$AWS_SECRET_ACCESS_KEY" ]; then
  echo "Please double check AWS creds."
  echo "AWS_SECRET_ACCESS_KEY is missing."
  exit 1
fi

if [ -z "$AWS_DEFAULT_REGION" ]; then
  echo "Please double check AWS creds."
  echo "AWS_DEFAULT_REGION is missing."
  exit 1
fi

if [ -z "${NAMESPACE}" ]; then
  echo "NAMESPACE is missing."
  echo "Exiting ..."
  exit 1
fi

if [ -z "${K8S_CLUSTER_NAME}" ]; then
  echo "K8S_CLUSTER_NAME is missing."
  echo "Exiting ..."
  exit 1
fi

#Check and remove env variables for AWS_TAG_Name, since this tag:Name is used by k8s.
if [ ! -z "${AWS_TAG_Name}" ]; then
  log "tag:Name is used by k8s and can not be defined as custom tag."
  unset AWS_TAG_Name
fi

VOL_TAGS=($(env | grep "AWS_TAG_"))
if [ "${#VOL_TAGS[@]}" -eq 0 ]; then
  log "Unable to find variables with prefix \"\$AWS_TAG_*\" in env that could be applied for volume tagging."
  exit 0
fi

VOL_LIST=($(get_vol_list))
if [ "${#VOL_LIST[@]}" -eq 0 ]; then
  log "Volume list is empty. Unable to find available volumes for tagging."
  exit 0
fi

for vol in "${VOL_LIST[@]}"; do
  create_vol_tags_based_on_env_vars $vol
done
