#!/bin/bash

K8S_ENDPOINT="https://$KUBERNETES_SERVICE_HOST:$KUBERNETES_PORT_443_TCP_PORT"
TOKEN=$(</var/run/secrets/kubernetes.io/serviceaccount/token)
AUTH_HEADER="Authorization: Bearer ${TOKEN}"
NAMESPACE=$(</var/run/secrets/kubernetes.io/serviceaccount/namespace)

function log(){
  echo "[ $(date "+%Y-%m-%d %H:%M:%S") ] --> $@"
}

function k8s_api_call(){
  local curl_action=$1
  local url=$2
  curl -X ${curl_action} -sSk -H "${AUTH_HEADER}" ${K8S_ENDPOINT}$url
}

function aws_call(){
  aws elb $@
}

function get_elb_dns_list(){
  k8s_api_call GET /api/v1/namespaces/${NAMESPACE}/services | jq -r -e '.items[] | select( .status.loadBalancer.ingress ) | .status.loadBalancer.ingress[].hostname'
  if [ $? -ne 0 ]; then
    log "Unable to get list with ELB DNS names from k8s. Exiting ..."
    return 1
  fi
}

function get_elb_resource_name(){
  local elb_dns_name=$1

  if [ -z "${elb_dns_name}" ]; then
    log "skipping ... ${elb_dns_name} has not been provided."
    return 1
  else
    aws_call describe-load-balancers | jq -e '.LoadBalancerDescriptions[] | select(.DNSName=='\"${elb_dns_name}\"') | .LoadBalancerName'
    if [ $? -ne 0 ]; then
      log "Unable to get ELB name based on provided DNSName: \"${elb_dns_name}\""
      return 1
    fi
  fi
}

function add_elb_tag(){
  local elb_name=$( echo $1 | tr -d '"')
  local key=$2
  local value=$3

  if [ -z "${elb_name}" -o -z "${key}" -o -z "${value}" ]; then
    log "========================================================="
    log "ELB name: ${elb_name}"
    log "Tag name: ${key}"
    log "Tag value: ${value}"
    log "skipping ... since not all input params have been passed."
    return 1
  else
    aws_call add-tags --load-balancer-name ${elb_name} --tags "Key=${key},Value=${value}"
    log "Resource ${elb_name} has been tagged with ${key}: ${value}"
  fi
}

function create_elb_tags_based_on_env_vars(){
  local elb_name=$1
  for tag in "${ELB_TAGS[@]}"; do
    key=$(echo $tag | awk -F= '{print$1}' | sed -e 's/AWS_TAG_//')
    value=$(echo $tag | awk -F= '{print$2}')
    add_elb_tag ${elb_name} ${key} ${value}
  done
}

if [ -z "$AWS_ACCESS_KEY_ID" ]; then
  echo "Please double check AWS creds."
  echo "AWS_ACCESS_KEY_ID is missing."
  exit 1
fi

if [ -z "$AWS_SECRET_ACCESS_KEY" ]; then
  echo "Please double check AWS creds."
  echo "AWS_SECRET_ACCESS_KEY is missing."
  exit 1
fi

if [ -z "$AWS_DEFAULT_REGION" ]; then
  echo "Please double check AWS creds."
  echo "AWS_DEFAULT_REGION is missing."
  exit 1
fi

ELB_TAGS=($(env | grep "AWS_TAG_"))
if [ "${#ELB_TAGS[@]}" -eq 0 ]; then
  log "Unable to find variables with prefix \"\$AWS_TAG_*\" in env that could be applied for ELB tagging."
  exit 0
fi

ELB_DNS_LIST=($(get_elb_dns_list)) || exit $?
if [ "${#ELB_DNS_LIST[@]}" -eq 0 ]; then
  log "ELB list is empty. Unable to find available ELB for tagging."
  exit 0
fi

for elb_dns_name in "${ELB_DNS_LIST[@]}"; do
  ELB_NAME=$(get_elb_resource_name ${elb_dns_name})
  if [ ! -z "${ELB_NAME}" ]; then
    create_elb_tags_based_on_env_vars ${ELB_NAME}
  else
    log "Unable to get resource name for ${elb_dns_name}"
  fi
done
