#!/bin/bash
K8S_ENDPOINT="https://$KUBERNETES_SERVICE_HOST:$KUBERNETES_PORT_443_TCP_PORT"
TOKEN=$(</var/run/secrets/kubernetes.io/serviceaccount/token)
AUTH_HEADER="Authorization: Bearer ${TOKEN}"
NAMESPACE=$(</var/run/secrets/kubernetes.io/serviceaccount/namespace)

function log_date(){
  echo "[$(date)] -- $@"
}

function k8s_api_call(){
  local curl_action=$1
  local url=$2
  curl -X ${curl_action} -sSk -H "${AUTH_HEADER}" ${K8S_ENDPOINT}/$url
}

function k8s_delete_pod(){
  local pod_name=$(hostname)
  k8s_api_call DELETE api/v1/namespaces/${NAMESPACE}/pods/${pod_name}
}

function secrets_hash_checker(){
  for i in ${ENV_LIST[@]}; do
    local var_name=$(echo $i | cut -f1 -d'=');
    local hash_file=$(cat ${HASH_DIR}/${var_name});
    if [ "${!var_name}" != "$hash_file" ]; then
      log_date "$i has been changed."
      log_date "Attempt to recreate pod..."
      k8s_delete_pod
    fi
  done
}

VAR_PATTERN=$1
if [ -z "$VAR_PATTERN" ]; then
  VAR_PATTERN="_HASH"
fi

HASH_DIR=$2
if [ -z "$HASH_DIR" ]; then
  HASH_DIR="/etc/secret-hash"
fi

ENV_LIST=($(env | grep ${VAR_PATTERN}))
if [ ${#ENV_LIST[@]} -eq 0 ]; then
  log_date "Unable to find variable based on \$VAR_PATTERN=${VAR_PATTERN}"
  exit 0
fi

secrets_hash_checker
