#!/usr/bin/dumb-init /bin/bash

source /scripts/common.sh
source /scripts/kafka_api.sh

kube_version_check || exit $?
time create_kafka_topics || exit $?

