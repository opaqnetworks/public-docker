function log(){
  echo "-- $@"
}

function error(){
  log "ERROR: $@"
}

function kube_version_check() {
  version_output="$(kubectl version)"
  client_major_version="$(echo ${version_output} | grep "Client" | sed -r 's;.*Major:"([0-9]+)".*;\1;g')"
  client_minor_version="$(echo ${version_output} | grep "Client" | sed -r 's;.*Minor:"([0-9]+)".*;\1;g')"
  server_major_version="$(echo ${version_output} | grep "Server" | sed -r 's;.*Major:"([0-9]+)".*;\1;g')"
  server_minor_version="$(echo ${version_output} | grep "Server" | sed -r 's;.+Minor:"([0-9]+)\+?".+;\1;g')"

  if [ -n "$client_major_version" ] || [ -n "$client_minor_version" ] || \
     [ -n "$server_major_version" ] || [ -n "$server_minor_version" ] || \
     [[ "$client_major_version" -eq "$server_major_version" ]] || \
     [[ "$client_minor_version" -ge "$server_minor_version" ]]; then
     log "Client and Server kubectl versions are compatible"
    return 0
  else
    error "Server and Client version mismatch"
    log "$version_output"
    return 1
  fi
}

