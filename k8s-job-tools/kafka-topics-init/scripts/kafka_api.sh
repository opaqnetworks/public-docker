#!/usr/bin/env bash

KAFKA_TOPICS_INIT_FILE=/specs/kafka-topics-init.json

function echo_err(){
  echo -e "\033[1;31m-- ERROR: $* \033[0m"
}

function create_kafka_topic() {
  local topic=$1
  local partitions=$2
  local replication_factor=$3
  local exists_already=$4

  if [ "$exists_already" != "true" ]; then
    echo "-- Creating topic=${topic}, partitions=${partitions}, replication_factor=${replication_factor}"
    kubectl exec cp-kafka-0 -c cp-kafka-broker -- kafka-topics --bootstrap-server cp-kafka:9092 --create --topic "${topic}" --partitions "${partitions}" --replication-factor "${replication_factor}"
  else
    #
    # Error: Option "[replication-factor]" can't be used with option "[alter]"
    #
    # https://stackoverflow.com/questions/37960767/how-to-change-the-number-of-replicas-of-a-kafka-topic
    # Fix is expensive to implement, so for now replication factor can't be changed via script, only manually
    #
    echo "-- Topic ${topic} already exists, updating ... topic=${topic}, partitions=${partitions}"
    kubectl exec cp-kafka-0 -c cp-kafka-broker -- kafka-topics --bootstrap-server cp-kafka:9092 --alter --topic "${topic}" --partitions "${partitions}"
  fi
}

function print_topics() {
  echo "-- Kafka Topics:"
  topics=$(kubectl exec cp-kafka-0 -c cp-kafka-broker -- kafka-topics --bootstrap-server cp-kafka:9092 --list)

  for topic in ${topics}; do
    if [[ ${topic}  != _* ]]; then # skip internal topics that start with underscore _
       echo "${topic}"
    fi
  done
}

function join () {
  local IFS="$1"
  shift
  echo "$*"
}

function alter_topic_config() {
  local topic=$1
  shift
  local configs=$*

  local config_params
  config_params=$(join "," ${configs})
  echo "-- Setting config params for topic $topic: $config_params"
  kubectl exec cp-kafka-0 -c cp-kafka-broker -- kafka-configs --zookeeper gs-zookeeper:2181 --entity-type topics --entity-name "${topic}" --alter --add-config "${config_params}" > /dev/null
}

function get_sts_replicas() {
  kubectl get sts "$1" -o jsonpath='{.status.readyReplicas} {.status.replicas}'
}

function wait_for_ready_replicas(){
  local name=$1
  local status
  local replicas
  local readyReplicas
  local tries
  local maxTries=6

  status=$(get_sts_replicas "${name}")
  replicas=$(echo "$status" | cut -d " " -f 1)
  readyReplicas=$(echo "$status" | cut -d " " -f 2)
  echo "-- Initial replicas: $replicas, readyReplicas: $readyReplicas"

  tries=0
  while (( replicas != readyReplicas )) && (( tries < maxTries )); do
    echo "-- ${name} pods are not ready yet. Waiting 10 seconds for next try..."
    tries=$((tries + 1))
    sleep 10
    status=$(get_sts_replicas "${name}")
    replicas=$(echo "$status" | cut -d " " -f 1)
    readyReplicas=$(echo "$status" | cut -d " " -f 2)
    echo "-- $tries/$maxTries - replicas: $replicas, readyReplicas: $readyReplicas"
  done

  if (( replicas != readyReplicas )); then
    echo_err " Skipping ${name} tasks. No ${name} running"
    return 1
  fi
}

function wait_for_kafka_0_ready_container() {
  local is_ready
  local sleep=1 # seconds
  local maxTries=9000 # 90 tries * 2 seconds = 180 seconds = 3 min
  local tries=0

  while((tries != maxTries)); do
    is_ready=$(kubectl get po "cp-kafka-0" -o jsonpath='{.status.containerStatuses[0].ready}')
    if [[ "${is_ready}" == "true" ]]; then
      echo "cp-kafka-0 is ready"
      return 0
    fi
    date +'%Y-%m-%d %H:%M:%S'
    tries=$((tries + 1))
    echo "${tries}: cp-kafka-0 is not ready yet. Sleep ${sleep} seconds"
    sleep ${sleep}
  done
  echo_err "cp-kafka-0 is not ready. Exiting.."
  return 1
}

function get_kafka_topics_info() {
  local info
  info=$(cat $KAFKA_TOPICS_INIT_FILE)

  read -r -d '' topics_info_command << EOL
  (.[].topic_defaults | select(.)) as \$defaults | map(.topic | select(.) | {
    name: .name,
    replication_factor: (.replication_factor // \$defaults.replication_factor | tonumber),
    retention_ms: (.retention_ms // \$defaults.retention_ms | tonumber),
    retention_bytes: (.retention_bytes // \$defaults.retention_bytes // "-1" | tonumber),
    partitions: (.partitions // \$defaults.partitions | tonumber),
    segment_bytes: (.segment_bytes // \$defaults.segment_bytes | tonumber),
    cleanup_policy: (.cleanup_policy // \$defaults.cleanup_policy // "delete")
  })
EOL

  echo "$info" | jq "$topics_info_command"
}

function get_existing_topics_info() {
  local actual_topics
  actual_topics=$(kubectl exec cp-kafka-0 -c cp-kafka-broker -- kafka-topics --bootstrap-server cp-kafka:9092 --describe \
    | awk '$3 ~ /PartitionCount/ { print $0}' \
    | while read line; do
        local topic_name=$(echo "$line" | cut -d$'\t' -f 1 | sed 's/^Topic: //')
        local partitions=$(echo "$line" | cut -d$'\t' -f 2 | sed 's/^PartitionCount: //')
        local replication_factor=$(echo "$line" | cut -d$'\t' -f 3 | sed 's/^ReplicationFactor: //')
        local configs=$(echo "$line" | cut -d$'\t' -f 4 | sed 's/Configs: //')
        echo -n "{\"name\":\"$topic_name\", \"replication_factor\":\"$replication_factor\", \"partitions\":\"$partitions\"";
        local key
        local value
        for config in ${configs//,/ }; do
          key=$(echo "$config" | cut -d '=' -f 1)
          value=$(echo "$config" | cut -d '=' -f 2)
          echo -n ", \"$key\":\"$value\""
        done
        echo -e "}"
      done | paste -sd "," -)

  echo "[$actual_topics]" | jq '.'
}

function create_kafka_topics() {
  echo "-- Creating Kafka topics"

  local topics_info
  topics_info=$(get_kafka_topics_info)

  wait_for_ready_replicas gs-zookeeper || exit $?
  wait_for_kafka_0_ready_container || exit $?

  local actual_topics_info
  actual_topics_info=$(get_existing_topics_info)

  #
  # converting to base64 and back is used to remove spaces in the single topic info
  # as a result bash iterates over topics is not iterates over topic parts
  #
  for topic_info_b64 in $(echo "$topics_info" | jq "map(.) | map(@base64) | .[]" -r); do
    local topic_info
    topic_info=$(echo "$topic_info_b64" | base64 -d)

    local topic_name=$(echo "$topic_info" | jq ".name" -r) # remove quotes
    local replication_factor=$(echo "$topic_info" | jq '.replication_factor')
    local retention_ms=$(echo "$topic_info" | jq '.retention_ms')
    local retention_bytes=$(echo "$topic_info" | jq '.retention_bytes')
    local segment_bytes=$(echo "$topic_info" | jq '.segment_bytes')
    local partitions=$(echo "$topic_info" | jq '.partitions')
    local cleanup_policy=$(echo "$topic_info" | jq '.cleanup_policy' -r) # remove quotes

    local actual_topic_info=$(echo "$actual_topics_info" | jq ".[] | select (.name==\"$topic_name\")")
    local exists_already=$(echo "$actual_topics_info" | jq "any (.[]; .name==\"$topic_name\")")
    local part_replyf_same=$(echo "$actual_topic_info" \
      | jq "any (.; .partitions==\"$partitions\" and .replication_factor==\"$replication_factor\" )")
    local configs_same=$(echo "$actual_topic_info" \
      | jq "any (.; .\"retention.ms\"==\"$retention_ms\" and .\"segment.bytes\"==\"$segment_bytes\" and .\"cleanup.policy\"==\"$cleanup_policy\")")

    echo "#### ${topic_name}:"
    echo -n "Expected:"
    echo "$topic_info" | jq -c "."
    echo -n "Actual:"
    echo "$actual_topic_info" | jq -c "."
    echo "exists_already=$exists_already part_replyf_same=$part_replyf_same configs_same=$configs_same"

    if [[ "${exists_already}" == "true" ]]; then
      if [[ "${part_replyf_same}" != "true" ]]; then
        echo "-- Update kafka topic"
        create_kafka_topic "$topic_name" "$partitions" "$replication_factor" "true"
      fi
      if [[ "${configs_same}" != "true" ]]; then
        echo "-- Update kafka topic configs"
        alter_topic_config "$topic_name" retention.ms="$retention_ms" retention.bytes="$retention_bytes" segment.bytes="$segment_bytes" cleanup.policy="$cleanup_policy" || exit $?
      fi
    else
      echo "-- Create kafka topic from scratch"
      create_kafka_topic "$topic_name" "$partitions" "$replication_factor" "false"
      echo "-- Put kafka topic configs from scratch"
      alter_topic_config "$topic_name" retention.ms="$retention_ms" retention.bytes="$retention_bytes" segment.bytes="$segment_bytes" cleanup.policy="$cleanup_policy" || exit $?
    fi
  done

  print_topics
  echo "-- Kafka topic creation complete"
}
