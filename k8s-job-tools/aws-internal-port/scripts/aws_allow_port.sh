#!/bin/bash

source $SCRIPT_PATH/lib/common.sh

SG_LOGICAL_RESOURCE_PATTERN="SecurityGroupWorker"

if [ -z "${INTERNAL_PORT}" ]; then
  echo "INTERNAL_PORT is missing."
  echo "Exiting ..."
  exit 1
fi

if [ -z "${K8S_CLUSTER_NAME}" ]; then
  echo "K8S_CLUSTER_NAME is missing."
  echo "Exiting ..."
  exit 1
fi

echo "Attempt to fetch info about resource details from aws ..."
WORKER_STACK_NAME=$(aws_cf_call describe-stacks --output text | grep ${K8S_CLUSTER_NAME} | grep -m1 ControlPlaneStackName | awk '{print$3}')

if [ -z "${WORKER_STACK_NAME}" ]; then
  echo "Unable to find cloudformation stack that could be related to \"${K8S_CLUSTER_NAME}\""
  exit 1
fi

echo "========================================================================="
echo "Worker Stack Name: ${WORKER_STACK_NAME}"


SG_ID=$(get_physical_resource_id ${WORKER_STACK_NAME} ${SG_LOGICAL_RESOURCE_PATTERN})
if [ -z "${SG_ID}" ]; then
  echo "Unable to grep security-group id in cloudformation stack \"${WORKER_STACK_NAME}\" using pattern: \"${SG_LOGICAL_RESOURCE_PATTERN}\""
  echo "========================================================================="
  exit 1
fi

echo "Security Group: ${SG_ID}"
echo "========================================================================="
modify_sg_ingress ${SG_ID} tcp ${INTERNAL_PORT}
