
if [ -z "$AWS_ACCESS_KEY_ID" ]; then
  echo "Please double check AWS creds."
  echo "AWS_ACCESS_KEY_ID is missing."
  exit 1
fi

if [ -z "$AWS_SECRET_ACCESS_KEY" ]; then
  echo "Please double check AWS creds."
  echo "AWS_SECRET_ACCESS_KEY is missing."
  exit 1
fi

if [ -z "$AWS_DEFAULT_REGION" ]; then
  echo "Please double check AWS creds."
  echo "AWS_DEFAULT_REGION is missing."
  exit 1
fi

function aws_cf_call(){
  aws cloudformation $@
}

function get_physical_resource_id(){
  local stack_name=$1
  local logical_resource_id=$2
  aws_cf_call describe-stack-resources --stack-name ${stack_name} --query 'StackResources[?LogicalResourceId==`'"${logical_resource_id}"'`].PhysicalResourceId' --output=text
}

function modify_sg_ingress(){
  local sg_id=$1
  local protocol=$2
  local port=$3

  #check if port already there or not
  test -z $(aws ec2 describe-security-groups --group-ids ${sg_id} --query 'SecurityGroups[].IpPermissions[?ToPort==`'"${port}"'`].ToPort' --output text);
  if [ $? -eq 0 ]; then
    aws ec2 authorize-security-group-ingress --group-id ${sg_id} --protocol ${protocol} --port ${port} --source-group ${sg_id}
    if [ $? -eq 0 ]; then
      echo "Port ${port} has been allowed in Security Group ${sg_id}."
    else
      echo
      echo "Failed. Unable to add port!"
      return 1
    fi
  else
    echo "Rule to allow traffic for port ${port} already existed. "
    return 0
  fi
}
