# Add IAM Permissions here.  The CD service account needs "Project IAM Admin".

# In general use `google_project_iam_binding` to ensure IaC controls the entire
# role, but careful to add back the internal serviceaccounts.

# Example:
#
# resource "google_project_iam_binding" "editors" {
#   project = var.project
#   role = "roles/editor"

#   members = [
#     # internal service accounts have a pattern
#     "serviceAccount:<project id>@<api>.gserviceaccount.com",

#     # service accounts added by the pipeline can be referenced this way
#     "serviceAccount:${google_service_account.service_accounts["my-service-account"].email}",

#     # users are defined by email
#     "user:bobrzut@opaq.com",
#   ]

#   depends_on = [google_project_service.apis,]
# }
