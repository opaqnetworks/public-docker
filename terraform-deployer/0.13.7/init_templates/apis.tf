# A list of the APIs to enable.  The CD service account needs "API Keys Admin"
variable "apis" {
  type = set(string)

  default = [
    # Cloud Resource Manager API, so other APIs can be added
    "cloudresourcemanager.googleapis.com",

    # Meta API that enables other APIs
    "cloudapis.googleapis.com",

    # Kubernetes
    "compute.googleapis.com",
    "containerregistry.googleapis.com",
    "container.googleapis.com",

    # Monitoring
    "logging.googleapis.com",
    "monitoring.googleapis.com",

    # IAM
    "iam.googleapis.com",
    "iamcredentials.googleapis.com",

    # Service Accounts
    "servicemanagement.googleapis.com",

    # Buckets
    "storage-api.googleapis.com",
    "storage-component.googleapis.com",
  ]
}

## Iterate over all items and activate them ##
resource "google_project_service" "apis" {
  for_each = var.apis

  project = var.project

  service = each.value

  disable_on_destroy = false
}
