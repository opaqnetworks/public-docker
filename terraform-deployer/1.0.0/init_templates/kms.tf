# Add KMS Keyrings and keys.  The CD service account needs "Cloud KMS Admin".

# Example:
#
# resource "google_kms_key_ring" "keyring" {
#   name     = "my-keys"
#   location = var.region

#   lifecycle {
#     prevent_destroy = true
#   }
# }

# resource "google_kms_crypto_key" "key" {
#   name            = "key"
#   key_ring        = google_kms_key_ring.keyring.self_link
#   rotation_period = "2678400s" # 30 days

#   lifecycle {
#     prevent_destroy = true
#   }
# }
