variable "project" {
  description = ""
  default     = "my-project"
}

variable "region" {
  description = ""
  default     = "us-east4"
}

provider "google" {
  credentials = file("account.json") # created from the ACCOUNT_KEY env variable
  project     = var.project
}
