# Add buckets here. The CD service account needs "Storage Admin".

# Example:
#
# variable "buckets" {
#   type = set(string)

#   default = [
#     "my-first-bucket",
#     "my-second-bucket",
#   ]
# }

# resource "google_storage_bucket" "buckets" {
#   for_each = var.buckets

#   name          = each.key
#   location      = var.region
#   storage_class = "REGIONAL"

#   versioning {
#     enabled = true
#   }

#   lifecycle {
#     prevent_destroy = true
#   }
# }
