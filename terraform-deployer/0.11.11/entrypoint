#!/usr/bin/dumb-init /bin/bash

function log(){
  echo "-- $@" >&2
}

function header(){
  echo "========  $@  ========" >&2
}

function error(){
  log "ERROR: $@"
}

function info(){
  log "INFO: $@"
}

function usage(){
  echo "docker run -ti --rm -v /local/config:/data quay.io/opaqnetworks/terraform-deployer:0.11.11-201902041355 <command>"
  echo
  echo "Generic Opaq deployer for one-off terraform definitions."
  echo
  echo "Commands:"
  echo "  apply        - Apply changes defined by the *.tf files"
  echo "  destroy      - Destroy the items described by the *.tf files."
  echo "  help         - This help message"
  echo "  init         - Create some seed files."
  echo "  plan         - Plan the change based on the *.tf files"
  echo "  plan_destroy - Plan the detroy based on the *.tf file"
}

function init(){
  for path in $(find $INIT_DIR -type f); do
    local file=$(basename $path)
    header "$file"
    if [ -f "$DATA_DIR/$file" ]; then
      info "'$file' found, copying in original as reference"
      cp "$INIT_DIR/$file" "$DATA_DIR/$file.orig"
    else
      info "'$file' created"
      cp "$INIT_DIR/$file" "$DATA_DIR/$file"
    fi
  done
}

function setup(){
  # quick check that the data dir is mounted in
  if [ ! -d "$DATA_DIR" ]; then
    error "$DATA_DIR doesn't exist, was it mounted in?"
    usage
    exit 1
  fi
  
  terraform init
}

# Fail if any input is required
export TF_INPUT=false

if [ -n "$DEBUG" ]; then
  set -x
fi


case "$1" in
  apply)
    set -e
    setup
    terraform apply -auto-approve
    ;;
  destroy)
    setup
    terraform destroy -auto-approve
    ;;
  plan)
    setup
    terraform plan
    ;;
  plan_destroy)
    setup
    terraform plan -destroy
    ;;
  init)
    init
    ;;
  help|-h|--help)
    usage
    exit 0
    ;;
  "")
    error "No command supplied"
    usage
    exit 1
    ;;
  *)
    error "Unknown command '$1'"
    usage
    exit 1
    ;;
esac