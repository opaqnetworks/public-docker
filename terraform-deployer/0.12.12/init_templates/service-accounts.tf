# Add additional service accounts.  The CD service account will need
# "Service Account Admin"

# Note: a key for this service account will need to be manually generated
# and downloaded for use in the Vault service.  Also, perms needs to be
# assigned in iam.tf.

# Example:
#
# variable "accounts" {
#   type = set(string)
#
#   default = [
#     "my-service-account",
#   ]
# }
# resource "google_service_account" "service_accounts" {
#   for_each = var.accounts
#
#   account_id = "vault-server"
#   display_name = "Valut Server"
#   project = "${var.project}"
# }
