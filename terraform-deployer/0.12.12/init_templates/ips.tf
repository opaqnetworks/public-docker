# Add Static IP pre-allocation here.  The CD service account needs
# "Compute Network Admin".

# Example:
#
# variable "ips" {
#   type = set(string)
#
#   default = [
#     "first-ip",
#     "second-ip",
#   ]
# }
# resource "google_compute_address" "ips" {
#   for_each = var.ips
#
#   name    = each.value
#   region  = "${var.region}"
#   project = "${var.project}"
# }
# output "ips" {
#   value = "${google_compute_address.ips[*].address}"
# }
