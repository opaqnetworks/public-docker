variable project {
  description = ""
  default = "my-project"
}

variable region {
  description = ""
  default = "us-east4"
}

provider "google" {
  # Note: the "account.json" is created from the ACCOUNT_KEY ENV variable.
  credentials = "${file("account.json")}"
  project     = "${var.project}"
}
