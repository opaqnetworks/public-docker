#!/usr/bin/dumb-init /bin/bash
set -e

LOGGER_PROPS_FILE="$ZK_CONF_DIR/log4j.properties"
JAVA_ENV_FILE="$ZK_CONF_DIR/java.env"
HOST=$(hostname -s)
DOMAIN=$(hostname -d)

function validate_env() {
  echo "Validating environment"

  if [ -z $ZK_REPLICAS ]; then
    echo "ZK_REPLICAS is a mandatory environment variable"
    exit 1
  fi

  if [[ $HOST =~ (.*)-([0-9]+)$ ]]; then
    NAME=${BASH_REMATCH[1]}
    ORD=${BASH_REMATCH[2]}
  else
    echo "Failed to extract ordinal from hostname $HOST"
    exit 1
  fi

  ZK_MY_ID=$((ORD + 1))
  echo "ZK_REPLICAS=$ZK_REPLICAS"
  echo "ZK_MY_ID=$ZK_MY_ID"
  echo "ZK_LOG_LEVEL=$ZK_LOG_LEVEL"
  echo "ZK_DATA_DIR=$ZK_DATA_DIR"
  echo "ZK_DATA_LOG_DIR=$ZK_DATA_LOG_DIR"
  echo "ZK_LOG_DIR=$ZK_LOG_DIR"
  echo "ZK_CLIENT_PORT=$ZK_CLIENT_PORT"
  echo "ZK_SERVER_PORT=$ZK_SERVER_PORT"
  echo "ZK_ELECTION_PORT=$ZK_ELECTION_PORT"
  echo "ZK_TICK_TIME=$ZK_TICK_TIME"
  echo "ZK_INIT_LIMIT=$ZK_INIT_LIMIT"
  echo "ZK_SYNC_LIMIT=$ZK_SYNC_LIMIT"
  echo "ZK_MAX_CLIENT_CNXNS=$ZK_MAX_CLIENT_CNXNS"
  echo "ZK_MIN_SESSION_TIMEOUT=$ZK_MIN_SESSION_TIMEOUT"
  echo "ZK_MAX_SESSION_TIMEOUT=$ZK_MAX_SESSION_TIMEOUT"
  echo "ZK_HEAP_SIZE=$ZK_HEAP_SIZE"
  echo "ZK_SNAP_RETAIN_COUNT=$ZK_SNAP_RETAIN_COUNT"
  echo "ZK_PURGE_INTERVAL=$ZK_PURGE_INTERVAL"
  echo "ENSEMBLE"
  print_servers
  echo "Environment validation successful"
}

function print_servers() {
  for ((i = 1; i <= $ZK_REPLICAS; i++)); do
    echo "server.$i=$NAME-$((i - 1)).$DOMAIN:$ZK_SERVER_PORT:$ZK_ELECTION_PORT"
  done
}

function create_log_props() {
  rm -f $LOGGER_PROPS_FILE
  echo "Creating ZooKeeper log4j configuration"
  echo "zookeeper.root.logger=CONSOLE" >>$LOGGER_PROPS_FILE
  echo "zookeeper.console.threshold="$ZK_LOG_LEVEL >>$LOGGER_PROPS_FILE
  echo "log4j.rootLogger=\${zookeeper.root.logger}" >>$LOGGER_PROPS_FILE
  echo "log4j.appender.CONSOLE=org.apache.log4j.ConsoleAppender" >>$LOGGER_PROPS_FILE
  echo "log4j.appender.CONSOLE.Threshold=\${zookeeper.console.threshold}" >>$LOGGER_PROPS_FILE
  echo "log4j.appender.CONSOLE.layout=org.apache.log4j.PatternLayout" >>$LOGGER_PROPS_FILE
  echo "log4j.appender.CONSOLE.layout.ConversionPattern=%d{ISO8601} [myid:%X{myid}] - %-5p [%t:%C{1}@%L] - %m%n" >>$LOGGER_PROPS_FILE
  echo "Wrote log4j configuration to $LOGGER_PROPS_FILE"
}

function create_java_env() {
  rm -f $JAVA_ENV_FILE
  echo "Creating JVM configuration file"
  echo "ZK_LOG_DIR=$ZK_LOG_DIR" >>$JAVA_ENV_FILE
  echo "JVMFLAGS=\"-Xmx$ZK_HEAP_SIZE -Xms$ZK_HEAP_SIZE\"" >>$JAVA_ENV_FILE
  echo "Wrote JVM configuration to $JAVA_ENV_FILE"
}

function create_config() {
  rm -f $ZK_CONFIG_FILE
  CONFIG="$ZK_CONF_DIR/zoo.cfg"
  {
    echo "clientPort=$ZK_CLIENT_PORT"
    echo "dataDir=$ZK_DATA_DIR"
    echo "dataLogDir=$ZK_DATA_LOG_DIR"

    echo "tickTime=$ZK_TICK_TIME"
    echo "initLimit=$ZK_INIT_LIMIT"
    echo "syncLimit=$ZK_SYNC_LIMIT"
    echo "minSessionTimeout=$ZK_MIN_SESSION_TIMEOUT"
    echo "maxSessionTimeout=$ZK_MAX_SESSION_TIMEOUT"
    echo "autopurge.snapRetainCount=$ZK_SNAP_RETAIN_COUNT"
    echo "autopurge.purgeInterval=$ZK_PURGE_INTERVAL"
    echo "maxClientCnxns=$ZK_MAX_CLIENT_CNXNS"
    echo "standaloneEnabled=$ZK_STANDALONE_ENABLED"
    echo "admin.enableServer=$ZK_ADMINSERVER_ENABLED"
  } >>"$CONFIG"

  if [[ -z $(find "$ZK_DATA_DIR"/ -name "snapshot*") ]]; then
    wget "$GCS_PATH/zookeeper/snapshot.0" -P "$ZK_DATA_DIR"/version-2/
  fi

  if [ $ZK_REPLICAS -gt 1 ]; then
    print_servers >>$CONFIG
  fi

  if [[ -n $ZK_4LW_COMMANDS_WHITELIST ]]; then
    echo "4lw.commands.whitelist=$ZK_4LW_COMMANDS_WHITELIST" >>"$CONFIG"
  fi

  for cfg_extra_entry in $ZK_CFG_EXTRA; do
    echo "$cfg_extra_entry" >>"$CONFIG"
  done
}

function create_data_dirs() {
  echo "Creating ZooKeeper data directories"

  if [ ! -d $ZK_DATA_DIR ]; then
    mkdir -p $ZK_DATA_DIR
    chown -R zookeeper:zookeeper $ZK_DATA_DIR
  fi

  if [ ! -d $ZK_DATA_LOG_DIR ]; then
    mkdir -p $ZK_DATA_LOG_DIR
    chown -R zookeeper:zookeeper $ZK_DATA_LOG_DIR
  fi

  if [ ! -d $ZK_LOG_DIR ]; then
    mkdir -p $ZK_LOG_DIR
    chown -R zookeeper:zookeeper $ZK_LOG_DIR
  fi

  if [ ! -f $ZK_DATA_DIR/myid ]; then
    echo $ZK_MY_ID >>"$ZK_DATA_DIR/myid"
  fi

  echo "Created ZooKeeper data directories"
}

validate_env && create_config && create_log_props && create_data_dirs && create_java_env
