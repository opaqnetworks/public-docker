#!/bin/bash

MANIFEST=$1
if [ -z "$MANIFEST" ]; then
  MANIFEST="/tmp/MANIFEST"
fi

echo "Reading manifest $MANIFEST"

pinto init
for i in $(cat ${MANIFEST}); do
  echo "Pulling ${i} ... "
  pinto pull ${i};
done
