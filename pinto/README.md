Pinto - local cpan repository.

Image has been splitted up into two parts:
1) base image, since install takes pretty much time
2) dca-cpan-repo which contains MANIFEST with the list of perl modules that should be installed during building docker image.

Image can be used as standalone by running the following commands:
docker pull fourv/pinto:dca-cpan-repo

In order to list available modules run:
docker run -ti --rm fourv/pinto:dca-cpan-repo pinto list

In order to run repository in "daemon mode":
docker run --rm --name pintod -p 3111:3111 fourv/pinto:dca-cpan-repo

For example cpanm accepts the following args:

--mirror="http://localhost:3111" --mirror-only

These args allow to bind cpan to use particular repository.
