#!/usr/bin/env bash

## Define functions
function main(){
  while getopts ":hv" opt; do
    case "${opt}" in
      h) cat usage.txt && exit
        ;;
      v) kubectl version --client --short && exit
        ;;
      *) log "Unknown option -$opt"
        usage && exit 1
        ;;
    esac
  done
  shift $((OPTIND-1))

  ## Sanity check
  if [[ -n "${KEEP_LAST_SNAPSHOTS}" ]]; then
    rotateSnapshots "${KEEP_LAST_SNAPSHOTS}"
  else
    log "KEEP_LAST_SNAPSHOTS env var is not provided. Exiting..."
    exit 1
  fi
}

function log(){
  echo "-- $*"
}

function header(){
  echo "========  $*  ========"
}

function usage() {
  echo "Args:"
  echo " -h  Print help message"
  echo " -v  Print kubectl version"
}

function rotateSnapshots(){
  local keep_snap
  local all_snap
  local del_snap

  keep_snap=${1}
  all_snap=$(kubectl get snap | awk '/[0-9]{8}-[0-9]{6}/{print $1}')
  del_snap=$( echo "${all_snap}" | head -n -"${keep_snap}")

  log "Keeping ${keep_snap} last snapshots"
  echo
  header "ALL SNAPSHOTS"
  echo "${all_snap}"
  echo

  if [[ -n "${del_snap}" ]]; then
    header "SNAPSHOTS TO DELETE"
    echo "${del_snap}"
    echo
    log "Deleting snapshots"
    echo "${del_snap}" | xargs -n 1 kubectl delete snap
  else
    log "No snapshots to delete. Skiping..."
  fi
}

## Main entrypoint
main "$*"