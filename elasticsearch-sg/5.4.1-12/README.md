# ElasticSearch with SearchGuard

This is ES with SG added.  It generates a set of PKI certs so that a cluster can be made from the same set of docker images.  These certs should not be used in production!

New certs can be generated in a child docker image as follows:

```Dockerfile
FROM <this image>

RUN cd ${PKI_HOME} \
    && ./clean.sh \
    && ./generate_certs.sh
```

A new cluster can be created with this image that the old docker images cannot connect to.
