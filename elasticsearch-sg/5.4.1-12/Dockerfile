FROM quay.io/opaqnetworks/alpine:3.5-df5132f

LABEL maintainer "FourV Systems Inc <admin@fourv.com>"
LABEL revision "507c185"
EXPOSE 9200 9300

ENV ELASTIC_VERSION 5.4.1
ENV SEARCH_GUARD_VER ${ELASTIC_VERSION}-12
# ENV SG_JWT_VER 5.0-5
ENV SG_KIBANA_MT_VER 5.4-1

# PKI Settings
ENV PKI_ROOT_CA_PASS casecret
ENV PKI_TRUSTSTORE_PASS changeme
ENV PKI_KEYSTORE_PASS changeme
ENV PKI_CLIENT_PASS changeme
ENV PKI_CLIENT_DN "CN=admin,OU=client,O=client,L=test,C=US"

ENV ES_HOME /usr/share/elasticsearch
ENV PKI_HOME /usr/share/pki-scripts
ENV JAVA_HOME /usr/lib/jvm/java-1.8-openjdk
ENV PATH $ES_HOME/bin:$PATH

ENV ELASTIC_TARBALL elasticsearch-$ELASTIC_VERSION.tar.gz
ENV ELASTIC_URL https://artifacts.elastic.co/downloads/elasticsearch/$ELASTIC_TARBALL

WORKDIR $ES_HOME

RUN apk add --no-cache \
      bash \
      ca-certificates \
      openjdk8 \
      openssl \
      su-exec \
      wget \
    && update-ca-certificates \
    #### Setup ES user
    && addgroup -g 1000 -S elasticsearch \
    && adduser -S -u 1000 -G elasticsearch \
         -h /usr/share/elasticsearch elasticsearch \
    #### Install ES
    && cd $ES_HOME \
    && wget "$ELASTIC_URL" \
    && tar zxf "$ELASTIC_TARBALL" \
    && chown -R elasticsearch:elasticsearch "elasticsearch-$ELASTIC_VERSION" \
    && mv elasticsearch-${ELASTIC_VERSION}/* . \
    && rmdir "elasticsearch-$ELASTIC_VERSION" \
    && rm "elasticsearch-$ELASTIC_VERSION.tar.gz" \
    #### Install SearchGuard
    && bin/elasticsearch-plugin install -b com.floragunn:search-guard-5:${SEARCH_GUARD_VER} \
    && cd plugins/search-guard-5 \
    && wget -O dlic-search-guard-module-kibana-multitenancy-${SG_KIBANA_MT_VER}-jar-with-dependencies.jar http://search.maven.org/remotecontent?filepath=com/floragunn/dlic-search-guard-module-kibana-multitenancy/${SG_KIBANA_MT_VER}/dlic-search-guard-module-kibana-multitenancy-${SG_KIBANA_MT_VER}-jar-with-dependencies.jar

COPY config/* config/
COPY bin/* bin/
COPY dlic-search-guard-auth-http-jwt-5.0-5-SNAPSHOT-jar-with-dependencies.jar \
     plugins/search-guard-5/
ADD pki-scripts ${ES_HOME}/../pki-scripts

RUN cd ${ES_HOME} \
    && mkdir -p data config/tls \
    && chmod 0755 bin/entrypoint.sh ${PKI_HOME}/*.sh \
    # generate root cert
    && cd ${PKI_HOME} \
    && ./generate_certs.sh \
    # Fix the permissions
    && chown -R elasticsearch:elasticsearch .

CMD ["/bin/bash", "bin/entrypoint.sh"]
