#!/bin/bash

# Run Elasticsearch and allow setting default settings via env vars
#
# e.g. Setting the env var cluster.name=testcluster
#
# will cause Elasticsearch to be invoked with -Ecluster.name=testcluster
#
# see https://www.elastic.co/guide/en/elasticsearch/reference/current/settings.html#_setting_default_settings

declare -a es_opts

# Translate PKI Options
: ${ES_searchguard__ssl__transport__truststore_password:=$PKI_TRUSTSTORE_PASS}
: ${ES_searchguard__ssl__transport__keystore_password:=$PKI_KEYSTORE_PASS}
: ${ES_searchguard__ssl__transport__keystore_password:=$PKI_KEYSTORE_PASS}
: ${ES_searchguard__authcz__admin_dn__0:=$PKI_CLIENT_DN}

while IFS='=' read -r envvar_key envvar_value; do
  if [[ "$envvar_key" =~ ^ES_[a-z0-9_]+$ ]]; then
    # convert ES_foo__bar_baz to foo.bar_baz
    envvar_key=$(echo $envvar_key | sed 's/^ES_//' | sed 's/__/./g')
    echo "Setting $envvar_key $envvar_value"
    
    es_opt="-E${envvar_key}=${envvar_value}"
    es_opts+=("${es_opt}")
  fi
done < <(set)

# Fix the permissions relative to this file.
APP_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"
echo "Fixing permissions of $APP_DIR"
chown -R elasticsearch:elasticsearch $APP_DIR

# The virtual file /proc/self/cgroup should list the current cgroup
# membership. For each hierarchy, you can follow the cgroup path from
# this file to the cgroup filesystem (usually /sys/fs/cgroup/) and
# introspect the statistics for the cgroup for the given
# hierarchy. Alas, Docker breaks this by mounting the container
# statistics at the root while leaving the cgroup paths as the actual
# paths. Therefore, Elasticsearch provides a mechanism to override
# reading the cgroup path from /proc/self/cgroup and instead uses the
# cgroup path defined the JVM system property
# es.cgroups.hierarchy.override. Therefore, we set this value here so
# that cgroup statistics are available for the container this process
# will run in.
export ES_JAVA_OPTS="-Des.cgroups.hierarchy.override=/ $ES_JAVA_OPTS"

echo "Starting ES"
echo "ES_JAVA_OPTS: ${ES_JAVA_OPTS}"
echo "ARGS: ${es_opts[@]}"

su-exec elasticsearch bin/elasticsearch "${es_opts[@]}"