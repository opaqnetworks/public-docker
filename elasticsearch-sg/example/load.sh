#!/bin/bash

configpath=$(pwd)
truststore=${PKI_HOME}/truststore.jks
keystore=${PKI_HOME}/admin-keystore.jks
sgadmin=${configpath}/plugins/search-guard-5/tools/sgadmin.sh

# Generate a new client cert, if needed.
# (cd $PKI_HOME && ls -laR && ./gen_client_cert.sh)

es_cluster_host="elasticsearch"
es_cluster_name="docker-cluster"

chmod +x ${sgadmin}

sgadmin="${sgadmin} -nhnv -h \"${es_cluster_host}\" -cn \"${es_cluster_name}\""
sgadmin="${sgadmin} -ts \"${truststore}\" -tspass \"${PKI_TRUSTSTORE_PASS}\""
sgadmin="${sgadmin} -ks \"${keystore}\" -kspass \"${PKI_CLIENT_PASS}\""

set -x

#cheap way to wait for ES
max=10
count=0

while ! ${sgadmin} -cd /src; do
  code=$?
  (( count++ ))
  
  if [ "$count" -ge "$max" ]; then
    echo "too many failures, exiting"
    exit "$code"
  else
    echo "waiting to try again"
    sleep 5
  fi
done