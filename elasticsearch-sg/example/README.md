# Example

This runs a simple cluster of a Kibana instance connected to an ElasticSearch Cluster.  Both Kibana and ES have SearchGuard installed.

There is also a "load" container which loads the SG config and exits.

There is an Nginx load balancer in front of Kibana which copies a cookie into a JWT auth header.  This is because browsers do not allow the setting of raw headers.

Each of these services is directly accessible for debugging purposes:

1. Nginx, Port 80
2. Kibana, Port 5601
3. ES, Ports 9200 (http(s)), 9300 (transport)

## Generating a JWT

SG expects an HS256 JWT to be provided in the Authorization header.  The key for this needs to be "qwertyuiopasdfghjklzxcvbnm123456", and is configured in the `sg_config.yml`.  Use http://jwtbuilder.jamiekurtz.com/ to generate the JWT.

Required fields:

1. sub - Need to be the subject of the request, any text will due
    1. The JWT settings will be cached by ES.  To test a new role a new subject is be needed.
2. Roles - Needs to be a role in the `sg_roles.yml` file.

### Changing the key

To change the key edit `sg_config.yml` and change `searchguard.dynamic.authc.jwt_auth_domain.http_authenticator.config.signing_key`.  The value should be the key base64 encoded.  Be sure there are no hidden characters.

```base
$ echo -n "new key" | base64
bmV3IGtleQ==
```

## Direct ES Access

Any logged in user can print their authinfo.

```bash
$ export JWT="previously.generated.jwt"
$ curl -H "Authorization: Bearer $JWT" localhost:9200/_searchguard/authinfo?pretty
{
  "user" : "User [name=jrocket@example.com, roles=[fourv_cust1_admin]]",
  "user_name" : "jrocket@example.com",
  "user_requested_tenant" : null,
  "remote_address" : "172.21.0.1:34542",
  "sg_roles" : [
    "fourv_cust1_admin"
  ],
  "sg_tenants" : {
    "fourv_cust1" : true,
    "jrocket@example.com" : true
  },
  "principal" : null,
  "peer_certificates" : "0"
}
```

### Adding Data

An internal admin user exists that has complete access to the cluster.  This allows the adding of data to any index and type.  Any data can be added by adjusting the following:

```bash
$ curl -H "Authorization: Basic YWRtaW46YWRtaW4xMjM=" localhost:9200/events-fourv-cust1-2017-01-01/events/1 -H 'Content-Type: application/json' -d '
{
    "field1" : "value1",
    "field2" : "value2",
    "field3" : "value3"
}
'
{"_index":"events-fourv-cust1-2017-01-01","_type":"events","_id":"1","_version":1,"result":"created","_shards":{"total":2,"successful":1,"failed":0},"created":true}
```