#!/bin/bash

if [ -z "$ES_HOME" ]; then
  echo "ES_HOME must be defined as ES root directory"
  exit 1
fi

if [ -z "$PKI_ROOT_CA_PASS" ]; then
  echo "PKI_ROOT_CA_PASS is required"
  exit 1
fi

if [ -z "$PKI_TRUSTSTORE_PASS" ]; then
  echo "PKI_TRUSTSTORE_PASS is required"
  exit 1
fi

if [ -z "$PKI_KEYSTORE_PASS" ]; then
  echo "PKI_KEYSTORE_PASS is required"
  exit 1
fi

set -xe
./clean.sh
./gen_root_ca.sh "${ES_HOME}" "${PKI_ROOT_CA_PASS}" "${PKI_TRUSTSTORE_PASS}"
./gen_node_cert.sh \
  "${ES_HOME}" \
  "CN=node.example.com, OU=SSL, O=Test, L=Test, C=US" "node.example.com" \
  "${PKI_ROOT_CA_PASS}" "${PKI_KEYSTORE_PASS}"
./gen_client_cert.sh
rm -f ./*tmp*