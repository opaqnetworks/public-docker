#!/bin/bash
#########################
# 'dname' and 'ext san' have to specified on two location in this file  
# For the meaning of oid:1.2.3.4.5.5 see:
#    https://github.com/floragunncom/search-guard-docs/blob/master/architecture.md
#    https://github.com/floragunncom/search-guard-docs/blob/master/installation.md
#########################

set -xe

ES_HOME=$1
SERVER_NAME="$2"
SERVER_DNS="$3"
CA_PASS="$4"
KS_PASS="$5"

FILENAME="node"

if [ "$#" -lt 5 ]; then
  echo "$0 <install path> <cert domain> <dns> <filename base> <root ca pass> <key pass>"
  exit 1
fi

rm -f $FILENAME-keystore.jks
rm -f $FILENAME.csr
rm -f $FILENAME-signed.pem

BIN_PATH="keytool"

if [ ! -z "$JAVA_HOME" ]; then
    BIN_PATH="$JAVA_HOME/bin/keytool"
fi

echo Generating keystore and certificate for node $FILENAME

"$BIN_PATH" -genkey \
        -alias     $FILENAME \
        -keystore  $FILENAME-keystore.jks \
        -keyalg    RSA \
        -keysize   2048 \
        -validity  712 \
        -sigalg SHA256withRSA \
        -keypass $KS_PASS \
        -storepass $KS_PASS \
        -dname "$SERVER_NAME" \
        -ext san=dns:${SERVER_DNS},dns:localhost,ip:127.0.0.1,oid:1.2.3.4.5.5 
        
#oid:1.2.3.4.5.5 denote this a server node certificate for search guard

echo Generating certificate signing request for node $FILENAME

"$BIN_PATH" -certreq \
        -alias      $FILENAME \
        -keystore   $FILENAME-keystore.jks \
        -file       $FILENAME.csr \
        -keyalg     rsa \
        -keypass $KS_PASS \
        -storepass $KS_PASS \
        -dname "$SERVER_NAME" \
        -ext san=dns:${SERVER_DNS},dns:localhost,ip:127.0.0.1,oid:1.2.3.4.5.5
        
#oid:1.2.3.4.5.5 denote this a server node certificate for search guard

echo Sign certificate request with CA
openssl ca \
    -in $FILENAME.csr \
    -notext \
    -out $FILENAME-signed.pem \
    -config etc/signing-ca.conf \
    -extensions v3_req \
    -batch \
	-passin pass:$CA_PASS \
	-extensions server_ext 

echo "Import back to keystore (including CA chain)"

cat ca/chain-ca.pem $FILENAME-signed.pem | "$BIN_PATH" \
    -importcert \
    -keystore $FILENAME-keystore.jks \
    -storepass $KS_PASS \
    -noprompt \
    -alias $FILENAME
    
"$BIN_PATH" -importkeystore -srckeystore $FILENAME-keystore.jks -srcstorepass $KS_PASS -srcstoretype JKS -deststoretype PKCS12 -deststorepass $KS_PASS -destkeystore $FILENAME-keystore.p12

openssl pkcs12 -in "$FILENAME-keystore.p12" -out "$FILENAME.key.pem" -nocerts -nodes -passin pass:$KS_PASS
openssl pkcs12 -in "$FILENAME-keystore.p12" -out "$FILENAME.crt.pem" -nokeys -passin pass:$KS_PASS

echo "Copying keystore to $ES_HOME"

mkdir -p $ES_HOME/config/tls
cp $FILENAME-keystore.jks $ES_HOME/config/tls

echo All done for $FILENAME
	
