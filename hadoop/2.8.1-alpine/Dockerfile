FROM quay.io/opaqnetworks/alpine:3.5-bd0d885

LABEL maintainer "FourV Systems <admin@fourv.com>"
LABEL revision "232be2b"

RUN apk add --no-cache bash \
                       curl \
                       gnupg \
                       openjdk8 \
                       perl

# This lib is needed to install
RUN apk add --no-cache libstdc++ \
                       libc6-compat

ENV JAVA_HOME /usr/lib/jvm/java-1.8-openjdk
ENV PATH $JAVA_HOME/bin:$PATH

RUN curl -fSL https://dist.apache.org/repos/dist/release/hadoop/common/KEYS -o /tmp/KEYS
RUN gpg --import /tmp/KEYS

ENV HADOOP_VERSION 2.8.1
ENV HADOOP_INSTALL_DIR /usr/local
ENV HADOOP_PREFIX=$HADOOP_INSTALL_DIR/hadoop-$HADOOP_VERSION
ENV HADOOP_CONF_DIR=/etc/hadoop

ENV HADOOP_URL https://www.apache.org/dist/hadoop/common/hadoop-$HADOOP_VERSION/hadoop-$HADOOP_VERSION.tar.gz
RUN set -x \
    && curl -fSL "$HADOOP_URL" -o /tmp/hadoop.tar.gz \
    && curl -fSL "$HADOOP_URL.asc" -o /tmp/hadoop.tar.gz.asc \
    && gpg --verify /tmp/hadoop.tar.gz.asc \
    && tar -xvf /tmp/hadoop.tar.gz -C "$HADOOP_INSTALL_DIR" \
    && rm /tmp/hadoop.tar.gz*

RUN ln -s "$HADOOP_INSTALL_DIR/hadoop-$HADOOP_VERSION/etc/hadoop" "$HADOOP_CONF_DIR"
RUN ln -s "$HADOOP_INSTALL_DIR/hadoop-$HADOOP_VERSION" "$HADOOP_INSTALL_DIR/hadoop"
RUN cp "$HADOOP_CONF_DIR/mapred-site.xml.template" "$HADOOP_CONF_DIR/mapred-site.xml"
RUN mkdir "$HADOOP_PREFIX/logs"

RUN mkdir /hadoop-data

ENV MULTIHOMED_NETWORK=0
ENV SPARK_YARN_DYNAMIC_ALLOCATION=1

ENV USER=root
ENV PATH $HADOOP_PREFIX/bin/:$PATH

COPY entrypoint.sh /entrypoint.sh
RUN chmod a+x /entrypoint.sh

RUN apk add --no-cache perl

ENTRYPOINT ["/entrypoint.sh"]
