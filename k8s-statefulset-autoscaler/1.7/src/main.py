#!/usr/bin/python
# -*- coding: utf-8 -*-

import time
import requests
import pykube
import json
import os
import sys

from tools import *

# RUN!

os.system('echo "Start StatefulSet Autoscaler v.0.0.1"')

# python main.py namespace="default" url_heapster="http://heapster/api/v1/model" autoscaler_count="5" time_query="10"
# patch_exec = os.path.dirname(os.path.realpath(__file__)) + "/"
# api = pykube.HTTPClient(pykube.KubeConfig.from_file(patch_exec + "credentials/config"))

api = pykube.HTTPClient(pykube.KubeConfig.from_service_account())

# Arguments

list_argv = []
sys.argv.remove(sys.argv[0])
for elements in sys.argv:
    args = elements.split('=')
    if len(args) == 1 or args[1] == '':
        raise NameError('[ERROR] Invalid Arguments [python example.py var="text"]')
    list_argv.append(args)

dic_argv = argument_to_dic(list_argv)
namespace = dic_argv['namespace']
url_heapster = dic_argv['url_heapster']
autoscaler_count = dic_argv['autoscaler_count']
time_query = int(dic_argv['time_query'])

if int(autoscaler_count) < 3:
    autoscaler_count = '3'
if time_query < 5:
    time_query = '5'

# autoscaler: For add list to autoscale stateful set.
# autoscaler_percent_cpu: Autoscaling if percent CPU is greater than number.
# autoscaler_count: Number of query for the next attempt to autoscale.
# autoreduce_normal: Autoscaling for the simple HA (Regardless of masters or slaves).
# autoreduce_percent_cpu: Reduce scale if percent CPU is below than number.
# min_replicas
# max_replicas

while True:
    list_set = select_statefulset(api, namespace)
    list_set = select_pod_form_set(api, list_set, namespace,
                                   url_heapster)
    list_set = percent_cpu_set(list_set)

    pre_set = pykube.StatefulSet.objects(api).filter(namespace=namespace)
    change = 0
    set_scaling = 0

    for sfs in pre_set:
        for setfull in list_set:
            if sfs.obj['metadata']['name'] != setfull['name']:
                continue

            replicas = int(sfs.obj['spec']['replicas'])

            os.system('echo "[StatefulSet] %s - percent_cpu: %s, scale up if percent_cpu >= %s, scale down when percent_cpu <= %s"' % (sfs.name, setfull['percent_cpu'], setfull['autoscaler_percent_cpu'], setfull['autoreduce_percent_cpu']))
            try:
                if sfs.obj['metadata']['labels']['autoscaler_count'] == '0':
                    # Autoscale
                    if sfs.obj['metadata']['name'] == setfull['name'] and int(setfull['autoscaler_percent_cpu']) <= int(setfull['percent_cpu']):
                        sfs.obj['spec']['replicas'] = replicas + 1
                        sfs.obj['metadata']['labels']['autoscaler_count'] = autoscaler_count
                        set_scaling += 1
                    elif sfs.obj['metadata']['name'] == setfull['name'] and int(setfull['autoreduce_percent_cpu']) >= int(setfull['percent_cpu']) and setfull['autoreduce_normal'].lower() == 'true':
                        # Autoreduce Normal
                        sfs.obj['spec']['replicas'] = replicas - 1
                        sfs.obj['metadata']['labels']['autoscaler_count'] = autoscaler_count
                        set_scaling += 1

                    if set_scaling != 0:
                        if int(setfull['min_replicas']) <= sfs.obj['spec']['replicas'] and int(setfull['max_replicas']) >= sfs.obj['spec']['replicas']:
                            pykube.StatefulSet(api, sfs.obj).update()
                            os.system('echo "[AUTOSCALING]  %s, replicas: min - %s, max - %s "' % (sfs.obj['metadata']['name'], setfull['min_replicas'], setfull['max_replicas']))
                            os.system('echo "[AUTOSCALING]  %s, replicas: %s to %s "' % (sfs.obj['metadata']['name'], replicas, sfs.obj['spec']['replicas']))
                            change += 1
                else:
                    # Reduce only autoscaler_count
                    sfs.obj['metadata']['labels']['autoscaler_count'] = str(int(sfs.obj['metadata']['labels']['autoscaler_count']) - 1)
                    os.system('echo "[INFO] Sleep StatefulSet  %s, replicas: %s, attempts: %s"' % (sfs.obj['metadata']['name'], sfs.obj['spec']['replicas'], sfs.obj['metadata']['labels']['autoscaler_count']))
                    pykube.StatefulSet(api, sfs.obj).update()
            except:
                pass

    os.system('echo "[INFO] StatefulSet autoscaler %s"' % change)
    os.system('echo "Sleep %ss for next query"' % time_query)
    time.sleep(time_query)
