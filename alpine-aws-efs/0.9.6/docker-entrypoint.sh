#!/bin/bash

if [ -z "$AWS_ACCESS_KEY_ID" ]; then
  echo "Please double check AWS creds."
  echo "AWS_ACCESS_KEY_ID is missing."
  exit 1
fi

if [ -z "$AWS_SECRET_ACCESS_KEY" ]; then
  echo "Please double check AWS creds."
  echo "AWS_SECRET_ACCESS_KEY is missing."
  exit 1
fi

if [ -z "$AWS_DEFAULT_REGION" ]; then
  echo "Please double check AWS creds."
  echo "AWS_DEFAULT_REGION is missing."
  exit 1
fi

if [ -z "${K8S_CLUSTER_NAME}" ]; then
  echo "K8S_CLUSTER_NAME is missing."
  echo "Exiting ..."
  exit 1
fi

if [ -z "${NAMESPACE}" ]; then
  echo "NAMESPACE is missing."
  echo "Exiting ..."
  exit 1
fi

if [ -z "${ACTION}" ]; then
  echo "ACTION is missing."
  echo "Available actions: install, uninstall"
  echo "Exiting ..."
  exit 1
fi

function aws_efs_call(){
  aws efs $@
}

function get_efs_id_by_uuid(){
  local uuid=$1
  aws_efs_call describe-file-systems --creation-token ${uuid} --query 'FileSystems[].FileSystemId' --output text
}

function describe_mount_targets(){
  local efs_id=$1
  local query=$2
  aws_efs_call describe-mount-targets --file-system-id ${efs_id} --query ''"MountTargets[].${query}"'' --output text
}

function aws_cf_call(){
  aws cloudformation $@
}

function get_physical_resource_id(){
  local stack_name=$1
  local logical_resource_id=$2
  aws_cf_call describe-stack-resources --stack-name ${stack_name} --query 'StackResources[?LogicalResourceId==`'"${logical_resource_id}"'`].PhysicalResourceId' --output=text
}

function modify_sg_ingress(){
  local sg_id=$1
  local protocol=$2
  local port=$3

  #check if port already there or not
  test -z $(aws ec2 describe-security-groups --group-ids ${sg_id} --query 'SecurityGroups[].IpPermissions[?ToPort==`'"${port}"'`].ToPort' --output text);
  if [ $? -eq 0 ]; then
    aws ec2 authorize-security-group-ingress --group-id ${sg_id} --protocol ${protocol} --port ${port} --source-group ${sg_id}
    if [ $? -eq 0 ]; then
      echo "Port ${port} has been allowed in Security Group ${sg_id}."
    else
      echo
      echo "Failed. Unable to add port!"
      return 1
    fi
  else
    echo "Rule to allow traffic for port ${port} already existed. "
    return 0
  fi
}

function create_efs(){
  local uuid=$1
  aws_efs_call create-file-system --creation-token ${uuid} --query 'FileSystemId' --output text
}

function delete_efs(){
  local efs_id=$1
  echo "Attempt to delete EFS_ID \"${EFS_ID}\""
  aws_efs_call delete-file-system --file-system-id ${efs_id}
  if [ $? -ne 0 ]; then
    echo "Unable to complete 'delete efs' request."
    echo "Please check EFS service in AWS Management Console"
    return 1
  fi

}

function wait_for_available(){
  local efs_id=$1
  local state
  state=$(aws_efs_call describe-file-systems --file-system-id ${efs_id} --query 'FileSystems[].LifeCycleState' --output text)
  until [ "${state}" == "available" ]; do
    sleep 5;
    echo "EFS Life Cycle State: ${state}"
    state=$(aws_efs_call describe-file-systems --file-system-id ${efs_id} --query 'FileSystems[].LifeCycleState' --output text)
  done
  echo "EFS Life Cycle State: ${state}"
}

function wait_for_zero_mount_targets(){
  local efs_id=$1
  local count
  count=$(aws_efs_call describe-file-systems --file-system-id ${efs_id} --query 'FileSystems[].NumberOfMountTargets' --output text)
  until [ $count -eq 0 ]; do
    sleep 5;
    echo "Number of mount targets: $count"
    count=$(aws_efs_call describe-file-systems --file-system-id ${efs_id} --query 'FileSystems[].NumberOfMountTargets' --output text)
  done
  echo "Number of mount targets: $count"
}

function create_mount_target(){
  local efs_id=$1
  local subnet_id=$2
  local sg_id=$3
  aws_efs_call create-mount-target --file-system-id ${efs_id} --subnet-id ${subnet_id} --security-group ${sg_id}
  if [ $? -ne 0 ]; then
    echo "Unable to create mount target."
    echo "Exiting ... "
    return 1
  fi
}

function delete_mount_target(){
  local mount_target=$1
  echo "Attempt to delete mount_target \"${mount_target}\" for EFS_ID: ${EFS_ID}"
  aws_efs_call delete-mount-target --mount-target-id ${mount_target}
  if [ $? -ne 0 ]; then
    echo "Unable to delete mount target: \"${mount_target}\""
    return 1
  fi
}

function create_efs_tag(){
  local efs_id=$1
  local key=$2
  local value=$3
  echo "Adding ${key} tag to newly-created EFS."
  aws_efs_call create-tags --file-system-id ${efs_id} --tags Key=${key},Value=${value}
  if [ $? -eq 0 ]; then
    echo "Tag \"${key}\" with value \"${value}\" has been created."
  else
    echo "Failed to create tag."
    return 1
  fi
}

function create_aws_tags_based_on_env_vars(){
  local aws_tags=($(env | grep "AWS_TAG_"))
  if [ "${#aws_tags[@]}" -ne 0 ]; then
    for tag in "${aws_tags[@]}"; do
      key=$(echo $tag | awk -F= '{print$1}' | sed -e 's/AWS_TAG_//')
      value=$(echo $tag | awk -F= '{print$2}')
      create_efs_tag ${EFS_ID} ${key} ${value}
    done
  fi
}

function check_and_bail_if_efs_exists(){
  if [ ! -z ${EFS_ID} ]; then
    echo "EFS with name ${UUID} already exists."
    echo "Trying to get IP address ... "
    EFS_IP_ADDRESS=$(describe_mount_targets ${EFS_ID} IpAddress)
    if [ ! -z "$EFS_IP_ADDRESS" ]; then
      echo "========================================================================="
      echo "EFS IP Address: ${EFS_IP_ADDRESS}"
      echo "========================================================================="
      create_aws_tags_based_on_env_vars
      exit 0
    else
      echo "Unable to get IP address of ${EFS_ID}".
      echo "Please check EFS service in AWS Management Console"
      return 1
    fi
  else
    return 0
  fi
}

function fetch_info_from_stack(){
  echo "Attempt to fetch info about resource details from aws ..."
  WORKER_STACK_NAME=$(aws_cf_call describe-stacks --output text | grep ${K8S_CLUSTER_NAME} | grep -m1 ControlPlaneStackName | awk '{print$3}')

  if [ -z "${WORKER_STACK_NAME}" ]; then
    echo "Unable to find cloudformation stack that could be related to \"${K8S_CLUSTER_NAME}\""
    return 1
  fi

  echo "========================================================================="
  echo "Worker Stack Name: ${WORKER_STACK_NAME}"

  SG_ID=$(get_physical_resource_id ${WORKER_STACK_NAME} SecurityGroupWorker)
  VPC_ID=$(get_physical_resource_id ${WORKER_STACK_NAME} VPC)
  SUBNET_ID=$(get_physical_resource_id ${WORKER_STACK_NAME} Subnet0)

  echo "Security Group: ${SG_ID}"
  echo "VPC ID: ${VPC_ID}"
  echo "Subnet ID: ${SUBNET_ID}"
  echo "========================================================================="
}

function install_efs(){
  export AWS_TAG_Name=$UUID
  export AWS_TAG_KubernetesCluster=${K8S_CLUSTER_NAME}
  check_and_bail_if_efs_exists && \
    fetch_info_from_stack && \
    echo "Attempt to create EFS ... "
    EFS_ID=$(create_efs ${UUID})
    if [ -z ${EFS_ID} ]; then
      echo "Failed to create new EFS."
      echo "Exiting ... "
      return 1
    fi

  echo "EFS_ID: ${EFS_ID}"
  wait_for_available ${EFS_ID}
  create_aws_tags_based_on_env_vars &&\
    create_mount_target ${EFS_ID} ${SUBNET_ID} ${SG_ID}
    if [ $? -eq 0 ]; then
      EFS_IP_ADDRESS=$(describe_mount_targets ${EFS_ID} IpAddress)
      echo "========================================================================="
      echo "EFS IP Address: ${EFS_IP_ADDRESS}"
      modify_sg_ingress ${SG_ID} tcp 2049
      echo "========================================================================="
    fi
}

function uninstall_efs(){
  if [ ! -z "${EFS_ID}" ]; then
    echo "EFS_ID \"${EFS_ID}\" has been found."
    local mount_target=$(describe_mount_targets ${EFS_ID} MountTargetId)
    if [ ! -z "${mount_target}" ]; then
      echo "Starting deletion procedure ..."
      delete_mount_target ${mount_target} &&\
        wait_for_zero_mount_targets ${EFS_ID}
        delete_efs ${EFS_ID} &&\
        echo "EFS: ${UUID} (${EFS_ID}) has been deleted successfully."
    fi
  else
    echo "Unable to find EFS_ID that could be related to \"${UUID}\""
    echo "Please check EFS service in AWS Management Console if you believe that EFS should be existed"
    echo "Nothing to do this time. Exiting ..."
    return 0
  fi
}

echo
echo "========================================================================="
echo "Cluster Name: ${K8S_CLUSTER_NAME}"
echo "Namespace Name: ${NAMESPACE}"
echo "========================================================================="
UUID="${K8S_CLUSTER_NAME}-${NAMESPACE}"

#Check and remove env variables for AWS_TAG_Name, since this tag:Name is used by k8s.
if [ ! -z "${AWS_TAG_Name}" ]; then
  echo "tag:Name is used by k8s and can not be defined as custom tag."
  unset AWS_TAG_Name
fi

echo
EFS_ID=$(get_efs_id_by_uuid ${UUID})

case $ACTION in
  install)
    install_efs
    ;;
  uninstall)
    uninstall_efs
    ;;
  *)
    echo "Available actions: install, uninstall"
    exit 1
    ;;
esac
