import os
import unittest
import logging

from druid_exporter.collector import DruidCollector
from druid_exporter.exporter import YamlEnvLoader
from druid_exporter.model import Config
from prometheus_client.exposition import generate_latest as collect
import json

log = logging.getLogger(__name__)
logging.basicConfig(format="%(asctime)s [%(levelname)5s] %(message)s", level="DEBUG")


class DruidPromExporterTest(unittest.TestCase):

    def test_01_yaml_with_envs(self):
        with open("./data/1/test-config.yaml") as f:
            yaml = YamlEnvLoader.load(f)
        self.assertEqual(yaml["metrics_defaults"]["labels"]["opaq_component_name"], "${DRUID_COMPONENT_NAME}")

        os.environ["DRUID_COMPONENT_NAME"] = "historical"
        with open("./data/1/test-config.yaml") as f:
            yaml = YamlEnvLoader.load(f)

        log.info(yaml)
        self.assertEqual(yaml["metrics_defaults"]["labels"]["opaq_component_name"], "historical")
        config = Config(yaml)

        self.assertEqual(config.metrics[0].labels["opaq_component_name"], "historical")

    def test_02(self):
        os.environ["DRUID_COMPONENT_NAME"] = "historical"
        config = self.config("1")
        collector = DruidCollector()
        log.info(collect(collector))
        for datapoint in json.loads(self.text_file("1/input-01.json")):
            collector.register_datapoint(datapoint, config)

        metrics = collect(collector)
        expected_metrics = self.text_file("1/output-test-02.txt")
        log.info(metrics)
        self.assertEqual(expected_metrics, metrics)

    def test_03(self):
        from prometheus_client import make_wsgi_app, REGISTRY

        os.environ["DRUID_COMPONENT_NAME"] = "historical"
        config = self.config("1")
        collector = DruidCollector()

        REGISTRY.register(collector)

        for datapoint in json.loads(self.text_file("1/input-01.json")):
            collector.register_datapoint(datapoint, config)

        http_environ = {"REQUEST_METHOD": "GET", "PATH_INFO": "/metrics"}

        def start_response(status, headers):
            log.info("start_response: {}, {}".format(status, headers))

        prometheus_app = make_wsgi_app()

        # metrics = collect2(collector)

        metrics = prometheus_app(http_environ, start_response)
        expected_metrics = self.text_file("1/output-test-03.txt")
        log.info(metrics[0])

        REGISTRY.unregister(collector)
        self.assertEqual(expected_metrics, metrics[0])

    def test_04(self):
        from prometheus_client import make_wsgi_app, REGISTRY

        os.environ["DRUID_COMPONENT_NAME"] = "historical"
        config = self.config("2")
        collector = DruidCollector()

        REGISTRY.register(collector)

        for datapoint in json.loads(self.text_file("2/input-01.json")):
            collector.register_datapoint(datapoint, config)

        for datapoint in json.loads(self.text_file("2/input-02.json")):
            collector.register_datapoint(datapoint, config)

        http_environ = {"REQUEST_METHOD": "GET", "PATH_INFO": "/metrics"}

        def start_response(status, headers):
            log.info("start_response: {}, {}".format(status, headers))

        prometheus_app = make_wsgi_app()

        metrics = prometheus_app(http_environ, start_response)
        expected_metrics = self.text_file("2/output-test-04.txt")
        log.info(metrics[0])

        REGISTRY.unregister(collector)
        self.assertEqual(expected_metrics, metrics[0])

    def config(self, path):
        self.assertNotEqual(self, None)
        with open("./data/{}/test-config.yaml".format(path)) as f:
            c = YamlEnvLoader.load(f)
        return Config(c)

    def text_file(self, filename):
        self.assertNotEqual(self, None)
        with open("./data/{}".format(filename)) as f:
            return f.read()
