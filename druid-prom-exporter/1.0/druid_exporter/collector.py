import logging
import re

from prometheus_client import metrics_core
from collections import defaultdict
from prometheus_client.core import CounterMetricFamily
from model import Metric

log = logging.getLogger(__name__)


class DruidCollector(object):
    def __init__(self):
        """
        DruidCollector
        """
        self.all_metrics = defaultdict(lambda: defaultdict(lambda: {}))
        self.datapoints_registered = 0
        # patching prometheus_client with better regex
        metrics_core.METRIC_NAME_RE = re.compile(r'^[a-zA-Z_:][a-zA-Z0-9_:/]*$')

    def collect(self):
        for v1 in self.all_metrics.values():
            for (group_names, v2) in v1.items():
                metric = v2.values()[0]
                metric_class = metric.metric_class
                prometheus_metric = metric_class(metric.name,
                                                 metric.description,
                                                 labels=list(metric.label_names))
                for (group_values, metric) in v2.items():
                    prometheus_metric.add_metric(list(metric.label_values), metric.value)
                yield prometheus_metric

        registered = CounterMetricFamily(
            'druid_exporter_datapoints_registered',
            'Number of datapoints successfully registered by the exporter.')
        registered.add_metric([], self.datapoints_registered)
        yield registered

    def register_datapoint(self, datapoint, config):
        metric_name = str(datapoint.get('metric', None))

        is_known_metric = metric_name in config.metric_names
        if not is_known_metric and not config.use_unknown_metrics:
            log.debug("Metric '{}' is unknown".format(metric_name))
            return

        metric_info = config.get_metric(metric_name)
        if len(metric_info.required_properties - set(datapoint.keys())) > 0:
            log.debug("Metric '{}' is missing required properties".format(metric_name))
            return

        metric = Metric(metric_info, datapoint)
        self.all_metrics[metric.name][metric.group_names][metric.group_values] = metric
        self.datapoints_registered = self.datapoints_registered + 1
        log.debug("Metric '{}' is successfully registered as {}".format(metric_name, metric_info.get_name()))
