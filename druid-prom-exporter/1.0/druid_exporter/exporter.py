import json
import logging
import os
import re
import sys
import yaml
import time

from collector import DruidCollector
from prometheus_client import make_wsgi_app, REGISTRY
from wsgiref.simple_server import make_server

from model import Config

log = logging.getLogger(__name__)


class DruidWSGIApp(object):
    def __init__(self, post_uri, druid_collector, prometheus_app, encoding, config):
        """
        DruidWSGIApp
        """
        self.prometheus_app = prometheus_app
        self.druid_collector = druid_collector
        self.post_uri = post_uri
        self.encoding = encoding
        self.config = config

    def __call__(self, environ, start_response):
        if environ['REQUEST_METHOD'] == 'GET' and environ['PATH_INFO'] == '/metrics':
            log.debug('Exporting metrics to Prometheus')
            return self.prometheus_app(environ, start_response)
        elif (environ['REQUEST_METHOD'] == 'POST'
              and environ['PATH_INFO'] == self.post_uri
              and environ['CONTENT_TYPE'] == 'application/json'):
            try:
                log.debug('Collecting metrics from Druid')
                request_body_size = int(environ.get('CONTENT_LENGTH', 0))
                log.debug('Content-Length: {}'.format(request_body_size))

                request_body = environ['wsgi.input'].read(request_body_size)
                decoded_data = request_body.decode(self.encoding)
                log.debug(decoded_data)

                datapoints = json.loads(decoded_data)

                # The HTTP metrics emitter can batch datapoints and send them to
                # a specific endpoint stated in the logs (this tool).
                for datapoint in datapoints:
                    self.druid_collector.register_datapoint(datapoint, self.config)
                status = '200 OK'
            except Exception as e:
                log.exception('Error processing POST data: {}'.format(e))
                status = '400 Bad Request'
        else:
            status = '400 Bad Request'
        start_response(status, [])
        return []


#
# Creates yaml from input stream and resolves all found env variables in text nodes (both in keys and in values):
#    some_key: "Home folder is ${HOME}"
# become
#    some_key: "Home folder is /Users/vitaliy_mykytenko/"
#
class YamlEnvLoader(yaml.SafeLoader):
    env_matcher = re.compile(r'\${([^}^{]+)}')
    environ = os.environ

    def __init__(self, stream):
        """
        YamlEnvLoader
        """
        super(YamlEnvLoader, self).__init__(stream)
        YamlEnvLoader.add_constructor('tag:yaml.org,2002:str', YamlEnvLoader.construct_env_var_str)

    def construct_env_var_str(self, node):
        text = self.construct_yaml_str(node)
        envs = self.env_matcher.findall(text)
        for key in envs:
            if key in self.environ:
                text = text.replace("${{{}}}".format(key), self.environ[key])
            else:
                log.warn("Env '{}' not found in text '{}'.".format(key, text))
        return text

    @staticmethod
    def load(stream):
        return yaml.load(stream, YamlEnvLoader) # nosec


def main():
    config_file = ('./config.yaml' if len(sys.argv) == 1 else sys.argv[1])
    with open(config_file) as f:
        c = YamlEnvLoader.load(f)

    config = Config(c)

    log_level = logging.getLevelName(config.connection.log_level)
    logging.basicConfig(level=log_level)

    logging.info('Config:\n{}'.format(yaml.dump(c)))

    druid_collector = DruidCollector()
    REGISTRY.register(druid_collector)
    prometheus_app = make_wsgi_app()
    druid_wsgi_app = DruidWSGIApp(
        config.connection.uri,
        druid_collector,
        prometheus_app,
        config.connection.encoding,
        config)

    for _ in range(config.connection.connect_attempts):
        httpd = None
        try:
            log.info("Creating server. (attempt {0} out of {1})".format(_, config.connection.connect_attempts))
            httpd = make_server(config.connection.address, config.connection.port, druid_wsgi_app)
            httpd.serve_forever()

        except Exception as err:
            log.exception("Error: {0}".format(err))
        finally:
            try:
                if httpd is not None:
                    httpd.server_close()
            except Exception as err:
                log.exception("Error during closing failed server: {0}".format(err))

            log.info("Sleeping {0} second(s)".format(config.connection.sleep_seconds_after_disconnection))
            time.sleep(config.connection.sleep_seconds_after_disconnection)


if __name__ == '__main__':
    sys.exit(main())
