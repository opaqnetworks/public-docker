from prometheus_client.core import (CounterMetricFamily, GaugeMetricFamily)
import logging

log = logging.getLogger(__name__)


class Metric(object):
    def __init__(self, metric_info, datapoint):
        """
        Metric
        """
        self.name = metric_info.get_name()
        self.type = metric_info.type
        self.description = metric_info.description or ""
        self.value = float(datapoint['value'])
        labels = {
            k: str(v)
            for (k, v) in datapoint.items()
            if k in metric_info.labels_whitelist
        }
        labels.update(metric_info.labels)
        label_names = sorted([str(k) for k in labels.keys()])
        self.label_names = tuple(label_names)
        self.label_values = tuple([labels[k] for k in label_names])

        self.group_names = tuple([str(k) for k in label_names if k != 'timestamp'])
        self.group_values = tuple([labels[k] for k in self.group_names])

        if self.type == "counter":
            self.metric_class = CounterMetricFamily
        elif self.type == "gauge":
            self.metric_class = GaugeMetricFamily
        else:
            log.warn("Unsupported type {}. Class {} will be used".format(
                self.type, GaugeMetricFamily))
            self.metric_class = GaugeMetricFamily


class Config(object):
    def __init__(self, obj):
        """
        Config
        """
        def to_bool(text):
            return text.lower() == "true"

        self.connection = Connection(obj["connection"])
        self.use_unknown_metrics = to_bool(obj["use_unknown_metrics"])

        metric_defaults = obj["metrics_defaults"]
        self.default = MetricsInfo(metric_defaults, metric_defaults)
        self.metrics = [
            MetricsInfo(i, metric_defaults) for i in obj["metrics"]
        ]
        self.metric_names = set([i.metric for i in self.metrics])

    def get_metric(self, metric_name):
        return next((i for i in self.metrics if i.metric == metric_name), self.default)


class MetricsInfo(object):
    def __init__(self, obj, defaults):
        """
        MetricsInfo
        """
        def nvl(key_name, obj1, obj2):
            return obj1.get(key_name, obj2.get(key_name, None))

        self.metric = obj.get("metric", None)
        self.type = nvl("type", obj, defaults)
        if self.type not in {"counter", "gauge"}:
            log.warn("Configuration issue: type [{}] is not supported. Object: {}".format(self.type, obj))
        self.description = nvl("description", obj, defaults)
        self.required_properties = set(nvl("required_properties", obj, defaults))
        self.labels = dict(nvl("labels", obj, defaults))
        self.labels_whitelist = set(nvl("labels_whitelist", obj, defaults))\
            .union(self.labels.keys())
        self.metric_name = obj.get("metric_name", None)

    def get_name(self):
        return self.metric_name or str(self.metric).replace("/", "_").lower()


class Connection(object):
    def __init__(self, obj):
        """
        Connection
        """
        self.address, port = obj["address"].split(':', 1)
        self.port = int(port)
        self.uri = obj["uri"]
        self.encoding = obj["encoding"]
        self.log_level = obj["log_level"].upper()
        self.connect_attempts = int(obj.get("connect_attempts", 1))
        self.sleep_seconds_after_disconnection = int(obj.get("sleep_seconds_after_disconnection", 0))
