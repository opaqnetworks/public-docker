#!/bin/bash

# Run Kibana, using environment variables to set longopts defining Kibana's
# configuration.
#
# eg. Setting the environment variable:
#
#       ES_elasticsearch__startupTimeout=60
#
# will cause Kibana to be invoked with:
#
#       --elasticsearch.startupTimeout=60

KIBANA_CONF_FILE=/usr/share/kibana/config/kibana.yml
echo "" >> $KIBANA_CONF_FILE

while IFS='=' read -r envvar_key envvar_value; do
  echo "$envvar_key -> $envvar_value"
  if [[ "$envvar_key" =~ ^CONF_[a-zA-Z_]+$ ]]; then
    # convert ES_foo__bar_baz to foo.bar_baz
    envvar_key=$(echo $envvar_key | sed 's/^CONF_//' | sed 's/__/./g')
    echo "Setting $envvar_key $envvar_value"
    
    echo "${envvar_key}: '${envvar_value}'" >> $KIBANA_CONF_FILE
  fi
done < <(env)

cat "$KIBANA_CONF_FILE"

# The virtual file /proc/self/cgroup should list the current cgroup
# membership. For each hierarchy, you can follow the cgroup path from
# this file to the cgroup filesystem (usually /sys/fs/cgroup/) and
# introspect the statistics for the cgroup for the given
# hierarchy. Alas, Docker breaks this by mounting the container
# statistics at the root while leaving the cgroup paths as the actual
# paths. Therefore, Kibana provides a mechanism to override
# reading the cgroup path from /proc/self/cgroup and instead uses the
# cgroup path defined the configuration properties
# cpu.cgroup.path.override and cpuacct.cgroup.path.override.
# Therefore, we set this value here so that cgroup statistics are
# available for the container this process will run in.

exec /usr/share/kibana/bin/kibana --cpu.cgroup.path.override=/ --cpuacct.cgroup.path.override=/