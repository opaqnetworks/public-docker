#!/bin/bash

echo "Starting Logstash"
echo "LS_JAVA_OPTS: ${LS_JAVA_OPTS}"

exec bin/logstash -f /usr/share/logstash/config-local/logstash.conf
