#!/bin/bash

if [ -z "$1" ]; then
  echo "A TSL endpoint IP or DNS is required."
  exit 1
fi

sed "s/--TSL_IP--/$1/" /etc/syslog-ng/syslog-ng.conf.tmpl > /etc/syslog-ng/syslog-ng.conf

echo "Running syslog with the following config"
cat /etc/syslog-ng/syslog-ng.conf
echo
echo "Accepting regular syslog (port 514)"

/usr/sbin/syslog-ng -F -f /etc/syslog-ng/syslog-ng.conf