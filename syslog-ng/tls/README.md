# TLS Syslog forwarder

This docker container is designed to accept standard syslog and forward it using TLS.  Currently this version accepts untrusted keys, and should not be used in production.

```
docker run -ti -p 514:514 fourv/syslog-ng:tls-<version> <TSL Remote IP>
```

Now send TCP syslog to `localhost:514`.

## Comparing streams

All the logs that are sent to the TSL endpoint are also captured in `/var/log/syslog-ng/messages.log`.  You can export that file to ensure all message send were actually received by the container and forwarded along.

```
docker run -ti -p 514:514 -v `pwd`:/var/log/syslog-ng fourv/syslog-ng:tls-<version> <TSL Remote IP>
```