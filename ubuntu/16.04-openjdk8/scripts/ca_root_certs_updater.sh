#!/bin/bash

if [ -f /etc/os-release ]; then
  RELESE_FILE=$(grep ^ID= /etc/os-release | cut -f2 -d'=' | tr -d '"')
elif [ -f /etc/lsb-release ]; then
  RELESE_FILE=$(grep DISTRIB_ID /etc/lsb-release | cut -f2 -d'=')
fi

function check_if_update-ca-certificates_installed(){
  which update-ca-certificates
  if [ $? -ne 0 ]; then
    if [ "$RELESE_FILE" == "ubuntu" -o "$RELESE_FILE" == "debian" ]; then
      apt-get update && apt-get install -y ca-certificates
    else
      apk add --update --no-cache ca-certificates
    fi
  fi
}

function update_ca_root_certs(){
  check_if_update-ca-certificates_installed &&\
    echo "Attempt to update CA roots certs for $RELESE_FILE..."
    update-ca-certificates --fresh
    if [ $? -eq 0 ]; then
      echo "CA roots certs have been updated successfully"
    else
      echo "ERROR: CA roots certs have not been updated."
      return 1
    fi
}

function check_if_java_installed(){
  which java || return $?
}

function update_ca_root_certs_for_java_ubuntu(){
  check_if_java_installed
  if [ $? -ne 1 ]; then
    echo "Attempt to update CA roots certs for java ..."
    check_if_update-ca-certificates_installed
    /var/lib/dpkg/info/ca-certificates-java.postinst configure
    if [ $? -eq 0 ]; then
      echo "CA roots certs for java have been updated successfully"
    else
      echo "ERROR: CA roots certs for java have not been updated."
      return 1
    fi
  else
    echo "Java is not installed or not in \$PATH."
    echo "Skipping this step now ... "
  fi
}

case $RELESE_FILE in
  ubuntu|debian)
    echo "OS: $RELESE_FILE"
    update_ca_root_certs &&\
      update_ca_root_certs_for_java_ubuntu
    ;;
  alpine)
    echo "OS: $RELESE_FILE"
    update_ca_root_certs
    ;;
  *)
    echo "ERROR: Unable to detect Linux Distribution."
    exit 1
    ;;
esac
