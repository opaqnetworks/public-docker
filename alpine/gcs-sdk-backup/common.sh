#!/bin/bash

## Standard flags
HELP=${HELP:-""}
DEBUG=${DEBUG:-""}
DRY_RUN=${DRY_RUN:-""}

## Logging
function log_stdout(){
  printf "%b\n" "-- ${*}"
}

function log_stderr(){
  printf "%b\n" "-- ${*}" >&2
}

function header(){
  log_stdout "========  ${*}  ========"
}


function info(){
  log_stdout "INFO: ${*}"
}

function warn(){
  log_stdout "WARN: ${*}"
}

function error(){
  log_stderr "ERROR: ${*}"
}

function fatal(){
  error "${*}"
  exit 1
}

function debug(){
  if [ -z "${DEBUG}" ]; then
    return 0
  fi
  log_stdout "DEBUG: ${*}"
}

## Parse standard arguments
function set_standard_args(){
  debug "set_standard_args ${*}"
  local OPTIND
  while getopts ":hnv" opt "${@}"; do
    debug "Opt: ${opt}"
    case "${opt}" in
      h)
        HELP=true
        debug "set variable HELP=${HELP}"
        ;;
      n)
        DRY_RUN=true
        debug "set variable DRY_RUN=${DRY_RUN}"
        ;;
      v)
        if [ "${DEBUG}" = "true" ]; then
          set -x  # enable bash-level debug with -vv
        fi
        DEBUG=true
        debug "set variable DEBUG=${DEBUG}"
        ;;
      \?)
        debug "Option -${OPTARG} added to the OTHER_ARGS array"
        OTHER_ARGS+=("-${OPTARG}")  # use with local getopts func: set_local_args "${OTHER_ARGS[@]}"
        ;;
    esac
  done

  if [[ -z "${OTHER_ARGS[*]}" ]]; then
    return 0
  else
    debug "OTHER_ARGS: ${OTHER_ARGS[*]}"
    return 1
  fi
}

## Check required environment variables
function check_required_envs(){
  debug "check_required_envs: ${*}"
  local exit_status=0
  for i in "${@}"; do
    debug "checking ${i} has a value"

    if [ -z "${!i}" ]; then
      error "${i} is not defined"
      exit_status=1
    fi
  done
  debug "exit_status: ${exit_status}"
  return ${exit_status}
}

## Check required commands
function check_required_cmds() {
  debug "check_required_cmds: ${*}"
  local exit_status=0
  for i in "${@}"; do
    debug "checking if $i is installed"

    if ! [ -x "$(command -v "${i}")" ]; then
      error "${i} is not installed."
      exit_status=1
    fi

  done
  debug "exit_status: $exit_status"
  return $exit_status
}

## Convert base64-encoded value to file and check if it's valid json
function base64_to_file {
  echo -n "${1}" | base64 -d >"${2}"
  if ! < "${2}" jq -e . &>/dev/null; then
    fatal "${2} is not valid json file"
  fi
}

## Activate service account
function activate_sa() {
  gcloud auth activate-service-account --key-file "${1}"
}

## Deactivate service account
function deactivate_sa() {
  local sa
  sa=$(gcloud auth list 2>&1 | grep '^\*' | awk '{print $2}')
  gcloud auth revoke "${sa}" 2>/dev/null
}

## Check if bucket exists
function check_bucket() {
  if ! gsutil ls -l "gs://${1}/" &>/dev/null; then
    error "bucket \"${1}\" does not exist or cannot be listed."
    return 1
  fi
}