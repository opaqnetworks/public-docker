#!/usr/bin/env bash

# shellcheck disable=SC1091
# shellcheck source=common.sh
source "${LOCAL_LIB}/common.sh"

## Define global variables
REQ_ENV_VARS=(SRC_BUCKET DST_BUCKET)
REQ_CMDS=(gcloud gsutil)
SA_KEY="sa-key.json"

## Local flags
FORCE_BACKUP=${FORCE_BACKUP:-""}

## Define functions
function main() {
  set_standard_args "${@}" || set_local_args "${OTHER_ARGS[@]}" || { usage && exit 1; }
  [[ -n "$HELP" ]] && { usage && exit 0; }
  check_required_envs "${REQ_ENV_VARS[@]}" || { usage && exit 1; }
  check_required_cmds "${REQ_CMDS[@]}" || { usage && exit 1; }

  info "Source buckets: ${SRC_BUCKET}"
  info "Destination bucket: ${DST_BUCKET}"

  if [[ -n ${GOOGLE_APPLICATION_CREDENTIALS_BASE64} ]]; then
    base64_to_file "${GOOGLE_APPLICATION_CREDENTIALS_BASE64}" "${SA_KEY}"
    activate_sa "${SA_KEY}"
    rm "${SA_KEY}"
    backup
    deactivate_sa
  else
    backup
  fi
}

function set_local_args(){
  debug "set_local_args ${*}"
  local OPTIND
  while getopts ":f" opt "${@}"; do
    debug "Opt: ${opt}"
    case "${opt}" in
      f)
        FORCE_BACKUP=true
        debug "set variable FORCE_BACKUP=${FORCE_BACKUP}"
        ;;
      \?)
        error "Invalid option -${OPTARG}"
        return 1
        ;;
    esac
  done
}

function usage() {
  cat <<-EOF
DESCR:
======
This script will rsync content of source bucket into subfolder at destination

USAGE:
======
Set following environment variables before running script:

SRC_BUCKET - Comma-separated list of source buckets to rsync files from         # REQUIRED
DST_BUCKET - Destination bucket to rsync files to                               # REQUIRED
GOOGLE_APPLICATION_CREDENTIALS_BASE64 - Base64-encoded google service account   # OPTIONAL

echo "Args:"
echo "  -h - This help message"
echo "  -n - Enable dry-run mode"
echo "  -v - Enable verbose output"
echo "  -f - Force daily backup"

EOF
}

function check_backup() {
  local fld bkp
  fld=${*%\/*}                      # strip backup name
  bkp=${*##*\/}                     # keep backup name
  bkp=${bkp%T*}                     # strip hours/minutes/seconds from backup name

  debug "check_backup: list folder gs://${fld}"
  debug "check_backup: grep backup ${bkp}"
  info "checking if todays backup exists"

  if grep -q "${bkp}" < <(gsutil ls "gs://${fld}" 2>/dev/null); then
    error "\"gs://${db}/${bp}\" contains todays backup. skipping..."
    info "backup can be forced with -f flag"
    return 1
  fi
}

function sync_backup() {
  info "syncing ${1} bucket"
  gsutil -m rsync -r "gs://${1}" "gs://${2}"
}

function backup() {
  local sb db ts bp
  sb=${SRC_BUCKET//,/ }             # replace commas with spaces
  sb=${sb//gs:\/\//}                # strip bucket scheme. keep sub-path. sub-path is allowed in SRC_BUCKET
  db=${DST_BUCKET//gs:\/\//}        # strip bucket scheme
  db=${db%%\/*}                     # strip sub-path. sub-path is not allowed in DST_BUCKET
  ts=$(date +'%Y-%m-%dT%H_%M_%SZ')  # ISO 8601 time format with colons replaced by undescores. e.g 2020-05-29T12_01_04Z

  for bucket in ${sb} ${db}; do
    check_bucket "${bucket}" || exit 1
  done

  for bucket in ${sb}; do
    bp=${bucket//\//_}              # replace slashes with underscores to keep flat folder structure
    debug "source: gs://${bucket}; destination: gs://${db}/${bp}/${ts}"

    if [ "${DRY_RUN}" = "true" ]; then
      info "skipping ${bucket} bucket sync for dry-run"
    else
      if [ "${FORCE_BACKUP}" = "true" ]; then
        warn "forced backup. skipping todays backup check"
        sync_backup "${bucket}" "${db}/${bp}/${ts}"
      else
        check_backup "${db}/${bp}/${ts}" && sync_backup "${bucket}" "${db}/${bp}/${ts}"
      fi
    fi
  done
}

## Entrypoint
main "${@}"
