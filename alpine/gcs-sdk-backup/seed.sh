#!/usr/bin/env bash

# shellcheck disable=SC1091
# shellcheck source=common.sh
source "${LOCAL_LIB}/common.sh"

## Define global variables
REQ_ENV_VARS=(SRC_BUCKET TARGET_BUCKET)
REQ_CMDS=(gcloud gsutil)
SA_KEY="sa-key.json"
DAYSECONDS=86400
TIMESTAMP=$(date +'%Y-%m-%dT%H:%M:%SZ')
BACKUP_START_DATE=${BACKUP_START_DATE:-$TIMESTAMP}
BACKUP_DAY_COUNT=${BACKUP_DAY_COUNT:-100}

## Define functions
function main() {
  set_standard_args "${@}" || { usage && exit 1; }
  [[ -n "$HELP" ]] && { usage && exit 0; }
  check_required_envs "${REQ_ENV_VARS[@]}" || { usage && exit 1; }
  check_required_cmds "${REQ_CMDS[@]}" || { usage && exit 1; }

  info "Source buckets: ${SRC_BUCKET}"
  info "Target bucket: ${TARGET_BUCKET}"

  if [[ -n ${GOOGLE_APPLICATION_CREDENTIALS_BASE64} ]]; then
    base64_to_file "${GOOGLE_APPLICATION_CREDENTIALS_BASE64}" "${SA_KEY}"
    activate_sa "${SA_KEY}"
    rm "${SA_KEY}"
    seed
    deactivate_sa
  else
    seed
  fi
}

function usage() {
  cat <<-EOF
DESCR:
======
This script will seed content of source bucket into subfolder at target iterating over backup days counter

USAGE:
======
Set following environment variables before running script:

SRC_BUCKET - Comma-separated list of source buckets to rsync files from         # REQUIRED
TARGET_BUCKET - Target bucket to seed files to                                  # REQUIRED
GOOGLE_APPLICATION_CREDENTIALS_BASE64 - Base64-encoded google service account   # OPTIONAL
BACKUP_START_DATE - Starting date in ISO format. e.g 2020-05-29T12:01:04Z.      # OPTIONAL
BACKUP_DAY_COUNT - Number of days to seed. default - 100                        # OPTIONAL

echo "Args:"
echo "  -h - This help message"
echo "  -n - Enable dry-run mode"
echo "  -v - Enable verbose output"

EOF
}

function seed_backup() {
  local sb tb ts dc ds bn
  sb="${1}"
  tb="${2}"
  ts="${3}"
  dc="${4}"
  ds=${DAYSECONDS}

  for ((i=0; i<=dc-1; i++)); do
    bn=$(date -d "@$(( $(date -d"${ts}" +%s) + "${ds}" * "${i}" ))" +'%Y-%m-%dT%H_%M_%SZ')
    if [ "${DRY_RUN}" = "true" ]; then
      echo "gs://${tb}/${bn}"
    else
      gsutil -m rsync -r "gs://${sb}" "gs://${tb}/${bn}"
    fi
  done
}

function seed() {
  local sb db ts bp dc
  sb=${SRC_BUCKET//,/ }                # replace commas with spaces
  sb=${sb//gs:\/\//}                   # strip bucket scheme. keep sub-path. sub-path is allowed in SRC_BUCKET
  db=${TARGET_BUCKET//gs:\/\//}        # strip bucket scheme
  db=${db%%\/*}                        # strip sub-path. sub-path is not allowed in TARGET_BUCKET
  ts=${BACKUP_START_DATE}              # ISO 8601 time format
  dc=${BACKUP_DAY_COUNT}

  for bucket in ${sb} ${db}; do
    check_bucket "${bucket}" || exit 1
  done

  for bucket in ${sb}; do
    bp=${bucket//\//_}              # replace slashes with underscores to keep flat folder structure

    if [ "${DRY_RUN}" = "true" ]; then
      info "skipping gs://${db}/${bp} bucket seed for dry-run."
      info "printing target structure after seeding"
      seed_backup "${bucket}" "${db}/${bp}" "${ts}" "${dc}"
    else
      info "seeding gs://${db}/${bp} bucket from gs://${bucket}"
      seed_backup "${bucket}" "${db}/${bp}" "${ts}" "${dc}"
    fi
  done
}

## Entrypoint
main "${@}"
