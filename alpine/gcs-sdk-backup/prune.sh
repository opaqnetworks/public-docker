#!/usr/bin/env bash

# shellcheck disable=SC1091
# shellcheck source=common.sh
source "${LOCAL_LIB}/common.sh"

## Define global variables
REQ_ENV_VARS=(TARGET_BUCKET KEEP_DAYS KEEP_WEEKS KEEP_MONTHS)
REQ_CMDS=(gcloud gsutil)
SA_KEY="sa-key.json"
DAYSECONDS=86400
WEEKSECONDS=$(( DAYSECONDS * 7 ))
MONTHSECONDS=$(( WEEKSECONDS * 4 ))

KEEP_DAYS_MIN=7
KEEP_WEEKS_MIN=4
KEEP_MONTHS_MIN=1

## Define functions
function main() {
  set_standard_args "$@" || { usage && exit 1; }
  [[ -n "$HELP" ]] && { usage && exit 0; }
  check_required_envs "${REQ_ENV_VARS[@]}" || { usage && exit 1; }
  check_required_cmds "${REQ_CMDS[@]}" || { usage && exit 1; }

  info "Target bucket: ${TARGET_BUCKET}"

  ## bail if unsafe values are used to prevent backup bucket from wipeout
  [[ ${KEEP_DAYS} -lt ${KEEP_DAYS_MIN} ]] && fatal "KEEP_DAYS is less than allowed minimum: ${KEEP_DAYS_MIN}"
  [[ ${KEEP_WEEKS} -lt ${KEEP_WEEKS_MIN} ]] && fatal "KEEP_WEEKS is less than allowed minimum: ${KEEP_WEEKS_MIN}"
  [[ ${KEEP_MONTHS} -lt ${KEEP_MONTHS_MIN} ]] && fatal "KEEP_MONTHS is less than allowed minimum: ${KEEP_MONTHS_MIN}"

  info "Prune settings:\nKEEP_DAYS=${KEEP_DAYS}\nKEEP_WEEKS=${KEEP_WEEKS}\nKEEP_MONTHS=${KEEP_MONTHS}"

  if [[ -n ${GOOGLE_APPLICATION_CREDENTIALS_BASE64} ]]; then
    base64_to_file "${GOOGLE_APPLICATION_CREDENTIALS_BASE64}" "${SA_KEY}"
    activate_sa "${SA_KEY}"
    rm "${SA_KEY}"
    prune
    deactivate_sa
  else
    prune
  fi
}

function usage() {
  cat <<-EOF
DESCR:
======
This script will prune content of target bucket based on environment variables settings.
This script expects target bucket folders structure to be created by backup script.

USAGE:
======
Set following environment variables before running script:

TARGET_BUCKET - Target bucket to prune                                           # REQUIRED
GOOGLE_APPLICATION_CREDENTIALS_BASE64 - Base64-encoded google service account'   # OPTIONAL
KEEP_DAYS - keep number of daily snapshots. minimum=7                            # REQUIRED
KEEP_WEEKS - keep number of weekly snapshots. minimum=4                          # REQUIRED
KEEP_MONTHS - keep number of monthly snapshots. minimum=1                        # REQUIRED

echo "Args:"
echo "  -h - This help message"
echo "  -n - Enable dry-run mode"
echo "  -v - Enable verbose output"

EOF
}

function list_all_folders() {
  gsutil ls "gs://${1}/" | sed 's#gs://##g;s#/$##'                # strip scheme and trailing slash
}

function list_all_backups() {
  gsutil ls "gs://${1}/" | sort -r | sed 's#/$##;s#gs://.*/##g'   # strip trailing slash and scheme with basepath
}

function prune() {
  local db
  db=${TARGET_BUCKET//,/ }          # replace commas with spaces
  db=${db//gs:\/\//}                # strip bucket scheme

  for bucket in ${db}; do
    header "processing bucket \"${bucket}\""
    bucket=${bucket%/}              # strip bucket trailing slash
    check_bucket "${bucket}" || exit 1
    unset all_folders               # destroy for reuse
    if [[ "${bucket}" =~ / ]]; then
      all_folders=("${bucket}")
    else
      mapfile -t all_folders < <(list_all_folders "${bucket}")
    fi
    debug "prune: all_folders:\n ${all_folders[*]}"

    for folder in "${all_folders[@]}"; do
      header "processing folder ${folder}"
      unset all_backups all_backups_uniq protected_ind protected_bak temp temp2  # unset arrays for each iteration

      mapfile -t all_backups < <(list_all_backups "${folder}")
      debug "prune: all_backups:\n ${all_backups[*]}"

      ## First Pass: Remove all items which cannot be converted to valid date
      local i j
      for i in "${!all_backups[@]}"; do
        if ! date -d "${all_backups[i]//_/:}" &>/dev/null; then
          unset 'all_backups[i]'              # remove array element
        fi
      done
      debug "prune: all_backups: dates only:\n ${all_backups[*]}"

      ## Second Pass: Remove all non-uniq items keeping latest date for duplicates
      all_backups_uniq=("${all_backups[@]}")  # preserve original array with duplicates
      declare -A temp                         # declare associative array for counting duplicates
      for i in "${all_backups_uniq[@]}"; do
        ((temp["${i%T*}"]++))                 # count duplicate dates. use dates as keys
      done

      for i in "${!temp[@]}"; do
        if [[ ${temp[$i]} -gt 1 ]]; then      # check all duplicate dates with values > 1
          local count
          count=0
          for j in "${!all_backups_uniq[@]}"; do
            if echo "${all_backups_uniq[j]}" | grep -q "${i}" ;then
              [[ count -ge 1 ]] && unset 'all_backups_uniq[j]'             # remove all duplicate dates keeping latest
              ((count++))
            fi
          done
        fi
      done
      all_backups_uniq=("${all_backups_uniq[@]}")                          # compact array removing empty indexes
      debug "prune: all_backups_uniq:\n ${all_backups_uniq[*]}"

      ## Third Pass: Find the protected
      local dec count max_gap length curr_day curr_sec prev_sec prev_day
      local attempt gap found found_all temp2 protected_ind protected_bak
      i=0                             # set pass-through array index
      length=${#all_backups_uniq[@]}  # get array length
      protected_ind+=("${i}")         # protect newest backup index. starting point
      debug "prune: first protected_ind backup: ${all_backups_uniq[i]} i: ${i}"
      for mode in KEEP_DAYS KEEP_WEEKS KEEP_MONTHS; do
        [[ "${mode}" == "KEEP_DAYS" ]] && dec=${DAYSECONDS} && max_gap=6 && count=1       # set daily decrement and backups counter
        [[ "${mode}" == "KEEP_WEEKS" ]] && dec=${WEEKSECONDS} && max_gap=3 && count=0     # set weekly decrement and backups counter
        [[ "${mode}" == "KEEP_MONTHS" ]] && dec=${MONTHSECONDS} && max_gap=11 && count=0  # set monthly decrement and backups counter
        found_all=0                                       # set marker to false for all found backups in current mode
        while [[ ${i} -lt $((length - 1)) ]]; do
          curr_day=${all_backups_uniq[${i}]%T*}           # strip all except date part
          curr_sec=$(date -d "${curr_day}" +'%s')         # convert to seconds
          ((i++))                                         # point to next backup
          temp2=("${all_backups_uniq[@]:i}")              # use temporary array for lookahead
          attempt=1
          found=0

          ## Backups gaps logic
          while [[ ${attempt} -le ${max_gap} ]]; do
            gap=$((dec * attempt))
            prev_sec=$(( curr_sec - gap ))                  # get previous backup date in seconds based on dec time gap
            prev_day=$(date -d "@${prev_sec}" +'%Y-%m-%d')  # convert to date
            debug "prune: date lookup attempt: ${attempt} mode: ${mode} gap: ${gap} curr_day: ${curr_day} prev_day: ${prev_day}"
            for j in "${!temp2[@]}"; do
              if echo "${temp2[j]}" | grep -q "${prev_day}" ;then
                ((count++))
                i=$((i+j))
                protected_ind+=("${i}")  # protect matched backup index
                found=1
                debug "prune: mode: ${mode} dec: ${dec} matched: ${temp2[j]} i: ${i} j:${j}"
                if [[ ${count} -eq ${!mode} ]];then
                  found_all=1  # set marker to true for all found backups in current mode
                  break 3      # go to next mode
                else
                  break 2      # go to next backup
                fi
              fi
            done
            [[ ${found} -eq 1 ]] && break  # stop attempts when match found
            ((attempt++))

            ## Backups transitioning logic
            local trans_day trans_sec prev_trans_day prev_trans_sec diff_sec
            trans_day=${all_backups_uniq[${i}]%T*}                    # get next backup
            trans_sec=$(date -d "${trans_day}" +'%s')                 # convert to seconds
            prev_trans_day=${all_backups_uniq[$((i+1))]%T*}           # get backup before next backup
            prev_trans_sec=$(date -d "${prev_trans_day}" +'%s')       # convert to seconds
            diff_sec=$((trans_sec-prev_trans_sec))

            debug "prune: trans: mode: ${mode} trans_day: ${trans_day} prev_trans_day: ${prev_trans_day} diff: $((trans_sec-prev_trans_sec))"

            ## Transition from days to weeks if difference between next backup and backup before next equals WEEKSECONDS
            if [[ "${mode}" == "KEEP_WEEKS" ]]; then
              if [[ ${diff_sec} -eq ${WEEKSECONDS} ]]; then
                protected_ind+=("${i}")
                ((count++))
                debug "prune: days ${trans_day} transitoned to weeks"
                break
              fi
            fi

            ## Transition from weeks to months if difference between next backup and backup before next equals MONTHSECONDS
            if [[ "${mode}" == "KEEP_MONTHS" ]]; then
              if [[ ${diff_sec} -eq ${MONTHSECONDS} ]]; then
                protected_ind+=("${i}")
                ((count++))
                debug "prune: weeks ${trans_day} transitoned to months"
                break
              fi
            fi

          done
        done

        ## Missing expected backups logic. Bail if KEEP_DAYS less than expected
        if [[ ${found_all} -ne 1 && "${mode}" == "KEEP_DAYS" ]]; then
          warn "skip pruning: found less ${mode} backups than expected" && exit 0
        fi

        ## Missing expected backups logic. Protect if KEEP_WEEKS less than expected
        if [[ ${found_all} -ne 1 && "${mode}" == "KEEP_WEEKS" ]]; then
          warn "protecting: found less ${mode} backups than expected. protect all older backups"
          for i in $(seq $((protected_ind[-1]+1)) $((length - 1))); do
            protected_ind+=("${i}")
          done
          break
        fi
      done

      ## Fourth Pass: Remember the protected
      debug "prune: protected_ind:\n${protected_ind[*]}"
      for i in "${protected_ind[@]}"; do
        protected_bak+=("${all_backups_uniq[i]}")
        debug "prune: protected_bak: ${all_backups_uniq[i]}"
      done

      ## Fifth pass: Cull the protected
      for i in "${!all_backups[@]}"; do
          for j in "${!protected_bak[@]}"; do
            [[ "${all_backups[i]}" == "${protected_bak[j]}" ]] && unset 'all_backups[i]'
          done
      done
      all_backups=("${all_backups[@]}")  # compact array removing empty indexes

      ## Sixth pass: Remove the useless
      info "files to keep:\n${protected_bak[*]}"
      info "files to prune:\n${all_backups[*]}"

      if [ "${DRY_RUN}" = "true" ]; then
        info "skipping ${folder} folder pruning for dry-run"
      else
        info "pruning ${folder} folder"
        for i in "${all_backups[@]}"; do
          gsutil -m rm -r "gs://${folder}/${i}"  # delete all live and versioned objects
        done
      fi

    done
  done
}

## Entrypoint
main "${@}"
