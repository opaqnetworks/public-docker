#!/usr/bin/env bash

function usage() {
  echo "Available commands:"
  find "${LOCAL_BIN}" -type f -perm -a=x -exec basename {} \; | sort | xargs -n1 echo "  "
  exit 1
}

if [[ -z "${1}" || "${1}" == "-h" || "${1}" == "--help" ]]; then
  usage
fi

if [[ "${1}" == "--" ]]; then
  shift
fi

if ! [ -x "$(command -v "${1}")" ]; then
  echo "Error: ${1} is not installed." >&2
  usage
fi

exec "$@"
