#!/usr/bin/env bash

# shellcheck disable=SC1091
# shellcheck source=common.sh
source "${LOCAL_LIB}/common.sh"

## Define global variables
REQ_ENV_VARS=(SRC_BUCKET DST_BUCKET)
REQ_CMDS=(gcloud gsutil)
SA_KEY="sa-key.json"

## Define functions
function main() {
  set_standard_args "${@}" || { usage && exit 1; }
  [[ -n "$HELP" ]] && { usage && exit 0; }
  check_required_envs "${REQ_ENV_VARS[@]}" || { usage && exit 1; }
  check_required_cmds "${REQ_CMDS[@]}" || { usage && exit 1; }

  info "Source buckets: ${SRC_BUCKET}"
  info "Destination bucket: ${DST_BUCKET}"

  if [[ -n ${GOOGLE_APPLICATION_CREDENTIALS_BASE64} ]]; then
    base64_to_file "${GOOGLE_APPLICATION_CREDENTIALS_BASE64}" "${SA_KEY}"
    activate_sa "${SA_KEY}"
    rm "${SA_KEY}"
    restore
    deactivate_sa
  else
    restore
  fi
}

function usage() {
  cat <<-EOF
DESCR:
======
This script will restore(mirror) content of source bucket into destination bucket

USAGE:
======
Set following environment variables before running script:

SRC_BUCKET - Source bucket to rsync files from                                  # REQUIRED
DST_BUCKET - Destination bucket to rsync files to                               # REQUIRED
GOOGLE_APPLICATION_CREDENTIALS_BASE64 - Base64-encoded google service account   # OPTIONAL

echo "Args:"
echo "  -h - This help message"
echo "  -n - Enable dry-run mode"
echo "  -v - Enable verbose output"

EOF
}

function restore_backup() {
  local args=("-m" "rsync" "-d" "-r")
  [[ "${DRY_RUN}" = "true" ]] && args+=("-n")
  gsutil "${args[@]}" "gs://${1}" "gs://${2}"
}

function restore() {
  local sb db
  sb=${SRC_BUCKET//gs:\/\//}        # strip bucket scheme
  db=${DST_BUCKET//gs:\/\//}        # strip bucket scheme

  for bucket in ${sb} ${db}; do
    check_bucket "${bucket}" || exit 1
  done

  debug "source: gs://${sb}; destination: gs://${db}"
  restore_backup "${sb}" "${db}"
}

## Entrypoint
main "${@}"
