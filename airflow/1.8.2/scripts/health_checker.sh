#!/bin/bash
FLOWER='flower'
SCHEDULER='airflow scheduler'
WEBSERVER='airflow webserver'

function log(){
  echo "[$(date)] -- $@"
}

function get_pid(){
  local proc=$1
  pgrep "$proc"
  if [ $? -ne 0 ]; then
    echo ''
    return 1
  fi
}

EXIT_STATUS=()

WEB_PID=$(get_pid "$WEBSERVER")
if [ -z "$WEB_PID" ]; then
  log "Unable to get pid for proccess: \"$WEBSERVER\""
  EXIT_STATUS+=1
else
  log "Process for \"$WEBSERVER\" has been found"
fi

FLOW_PID=$(get_pid "$FLOWER")
if [ -z "$FLOW_PID" ]; then
  log "Unable to get pid for proccess: \"$FLOWER\""
  EXIT_STATUS+=1
else
  log "Process for \"$FLOWER\" has been found"
fi

SCHED_PID=$(get_pid "$SCHEDULER")
if [ -z "$SCHED_PID" ]; then
  log "Unable to get pid for proccess: \"$SCHEDULER\""
  EXIT_STATUS+=1
else
  log "Process for \"$SCHEDULER\" has been found"
fi

if [ ${#EXIT_STATUS[@]} -eq 0 ]; then
  log "STATUS: success";
else
  log "STATUS: failure"
  exit 1
fi
