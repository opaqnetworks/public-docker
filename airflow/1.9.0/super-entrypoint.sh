#!/usr/bin/dumb-init /bin/bash

# Script performs migration of DAGs files from EFS to NFS.
# Script is located in public-docker repo -> fourv/airflow/1.9.0
if [ "$DAGS_MIGRATION" == "TRUE" ]; then
  /scripts/dags_migrator.sh
fi

TYPE=$1
shift
case $TYPE in
  airflow-scheduler)
    exec /entrypoint.sh /airflow-scheduler-config.sh $@
    ;;
  airflow-worker)
    exec /entrypoint.sh /airflow-worker-config.sh $@
    ;;
  airflow-webserver)
    exec /entrypoint.sh /airflow-webserver-config.sh $@
    ;;
  *)
    exec /entrypoint.sh $TYPE $@
    ;;
esac
