#!/bin/bash

SRC_DIR=/usr/local/airflow/dags
TRGT_DIR=/usr/local/airflow/dags_nfs
NAMESPACE=$(cat /run/secrets/kubernetes.io/serviceaccount/namespace)

function check_dir_exists(){
  local dir=$1
  test -d $dir
  if [ $? -ne 0 ]; then
    echo "Folder \"${dir}\" is missing. Skipping DAGs migration."
    return 1
  fi
}

function check_noempty_dir(){
  local dir=$1
  if [ ! "$(ls -A $dir)" ]; then
    echo "Folder \"${dir}\" is empty. Skipping DAGs migration."
    return 1
  fi
}

function helm_init(){
  helm init -c
}

function helm_delete_release(){
  local release_name=$1
  /tmp/linux-amd64/helm delete --purge "${NAMESPACE}-${release_name}"
  if [ $? -ne 0 ]; then
    echo "Unable to delete relase: \"${NAMESPACE}-${release_name}\""
    return 1
  fi
}

if [ "$DAGS_MIGRATION" == "TRUE" ]; then
  echo "DAGs migration task is about to start ..."

  check_dir_exists $SRC_DIR &&\
    check_dir_exists $TRGT_DIR &&\
    check_noempty_dir $SRC_DIR &&\
    cp -vfr $SRC_DIR/* $TRGT_DIR
    if [ $? -eq 0 ]; then
      echo "DAGs files have been migrated successfully from \"${SRC_DIR}\" to \"${TRGT_DIR}\""
      if [ ! -z "${NAMESPACE}" ]; then
        echo "Attempt to remove aws-efs helm components ... "
        helm_init
        helm_delete_release greyspark-efs-pv
        helm_delete_release aws-efs
        rm -r /root/.helm 2>/dev/null
      else
        echo "Unable to get name of Namespace. "
        echo "Remove aws efs related helm releases manually!"
      fi
    fi
fi
