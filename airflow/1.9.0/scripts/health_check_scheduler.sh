#!/bin/bash
source /scripts/health_check_common.sh

FLOWER='flower'
SCHEDULER='airflow scheduler'

EXIT_STATUS=()

FLOW_PID=$(get_pid "$FLOWER")
if [ -z "$FLOW_PID" ]; then
  log "Unable to get pid for process: \"$FLOWER\""
  EXIT_STATUS+=1
else
  log "Process for \"$FLOWER\" has been found"
fi

SCHED_PID=$(get_pid "$SCHEDULER")
if [ -z "$SCHED_PID" ]; then
  log "Unable to get pid for process: \"$SCHEDULER\""
  EXIT_STATUS+=1
else
  log "Process for \"$SCHEDULER\" has been found"
fi

DAG_STATUS=$(test_dir_for_dags "$AIRFLOW__CORE__DAGS_FOLDER")
if [ ${DAG_STATUS} -gt 0 ]; then
  log "DAG folder state check failed!"
  EXIT_STATUS+=1
else
  log "DAG folder state check passed"
fi

if [ ${#EXIT_STATUS[@]} -eq 0 ]; then
  log "STATUS: success";
else
  log "STATUS: failure"
  exit 1
fi
