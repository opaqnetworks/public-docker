FROM quay.io/opaqnetworks/alpine:3.5-bd0d885

LABEL maintainer "FourV Systems <admin@fourv.com>"
LABEL revision "a4fffe1"

ENV NUMPY_VERSION 1.12.0
ENV PANDAS_VERSION 0.16.2
ENV AIRFLOW_VERSION 1.8.0

ENV AIRFLOW_HOME=/usr/local/airflow

RUN apk add --no-cache \
    bash \
    build-base \
    ca-certificates \
    musl \
    linux-headers \
    libxml2-dev \
    libxslt-dev \
    python \
    python-dev \
    py-pip \
    postgresql-dev \
    py-psycopg2 && \
    ln -s /usr/include/locale.h /usr/include/xlocale.h && \
    pip install numpy==$NUMPY_VERSION && \
    pip install pandas==$PANDAS_VERSION && \
    pip install airflow[postgres]==$AIRFLOW_VERSION

EXPOSE 8080

COPY entrypoint.sh /
RUN chmod a+x /entrypoint.sh

ENV AIRFLOW__CORE__AIRFLOW_HOME=/usr/local/airflow
ENV AIRFLOW__CORE__DAGS_FOLDER=/usr/local/airflow/dags
ENV AIRFLOW__CORE__BASE_LOG_FOLDER=/usr/local/airflow/logs
ENV AIRFLOW__CORE__PLUGINS_FOLDER=/usr/local/airflow/plugins
#ENV AIRFLOW__CORE__EXECUTOR=SequentialExecutor
#ENV AIRFLOW__CORE__LOAD_EXAMPLES=False

ENTRYPOINT ["/entrypoint.sh"]
