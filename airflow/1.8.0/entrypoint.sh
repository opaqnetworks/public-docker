#!/bin/bash

CMD="airflow"

echo "Initialize database..."
$CMD initdb
exec $CMD webserver &
exec $CMD scheduler
