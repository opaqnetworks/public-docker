#!/bin/bash
source /scripts/health_check_common.sh

WEBSERVER='gunicorn: worker'

EXIT_STATUS=()

WEB_PID=$(get_pid "$WEBSERVER")
if [ -z "$WEB_PID" ]; then
  log "Unable to get pid for process: \"$WEBSERVER\""
  EXIT_STATUS+=1
else
  log "Process for \"$WEBSERVER\" has been found"
fi

DAG_STATUS=$(test_dir_for_dags "$AIRFLOW__CORE__DAGS_FOLDER")
if [ ${DAG_STATUS} -gt 0 ]; then
  log "DAG folder state check failed!"
  EXIT_STATUS+=1
else
  log "DAG folder state check passed"
fi

if [ ${#EXIT_STATUS[@]} -eq 0 ]; then
  log "STATUS: success";
else
  log "STATUS: failure"
  exit 1
fi
