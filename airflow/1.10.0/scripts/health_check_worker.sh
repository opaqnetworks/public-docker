#!/bin/bash
source /scripts/health_check_common.sh

WORKER='airflow serve_logs'

EXIT_STATUS=()

WORKER_PID=$(get_pid "$WORKER")
if [ -z "$WORKER_PID" ]; then
  log "Unable to get pid for process: \"$WORKER\""
  EXIT_STATUS+=1
else
  log "Process for \"$WORKER\" has been found"
fi

DAG_STATUS=$(test_dir_for_dags "$AIRFLOW__CORE__DAGS_FOLDER")
if [ ${DAG_STATUS} -gt 0 ]; then
  log "DAG folder state check failed!"
  EXIT_STATUS+=1
else
  log "DAG folder state check passed"
fi

if [ ${#EXIT_STATUS[@]} -eq 0 ]; then
  log "STATUS: success";
else
  log "STATUS: failure"
  exit 1
fi
