#!/bin/bash

function log(){
  echo "[$(date)] -- $@"
}

function get_pid(){
  local proc=$1
  pgrep -f "$proc"
  if [ $? -ne 0 ]; then
    echo ''
    return 1
  fi
}

function xor() {
  if (( $1 ^ $2 )) ;then
    echo 1
  else
    echo 0
  fi
}

# check for python dag files
# conditions mirror truth table of exclusive or (XOR):
# 1) initial state (no dags exist) - no further check needed, exit 0
# 2) dags exist - write .dags_exist file to dag folder - exit 0
# 3) .dags_exist file present, dag files (*.py*) present - exit 0
# 4) .dags_exist file present, dag files (*.py*) not present - exit 1
function test_dir_for_dags(){
  local dir=$1
  local exit_status=0
  local dags_exist=0
  local dags_found=0
  # dag files found test
  if [ $(find "$dir" -type f -name "*.py*" | wc -l) -gt 0 ]; then
    dags_found+=1
    touch /.dags_exist
  else
    dags_found=0
  fi
  # dags_exist flag test
  if [ $(find / -maxdepth 1 -type f -name ".dags_exist" | wc -l) -gt 0 ]; then
    dags_exist+=1
  else
    dags_exist=0
  fi
  exit_status=$(xor "$dags_found" "$dags_exist")
  echo "$exit_status"
}