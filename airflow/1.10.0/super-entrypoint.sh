#!/usr/bin/dumb-init /bin/bash

TYPE=$1
shift
case $TYPE in
  airflow-scheduler)
    exec /entrypoint.sh /airflow-scheduler-config.sh $@
    ;;
  airflow-worker)
    exec /entrypoint.sh /airflow-worker-config.sh $@
    ;;
  airflow-webserver)
    exec /entrypoint.sh /airflow-webserver-config.sh $@
    ;;
  *)
    exec /entrypoint.sh $TYPE $@
    ;;
esac
