#!/bin/bash

#GreySpark-alerting config
cp /tmp/application-config/application.conf $GREYSPARK_HOME/config
# start airflow services
if [ $AIRFLOW_INIT_DB -eq 1 ]; then
  airflow initdb
fi

airflow flower &
airflow scheduler -n "$SCHEDULER_RUNS"
