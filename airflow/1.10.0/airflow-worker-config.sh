#!/bin/bash
#GreySpark-alerting config
cp /tmp/application-config/application.conf $GREYSPARK_HOME/config
if [ -z "$AIRFLOW_LOG_CLEAR_LOOKBACK_DAYS" ]; then
  # default to 7 days
  AIRFLOW_LOG_CLEAR_LOOKBACK_DAYS=7
fi

if [ -z "$AIRFLOW_HOME" ]; then
  # default airflow home
  AIRFLOW_HOME="/usr/local/airflow"
fi

find "${AIRFLOW_HOME}/logs" -type f -mtime +"${AIRFLOW_LOG_CLEAR_LOOKBACK_DAYS}" | xargs rm -rf
# start airflow worker services
airflow worker
