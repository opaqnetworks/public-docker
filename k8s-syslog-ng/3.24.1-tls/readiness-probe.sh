#!/bin/bash
set -x
SNG_TLS_PORT=${SNG_TLS_PORT:-6514}
LSOF="/usr/bin/lsof"
$LSOF -i :${SNG_TLS_PORT} 1>/dev/null 2>&1
if [ $? -ne 0 ]; then
  echo "Unable to detect listening port for syslog-ng service: ${SNG_TLS_PORT}"
  exit 1
fi
