#!/bin/bash
set -x
SNG_TLS_PORT=${SNG_TLS_PORT:-6514}
LSOF="/usr/bin/lsof"
MD5="/usr/bin/md5sum"
SNG_CTL="/usr/sbin/syslog-ng-ctl"
SYSLOG_NG_CONF="${SYSLOG_NG_CONF:-/etc/syslog-ng/syslog-ng.conf}"
#CONFIGMAP_HASH=$(${MD5} ${SYSLOG_NG_CONF} | cut -f1 -d ' ')
LOADED_CONFIG_HASH=$(${SNG_CTL} config | ${MD5} | cut -f1 -d ' ')
DISK_BUFFER_DIR="/data/disk-buffer"
SSL_DIR=/etc/syslog-ng/ssl
INIT_CRT_FILE_PATH="${DISK_BUFFER_DIR}/.tls_crt_hash"
INIT_KEY_FILE_PATH="${DISK_BUFFER_DIR}/.tls_key_hash"
CRT_FILE="${SSL_DIR}/tls.crt"
KEY_FILE="${SSL_DIR}/tls.key"
RELOAD_REQUIRED="false"
REWRITE_HASH="false"

function get_md5_hash(){
  local file=$1
  ${MD5} ${file} | cut -f1 -d ' '
}

CONFIGMAP_HASH=$(get_md5_hash ${SYSLOG_NG_CONF})

if [ "${LOADED_CONFIG_HASH}" != "$CONFIGMAP_HASH" ]; then
  RELOAD_REQUIRED="true"
fi

if [ -d "${DISK_BUFFER_DIR}" ]; then
  if [ -f "${INIT_CRT_FILE_PATH}" ]; then
    INIT_CRT_FILE_HASH=$(cat "${INIT_CRT_FILE_PATH}")
    CRT_FILE_HASH=$(get_md5_hash "${CRT_FILE}")
    if [ "${INIT_CRT_FILE_HASH}" != "${CRT_FILE_HASH}" ]; then
      RELOAD_REQUIRED="true"
      REWRITE_HASH="true"
    fi
  fi
  if [ -f "${INIT_KEY_FILE_PATH}" ]; then
    INIT_KEY_FILE_HASH=$(cat "${INIT_KEY_FILE_PATH}")
    KEY_FILE_HASH=$(get_md5_hash "${KEY_FILE}")
    if [ "${INIT_KEY_FILE_HASH}" != "${KEY_FILE_HASH}" ]; then
      RELOAD_REQUIRED="true"
      REWRITE_HASH="true"
    fi
  fi
fi

if [ "${RELOAD_REQUIRED}" == "true" ]; then
  $SNG_CTL stop
fi

if [ "${REWRITE_HASH}" == "true" ]; then
  echo "${CRT_FILE_HASH}" > "${INIT_CRT_FILE_PATH}"
  echo "${KEY_FILE_HASH}" > "${INIT_KEY_FILE_PATH}"
fi

$LSOF -i :${SNG_TLS_PORT} 1>/dev/null 2>&1
if [ $? -ne 0 ]; then
  echo "Unable to detect listening port for syslog-ng service: ${SNG_TLS_PORT}"
  exit 1
fi
