#!/bin/bash
set -x
SNG_TLS_PORT=${SNG_TLS_PORT:-6514}
SNG_CTL="/usr/sbin/syslog-ng-ctl"
SYSLOG_NG_SOCKET="${SYSLOG_NG_SOCKET:-/var/lib/syslog-ng/syslog-ng.ctl}"
LSOF="/usr/bin/lsof"

if [ ! -S "${SYSLOG_NG_SOCKET}" ]; then
  echo "ERROR: Unable to find syslog-ng socket in path: ${SYSLOG_NG_SOCKET}"
  exit 1
fi

${SNG_CTL} config 1>/dev/null
if [ $? -ne 0 ]; then
  echo "ERROR: Unable to fetch data from syslog-ng config."
  exit 1
fi

$LSOF -i :${SNG_TLS_PORT} 1>/dev/null 2>&1
if [ $? -ne 0 ]; then
  echo "ERROR: Unable to detect listening port for syslog-ng service: ${SNG_TLS_PORT}"
  exit 1
fi
