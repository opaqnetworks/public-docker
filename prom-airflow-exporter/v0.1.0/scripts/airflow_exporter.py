#!/usr/bin/python
import psycopg2
import os
import time
import re
import logging
from prometheus_client import start_http_server, Summary, Gauge

REQUEST_TIME = Summary(
                        'update_processing_seconds',
                        'Time spent querying'
                    )


def main():
    port = int(os.environ.get('AE_PORT', '8080'))
    sleep_interval = int(os.environ.get('AE_SLEEP_INTERVAL', '300'))
    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p')
    logging.debug("Starting web-server ...")
    start_http_server(port)
    # Starting to create metrics
    #
    # airflow_dag_count
    logging.debug("Creating metrics ...")
    adc = Gauge("airflow_dag_count",
                "The total number of Dags",
                ['dag_id'])
    #
    # total execution time by day (blindly report the last 2 execution times).
    # Simple, but has the limitation that any day that takes longer then 2 days
    # to be processed will be misreported. That is fine for now.
    tetbd = Gauge("airflow_dag_total_execution_time_by_day",
                  "The total execution time by day",
                  ['execution_date'])
    #
    # airflow_dag_paused_count
    adpc = Gauge("airflow_dag_paused_count",
                 "The total number of Dags that are paused",
                 ['dag_id'])
    #
    # airflow-dag-last-execution-date
    adled = Gauge("airflow_dag_last_execution_date",
                  "Last execution date",
                  ['execution_date'])
    #
    # airflow-dag-*-count
    # common list
    adsc_list = [
                 'dag_id',
                 'count'
                 ]
    adrc = Gauge("airflow_dag_running_count",
                 "The total number of Dags by name running",
                 adsc_list)
    adsc = Gauge("airflow_dag_success_count",
                 "The total number of successes of a Dag",
                 adsc_list)
    adfc = Gauge("airflow_dag_failed_count",
                 "The total number of failed runs of a Dag",
                 adsc_list)
    #
    # airflow-dag-status
    ads_list = [
                'dag_id',
                'execution_date',
                'state'
               ]
    ads = Gauge("airflow_dag_status",
                "The status of the current dag",
                ads_list)
    # Set/Update metrics in while loop iterations with sleep interval
    while True:
        logging.debug("Starting to update metrics ... ")
        #
        # airflow_dag_count
        set_update_value_for_metric_from_count_query(adc,
                                                     "dag_id",
                                                     "dag")
        #
        # airflow_dag_paused_count
        set_update_value_for_metric_from_count_query(adpc,
                                                     "dag_id",
                                                     "dag WHERE is_paused = 't'")
        #
        # airflow-dag-running-count
        set_update_value_for_metric_from_query(adrc,
                                               adsc_list,
                                               "dag_stats WHERE state = 'running'")
        #
        # airflow-dag-success-count
        set_update_value_for_metric_from_query(adsc,
                                               adsc_list,
                                               "dag_stats WHERE state = 'success'")
        #
        # airflow-dag-failed-count
        set_update_value_for_metric_from_query(adfc,
                                               adsc_list,
                                               "dag_stats WHERE state = 'failed'")
        #
        # airflow-dag-status
        set_update_static_value_for_metrics(ads,
                                            "set_time",
                                            ads_list,
                                            "dag_run"
                                            )
        #
        # airflow-dag-last-execution-date
        set_update_value_for_metric_from_max_query(adled,
                                                   "execution_date",
                                                   "task_instance"
                                                   )
        #
        # total execution time by day (tetbd) (last two days, and execution that takes only up to 48H)
        set_update_value_for_metric_from_tetbd_query(tetbd)

        logging.debug("Metrics have been updated. Sleeping for " + str(sleep_interval) + " seconds ...")
        time.sleep(sleep_interval)


@REQUEST_TIME.time()
def psql_fetch(sql_query):

    db_host = os.environ.get('POSTGRES_HOST')
    db_name = os.environ.get('POSTGRES_DB')
    db_user = os.environ.get('POSTGRES_USER')
    db_pass = os.environ.get('POSTGRES_PASSWORD')
    # Define our connection string
    conn = psycopg2.connect(
        host=db_host,
        dbname=db_name,
        user=db_user,
        password=db_pass
    )
    cursor = conn.cursor()
    cursor.execute(sql_query)
    rows = cursor.fetchall()
    cursor.close()
    conn.close()
    return rows


# total execution time by day (tetbd) (last two days, and execution that takes only up to 48H)
def set_update_value_for_metric_from_tetbd_query(var_metric_name):
    sql_query = """SELECT execution_date, (max(end_date) - min(start_date)) as duration
                   FROM task_instance
                   WHERE execution_date
                   BETWEEN now() - interval '2 day' AND now()
                   GROUP BY execution_date
                   ORDER BY execution_date"""
    fetched_data = psql_fetch(sql_query)
    for line in fetched_data:
        execution_date = line[0]
        duration = line[1]
        if duration:
            duration_wo_ms = str(duration).split('.')[0]
            hms_regex = re.compile('^(\d+):(\d+):(\d+)$')
            ms_regex = re.compile('^(\d+):(\d+)$')
            s_regex = re.compile('^(\d+)$')
            found_hms = hms_regex.match(duration_wo_ms)
            found_ms = ms_regex.match(duration_wo_ms)
            found_s = s_regex.match(duration_wo_ms)
            if found_hms:
                dur_in_sec = 3600 * int(found_hms.group(1)) + \
                    60 * int(found_hms.group(2)) +\
                    int(found_hms.group(3))
            if found_ms:
                dur_in_sec = 60 * int(found_ms.group(1)) +\
                    int(found_ms.group(2))
            if found_s:
                dur_in_sec = int(found_s.group(1))
            if execution_date and dur_in_sec:
                var_metric_name.labels(execution_date).set(dur_in_sec)


def set_update_value_for_metric_from_count_query(var_metric_name,
                                                 column_id,
                                                 table_name):
    sql_query = """SELECT COUNT({0}) FROM {1}""".format(column_id, table_name)
    fetched_data = psql_fetch(sql_query)
    for line in fetched_data:
        count = ("%d" % (line))
        var_metric_name.labels(count).set(count)


def set_update_value_for_metric_from_max_query(var_metric_name,
                                               column_id,
                                               table_name):
    sql_query = """SELECT max({0}) FROM {1}""".format(column_id, table_name)
    fetched_data = psql_fetch(sql_query)
    for line in fetched_data:
        execution_date = line[0]
        if execution_date:
            var_metric_name.labels(execution_date).set_to_current_time()


def set_update_value_for_metric_from_query(var_metric_name,
                                           columns_list,
                                           table_name):
    columns_string = ', '.join(map(str, columns_list))
    sql_query = """SELECT {0} FROM {1}""".format(columns_string, table_name)
    fetched_data = psql_fetch(sql_query)
    for line in fetched_data:
        dag_id = line[0]
        count = line[1]
        var_metric_name.labels(dag_id, count).set(count)


# For unix timestamp and static metric value
def set_update_static_value_for_metrics(var_metric_name,
                                        metric_value,
                                        columns_list,
                                        table_name):
    columns_string = ', '.join(map(str, columns_list))
    sql_query = """SELECT {0} FROM {1}""".format(columns_string, table_name)
    fetched_data = psql_fetch(sql_query)
    for line in fetched_data:
        labels = {}
        for idx, column in enumerate(columns_list):
            # All data returned should be a for a column in the column list
            # so it is best to iterate over the known list, instead of the
            # line itself.
            labels[column] = line[idx]
        # since the labels are a hash, just hash-splat into a keyword arg list
        metric = var_metric_name.labels(**labels)

        if metric_value == "set_time":
            metric.set_to_current_time()
        else:
            metric.set(metric_value)


if __name__ == "__main__":
    main()
