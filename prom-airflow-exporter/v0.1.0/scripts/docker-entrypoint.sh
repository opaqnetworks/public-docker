#!/usr/bin/dumb-init /bin/bash

if [ -z "$POSTGRES_HOST" ]; then
  echo "POSTGRES_HOST is missing."
  exit 1
fi

if [ -z "$POSTGRES_USER" ]; then
  echo "POSTGRES_USER is missing."
  exit 1
fi

if [ -z "$POSTGRES_PASSWORD" ]; then
  echo "POSTGRES_PASSWORD is missing."
  exit 1
fi

if [ -z "$POSTGRES_DB" ]; then
  echo "POSTGRES_DB is missing."
  exit 1
fi

echo "Verify the Database is available"
  export PGPASSWORD=$POSTGRES_PASSWORD
  until psql -h "$POSTGRES_HOST" -p 5432 -U "$POSTGRES_USER" -d "$POSTGRES_DB" -c '\l'; do
    >&2 echo "PostgreSQL DB is unavailable ($DB_HOST:$DB_PORT/$DB_NAME) - sleeping........."
    sleep 5
  done

# List of tables from airflow_exporter.py
echo "Check if tables exist"
for table in dag dag_stats dag_run task_instance; do
  echo -n "Table: ${table} --> "
  until psql -q -h "$POSTGRES_HOST" -p 5432 -U "$POSTGRES_USER" -d "$POSTGRES_DB" -c 'SELECT * FROM '"${table}"' LIMIT 1' 1>/dev/null 2>&1 ; do
    echo "PostgreSQL table \"${table}\" is unavailable ($DB_HOST:$DB_PORT/$DB_NAME) - sleeping........."
    sleep 5
  done
  echo "OK"
done

python /scripts/airflow_exporter.py
