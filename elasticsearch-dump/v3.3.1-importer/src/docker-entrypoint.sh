#!/bin/bash

DUMP_DIR=/ES_DATA
mkdir -p ${DUMP_DIR}

if [ -z "$AWS_ACCESS_KEY_ID" ]; then
  echo "Please double check AWS creds."
  echo "AWS_ACCESS_KEY_ID is missing."
  exit 1
fi

if [ -z "$AWS_SECRET_ACCESS_KEY" ]; then
  echo "Please double check AWS creds."
  echo "AWS_SECRET_ACCESS_KEY is missing."
  exit 1
fi

if [ -z "$S3_BUCKET" ]; then
  echo "S3_BUCKET variable is missing."
  exit 1
fi

if [ -z "${ELASTICSEARCH_BASE_URL}" ]; then
  echo "ELASTICSEARCH_BASE_URL variable is missing."
  echo "That variable should include the protocol, auth details, server and port"
  echo "E.g.: \"http://user:password@server:port\""
  exit 1
fi

touch /var/spool/cron/crontabs/root
crond -f -d0 &
tail -F /var/log/crontasks.log
