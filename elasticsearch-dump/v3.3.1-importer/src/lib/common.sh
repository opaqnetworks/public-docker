DOWNLOADED_LOG="download.log"
IMPORTTED_LOG="import.log"

function log_date(){
  echo "[$(date)] -- $@"
}

function sed_os(){
  local file=$(echo ${1} | sed -e 's/\//\\\//g')
  local log_file=$2
  sed -i "/$file/d" $log_file
}

function get_ps_count(){
  local command=$1
  local threshold=$2
  local count=$(ps aux | grep $command | grep -v grep | wc -l | tr -d ' ')
  if [ $count -ge $threshold -a $count -lt $(($threshold + 1)) ]; then
    log_date "Max number of parallel jobs has been reached."
    return 1
  fi
}
