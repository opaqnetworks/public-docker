# elasticsearch-dump:v3.3.1-importer

This container will continuously upload ES dump files from an S3 bucket to a cluster.  The S3 file list is checked for indexes that already exist and does not reprocess.  To re-upload drop the index.

The container is 3 concurrent processes that:
1. Scan the S3 bucket for files, and checks each against the processed index
2. Downloads up to MAX_DOWNLOAD in parallel
3. Uploads up to MAX_UPLOAD files in parallel
