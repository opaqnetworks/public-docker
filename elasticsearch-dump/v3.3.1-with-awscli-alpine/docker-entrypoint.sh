#!/bin/bash

DUMP_DIR=/ES_DATA
mkdir -p ${DUMP_DIR}

if [ -z "$AWS_ACCESS_KEY_ID" ]; then
  echo "Please double check AWS creds."
  echo "AWS_ACCESS_KEY_ID is missing."
  exit 1
fi

if [ -z "$AWS_SECRET_ACCESS_KEY" ]; then
  echo "Please double check AWS creds."
  echo "AWS_SECRET_ACCESS_KEY is missing."
  exit 1
fi

if [ -z "$S3_BUCKET" ]; then
  echo "S3_BUCKET variable is missing."
  exit 1
fi
S3_BASE=$(echo $S3_BUCKET | awk -F/ '{print $1}')

if [ -z "${ELASTICSEARCH_BASE_URL}" ]; then
  echo "ELASTICSEARCH_BASE_URL variable is missing."
  echo "That variable should include the protocol, auth details, server and port"
  echo "E.g.: \"http://user:password@server:port\""
  exit 1
fi

function log_date(){
  echo "[$(date)] -- $@"
}

# List all files recursively, remove everything not an esdump file.
# Strip all the AWS cruft then duplicate the file name (with the date).
# Reverse sort strickly on the file name.  Then remove the extra field.
# Expensive, but the best we can do in Bash.
S3_FILE_LIST=($(aws s3 ls --recursive s3://${S3_BUCKET}/ | grep -e "esdump$" | awk '{print $NF}' | awk -F/ '{print $NF, $0}' | sort -r | cut -f 2 -d ' ' ))
if [[ -z "${S3_FILE_LIST[@]}" ]]; then
  log_date "Unable to load list of indices from s3://${S3_BUCKET}. Exiting ..."
  exit 1
fi


DOWNLOADED_LOG="/$DUMP_DIR/downloaded.log"
test -f $DOWNLOADED_LOG || touch $DOWNLOADED_LOG

function get_array_index_id(){
  local index_value=$1
  for id in "${!S3_FILE_LIST[@]}"; do
    if [[ "${S3_FILE_LIST[$id]}" = "$index_value" ]]; then
      echo "${id}"
    fi
  done
}

PROGRESS_LOG="/$DUMP_DIR/progress.log"
test -f ${PROGRESS_LOG} || touch ${PROGRESS_LOG}

IMPORTED_LOG="/$DUMP_DIR/imported.log"
test -f $IMPORTED_LOG || touch $IMPORTED_LOG

function print_log_table(){
  echo "Overall status:"
  echo "====================================================================================="
  cat "${DOWNLOADED_LOG}"
  echo "====================================================================================="
  cat "${PROGRESS_LOG}"
  echo "====================================================================================="
  cat "${IMPORTED_LOG}"
  echo "====================================================================================="
}

JOBS_IMPORT_LIMIT=5
function els_import_cmd(){
  local input=$1
  local output=$2
  local process_count=$(ps uax | grep elasticdump | grep -v grep | wc -l)
  while [ $process_count -ge $JOBS_IMPORT_LIMIT ]; do
    log_date "Limit number of simultaneous executions has been reached. (${process_count}/${JOBS_IMPORT_LIMIT})"
    print_log_table
    sleep 60
    process_count=$(ps uax | grep elasticdump | grep -v grep | wc -l)
  done
  elasticdump --input=${input} --output=${ELASTICSEARCH_BASE_URL}/${output}
  if [ $? -eq 0 ]; then
    log_date "${input} -- Imported" | tee -a ${IMPORTED_LOG}
    sed -i "/${input}/d" ${PROGRESS_LOG}
  fi
}

function import_indice(){
  local indice_name=$1
  new_indice_name=$(cat ${indice_name} | jq ._index | head -n1 | tr -d '"')
  if [ ! -z "$new_indice_name" ]; then
    log_date "${indice_name} -- Running" | tee -a ${PROGRESS_LOG}
    els_import_cmd ${indice_name} ${new_indice_name}
  else
    log_date "${indice_name} - unable to find index inside."
  fi
}

function check_imported_indice(){
  local indice_name=$1
  if $(grep -q ${indice_name} ${PROGRESS_LOG} ${IMPORTED_LOG}); then
    log_date "${indice_name} has already been imported earlier or in running state."
  else
    import_indice ${indice_name}
  fi
}

function get_downloaded_file_list(){
  for file in "${S3_FILE_LIST[@]}"; do
    if $(grep -q ${file} ${DOWNLOADED_LOG}); then
      log_date "${file} has already been downloaded earlier."
      check_imported_indice ${file} &
      unset S3_FILE_LIST[$(get_array_index_id ${file})]
    fi
  done
}

function check_running_elasticdump(){
  local command=$(ps uax | grep elasticdump | grep -v grep >/dev/null 2>&1; echo $?)
  if [ $command -eq 0 ]; then
    while [ $command -eq 0 ]; do
      print_log_table
      sleep 10
      command=$(ps uax | grep elasticdump | grep -v grep >/dev/null 2>&1; echo $?)
    done;
  fi
}

function broken_run_handler(){
  local running_list=($(cat ${PROGRESS_LOG} | awk '{print $8}'))
  if [[ ! -z "${running_list[@]}" ]]; then
    log_date "Attempt to fix import procedure."
    for indice_name in "${running_list[@]}"; do
      new_indice_name=$(cat ${indice_name} | jq ._index | head -n1 | tr -d '"')
      els_import_cmd ${indice_name} ${new_indice_name}
    done
  fi
}

function check_progress_list_before_exit(){
  if [ -s ${PROGRESS_LOG} ]; then
    log_date "${PROGRESS_LOG} is not empty, but I was not able to find any running 'elasticdump' process"
    echo "====================================================================================="
    cat "${PROGRESS_LOG}"
    echo "====================================================================================="
    broken_run_handler
    check_running_elasticdump
  fi
}

cd /${DUMP_DIR}
get_downloaded_file_list
for file in "${S3_FILE_LIST[@]}"; do
  log_date "Downloading ${S3_BASE}/${file}"
  aws s3 cp s3://${S3_BASE}/${file} /${DUMP_DIR}/${file}
  if [ $? -eq 0 ]; then
      log_date "${file} - downloaded" | tee -a $DOWNLOADED_LOG
  fi
  check_imported_indice ${file} &
done

check_running_elasticdump
check_progress_list_before_exit
