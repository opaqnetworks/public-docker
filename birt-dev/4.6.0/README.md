# BIRT 4.6.0 (Development)

Generic BIRT reporting engine with Tomcat 7.  It is intended to be used as a development container, that can be extended and deployed.
The BIRT engine is downloaded and saved as /birt-staging/birt-runtime-4.6.0.zip.

## Building

Building the BIRT Reporting Web Application is handled with the following steps:
1. Extract the The /birt-staging/birt-runtime-4.6.0.zip file to a temporary directory.
2. Copy any report files to <extracted directory>/WebViewerExample.
3. Copy any required libraries to <extracted directory>/WebViewerExample/WEB-INF/lib.
4. If needed, update <extracted directory>/WebViewerExample/WEB-INF/viewer.properties and <extracted directory>/WebViewerExample/WEB-INF/web.xml.
5. Package the WebViewerExample directory into a war.
6. Copy the new war file to /usr/local/tomcat/webapps, and start tomcat.

Other customizations can be made to the web application files. For more information, please refer to the [BIRT documentation](http://www.eclipse.org/birt/).

The `BIRT_RUNTIME_URL` provides the full URL to the zip file used to generate this docker image.  `wget "${BIRT_RUNTIME_URL} -O "/birt-staging/birt.zip"`.


## Using

Only HTTP access is supported, and exposed over port 8080.

The URL is defined by the name given to the war file.  For example, if the war file is "birt-reporting.war", then the url would be
"http://<hostname>:8080/birt-reporting/<report file>".

The reports and lib directories can be remounted at runtime to change the reports and widgets available to BIRT.
