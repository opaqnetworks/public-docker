FROM quay.io/opaqnetworks/alpine:3.11-202001192021
LABEL maintainer "Opaq <devops@opaqnetworks.com>"
LABEL revision "202003271902"

ENV GCLOUD_SDK_VERSION=280.0.0
ENV PATH /google-cloud-sdk/bin:$PATH
ENV HELM_VERSION v2.11.0

RUN sed -i "/edge/d" /etc/apk/repositories && \
    apk add --no-cache ansible \
      bash \
      curl \
      git \
      gnupg \
      less \
      libc6-compat \
      openssh-client \
      openssl \
      python \
      py-crcmod

RUN curl -O https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-${GCLOUD_SDK_VERSION}-linux-x86_64.tar.gz && \
    tar xzf google-cloud-sdk-${GCLOUD_SDK_VERSION}-linux-x86_64.tar.gz && \
    rm google-cloud-sdk-${GCLOUD_SDK_VERSION}-linux-x86_64.tar.gz && \
    ln -s /lib /lib64 && \
    gcloud config set core/disable_usage_reporting true && \
    gcloud config set core/disable_prompts true && \
    gcloud config set survey/disable_prompts true && \
    gcloud config set component_manager/disable_update_check true && \
    gcloud config set metrics/environment github_docker_image && \
    gcloud components install kubectl && \
    gcloud --version

RUN curl -O https://kubernetes-helm.storage.googleapis.com/helm-${HELM_VERSION}-linux-amd64.tar.gz && \
    tar -xzf helm-${HELM_VERSION}-linux-amd64.tar.gz && \
    mv linux-amd64/helm /bin && \
    rm -fr helm-${HELM_VERSION}-linux-amd64.tar.gz linux-amd64 && \
    helm version -c --short
