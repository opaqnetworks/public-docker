#!/usr/bin/dumb-init /bin/bash
set -x

echo "  server type=${1:0:1}"
echo "  DRUID_HOSTNAME=$DRUID_HOSTNAME"
echo "  DB_TYPE=$DB_TYPE"
echo "  DEEP_STORAGE=$DEEP_STORAGE"
echo "  GOOGLE_APPLICATION_CREDENTIALS=$GOOGLE_APPLICATION_CREDENTIALS"
echo "  GCS_BUCKET_NAME=$GCS_BUCKET_NAME"
echo "  GCS_BUCKET_PREFIX=$GCS_BUCKET_PREFIX"
echo "  DRUID_USE_CONTAINER_IP=$DRUID_USE_CONTAINER_IP"
echo "  JAVA_OPTS=$JAVA_OPTS"

if [ "${1:0:1}" = '' ]; then
  echo "Aborting: No druid server type set!"
  exit 1
fi

if [ "$DRUID_HOSTNAME" == "-" ]; then
  echo "Aborting: env var DRUID_HOSTNAME is not populatd"
  exit 1
fi

# mounted configs are immutable.
# however some values are known only after container has been started
# configuration files will be copied to another place and modified after that
cp -r /opt/druid/conf/cm/* /opt/druid/conf/
cp -r /opt/druid/hadoop-xml/* /opt/druid/conf/druid/_common/

COMMON_CONF=/opt/druid/conf/druid/_common/common.runtime.properties
COMPONENT_CONF=/opt/druid/conf/druid/"$1"/runtime.properties

if [ -z "${DB_TYPE}" ]; then
  DB_TYPE="postgresql"
fi

if [ -z "${DEEP_STORAGE}" ]; then
  DEEP_STORAGE="google"
fi

if [ "${DEEP_STORAGE}" == "google" ]; then
  if [ -n "${GOOGLE_APPLICATION_CREDENTIALS}" ]; then
    if [ -f "${GOOGLE_APPLICATION_CREDENTIALS}" ]; then
      gcloud auth activate-service-account --key-file "${GOOGLE_APPLICATION_CREDENTIALS}"
    else
      echo "Aborting: Unable find file in path ${GOOGLE_APPLICATION_CREDENTIALS}!"
      exit 1
    fi
  else
    echo "Aborting: Path for GOOGLE_APPLICATION_CREDENTIALS is not set!"
    exit 1
  fi

  if [ -z "${GCS_BUCKET_NAME}" ]; then
    echo "Aborting: No GCS bucket name set!"
    exit 1
  fi

  if [ -z "${GCS_BUCKET_PREFIX}" ]; then
    GCS_BUCKET_PREFIX="druid"
  fi
fi

echo "populating password..."
sed -ri 's#druid.metadata.storage.connector.password=.*#druid.metadata.storage.connector.password='${DB_PASSWORD}'#g' "$COMMON_CONF"


if [ "$DRUID_USE_CONTAINER_IP" == "true" ]; then
    ipaddress=$(ip a|grep "global eth0"|awk '{print $2}'|awk -F '\/' '{print $1}')
    echo "populating ipaddress $ipaddress ..."
    sed -ri 's/druid.host=.*/druid.host='${ipaddress}'/g' "$COMPONENT_CONF"
else
  echo "populating hostname $DRUID_HOSTNAME ..."
  sed -ri 's/druid.host=.*/druid.host='${DRUID_HOSTNAME}'/g' "$COMPONENT_CONF"
fi

java ${JAVA_OPTS} -cp /opt/druid/conf/druid/_common:/opt/druid/conf/druid/"$1":/opt/druid/lib/* org.apache.druid.cli.Main server "$@"
