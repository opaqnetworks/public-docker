#!/bin/bash
set -x

COMMON_CONF=/opt/druid/conf/druid/_common/common.runtime.properties

# Abort if no server type is given
if [ "${1:0:1}" = '' ]; then
    echo "Aborting: No druid server type set!"
    exit 1
fi

if [ -z "${DB_TYPE}" ]; then
  DB_TYPE="postgresql"
fi

if [ -z "${DEEP_STORAGE}" ]; then
  DEEP_STORAGE="google"
fi

if [ "${DEEP_STORAGE}" == "google" ]; then
  if [ ! -z ${GOOGLE_APPLICATION_CREDENTIALS} ]; then
    if [ -f ${GOOGLE_APPLICATION_CREDENTIALS} ]; then
      gcloud auth activate-service-account --key-file ${GOOGLE_APPLICATION_CREDENTIALS}
    else
      echo "Aborting: Unable find file in path ${GOOGLE_APPLICATION_CREDENTIALS}!"
      exit 1
    fi
  else
    echo "Aborting: Path for GOOGLE_APPLICATION_CREDENTIALS is not set!"
    exit 1
  fi

  if [ -z ${GCS_BUCKET_NAME} ]; then
    echo "Aborting: No GCS bucket name set!"
    exit 1
  fi

  if [ -z ${GCS_BUCKET_PREFIX} ]; then
    GCS_BUCKET_PREFIX="druid"
  fi

  # disable hdfs storage
  sed -ri 's|druid.storage.type=hdfs|#druid.storage.type=hdfs|' $COMMON_CONF
  sed -ri 's|druid.storage.storageDirectory|#druid.storage.storageDirectory|' $COMMON_CONF
  sed -ri 's|druid.indexer.logs.type=hdfs|#druid.indexer.logs.type=hdfs|' $COMMON_CONF
  sed -ri 's|druid.indexer.logs.directory|#druid.indexer.logs.directory|' $COMMON_CONF

  # enable google extension
  sed -ri 's|druid-hdfs-storage|druid-google-extensions|' $COMMON_CONF

  # enable google storage
  sed -ri 's|#druid.storage.type=google|druid.storage.type=google|' $COMMON_CONF
  sed -ri 's|#druid.google.bucket=.*|druid.google.bucket='${GCS_BUCKET_NAME}'|' $COMMON_CONF
  sed -ri 's|#druid.google.prefix=.*|druid.google.prefix='${GCS_BUCKET_PREFIX}\/segments'|' $COMMON_CONF
  sed -ri 's|#druid.indexer.logs.type=google|druid.indexer.logs.type=google|' $COMMON_CONF
  sed -ri 's|#druid.indexer.logs.bucket=.*|druid.indexer.logs.bucket='${GCS_BUCKET_NAME}'|' $COMMON_CONF
  sed -ri 's|#druid.indexer.logs.prefix=.*|druid.indexer.logs.prefix='${GCS_BUCKET_PREFIX}\/indexing-logs'|' $COMMON_CONF

fi

sed -ri 's#druid.zk.service.host.*#druid.zk.service.host='${ZOOKEEPER_HOST}'#g' $COMMON_CONF

DB_CONNECT_URI="jdbc:${DB_TYPE}\:\/\/${DB_HOST}\:${DB_PORT}\/${DB_DBNAME}"

sed -ri 's#druid.metadata.storage.type.*#druid.metadata.storage.type='${DB_TYPE}'#g' $COMMON_CONF
sed -ri 's#druid.metadata.storage.connector.connectURI.*#druid.metadata.storage.connector.connectURI='${DB_CONNECT_URI}'#g' $COMMON_CONF
sed -ri 's#druid.metadata.storage.connector.user.*#druid.metadata.storage.connector.user='${DB_USERNAME}'#g' $COMMON_CONF
sed -ri 's#druid.metadata.storage.connector.password.*#druid.metadata.storage.connector.password='${DB_PASSWORD}'#g' $COMMON_CONF


if [ "$DRUID_HOSTNAME" != "-" ]; then
    sed -ri 's/druid.host=.*/druid.host='${DRUID_HOSTNAME}'/g' /opt/druid/conf/druid/$1/runtime.properties
fi

if [ "$DRUID_LOGLEVEL" != "-" ]; then
    sed -ri 's/druid.emitter.logging.logLevel=.*/druid.emitter.logging.logLevel='${DRUID_LOGLEVEL}'/g' $COMMON_CONF
fi

if [ "$DRUID_USE_CONTAINER_IP" != "-" ]; then
    ipaddress=$(ip a|grep "global eth0"|awk '{print $2}'|awk -F '\/' '{print $1}')
    sed -ri 's/druid.host=.*/druid.host='${ipaddress}'/g' /opt/druid/conf/druid/$1/runtime.properties
fi

if [ "${1}" == "broker" ]; then
  echo 'druid.sql.enable=true' >> /opt/druid/conf/druid/broker/runtime.properties
fi

cat $COMMON_CONF

java ${JAVA_OPTS} -cp /opt/druid/conf/druid/_common:/opt/druid/conf/druid/$1:/opt/druid/lib/* org.apache.druid.cli.Main server $@
