FROM google/cloud-sdk:319.0.0-alpine as source

FROM quay.io/opaqnetworks/alpine:3.11-202001192021
LABEL maintainer "Opaq Admin <admin@opaq.com>"
LABEL revision "202105131300"

ENV DRUID_XMX          '-'
ENV DRUID_XMS          '-'
ENV DRUID_NEWSIZE      '-'
ENV DRUID_MAXNEWSIZE   '-'
ENV DRUID_HOSTNAME     '-'
ENV DRUID_LOGLEVEL     '-'
ENV DRUID_USE_CONTAINER_IP '-'

ENV DRUID_VERSION apache-druid-0.20.2

ENV GC_SDK_PATH /google-cloud-sdk
ENV PATH ${GC_SDK_PATH}/bin:$PATH

ENV GCS_PATH https://storage.googleapis.com/opaq-dev-public-docker/druid

RUN apk update --no-cache --purge \
    && apk add --no-cache curl python openjdk11-jre=11.0.5_p10-r0 jq

COPY --from=source ${GC_SDK_PATH} ${GC_SDK_PATH}
RUN gcloud config set core/disable_usage_reporting true \
    && gcloud config set core/disable_prompts true \
    && gcloud config set survey/disable_prompts true \
    && gcloud config set component_manager/disable_update_check true \
    && gcloud config set metrics/environment github_docker_image

RUN wget "${GCS_PATH}/${DRUID_VERSION}-bin.tar.gz" \
    && tar -xzf ${DRUID_VERSION}-bin.tar.gz -C /opt \
    && ln -s /opt/${DRUID_VERSION} /opt/druid \
    && rm -rf /opt/druid/hadoop-dependencies/ \
        /opt/druid/extensions/druid-azure-extensions/ \
        /opt/druid/extensions/druid-basic-security/ \
        /opt/druid/extensions/druid-bloom-filter/ \
        /opt/druid/extensions/druid-ec2-extensions/ \
        /opt/druid/extensions/druid-histogram/ \
        /opt/druid/extensions/druid-kerberos/ \
        /opt/druid/extensions/druid-kinesis-indexing-service/ \
        /opt/druid/extensions/druid-lookups-cached-single/ \
        /opt/druid/extensions/druid-orc-extensions/ \
        /opt/druid/extensions/druid-pac4j/ \
        /opt/druid/extensions/druid-parquet-extensions/ \
        /opt/druid/extensions/druid-protobuf-extensions/ \
        /opt/druid/extensions/druid-ranger-security/ \
        /opt/druid/extensions/druid-s3-extensions/ \
        /opt/druid/extensions/mysql-metadata-storage/ \
        /opt/druid/extensions/simple-client-sslcontext/ \
    && rm /${DRUID_VERSION}-bin.tar.gz

COPY start-druid.sh /start-druid.sh

ENTRYPOINT ["/start-druid.sh"]
