#!/usr/bin/env bash

#
# This functionality reads stored SourceEvent archives in daterange (start_date, end_date)
# from GCS bucket (gcs_source_path), filters them by provided condition (grep_filter_condition) and saves in
# another GCS bucket (gcs_dest_path).
#
# grep_filter_condition is escaped regex for grep
# So for example
#    "\"dataType\":\"(EPC_HR|EPC_RR)\""
# transforms to command
#    grep -e '"dataType":"(EPC_HR|EPC_RR)"'
# And command above takes only events with type EPC_HR or EPC_RR
#


# ===== configuration (launch from local) =====
#START_DATE="2021-01-07"
#END_DATE="2021-07-03"
#GCS_SOURCE_PATH="gs://opaq-reporting-integration/event_backup"
#LOCAL_PATH="./f
#GCS_DEST_PATH="gs://opaq-reporting-integration/endpoint_events_filtered"
#GREP_FILTER_CONDITION="\"dataType\":\"EPC_[HR]R\""
#FILE_SUFFIX="T000000000Z.111111"
#IS_KEEP_STATE="true"
# ===== end of configuration =====

TIMEFORMAT=%0lR

function download_files_from_gcs() {
  date +"%D %T copying from $GCS_SOURCE_PATH/messages.${PROCESSED_DATE}T*"
  gsutil -q -m cp "$GCS_SOURCE_PATH/messages.${PROCESSED_DATE}T*" .
  date +"%D %T copied from $GCS_SOURCE_PATH/messages.${PROCESSED_DATE}T*"
}

function upload_file_to_gcs() {
  gsutil cp "./messages.${PROCESSED_DATE}$FILE_SUFFIX.tar.gz" "$GCS_DEST_PATH/"
}

function filter_by_condition() {
  date +"%D %T ## filtering $PROCESSED_DATE ($(find . -type f -name 'messages.*.tar.gz' | wc -l) files)"
  find . -name "messages.*.tar.gz" -print0 | sort -z | while read -d $'\0' archive; do
    tar xOf "$archive">111.txt
#    date +"%D %T  - filtering $archive ($(cat 111.txt | wc -l) lines)"
  # shellcheck disable=SC2002
    cat 111.txt | grep -e "$GREP_FILTER_CONDITION">>"messages.$PROCESSED_DATE$FILE_SUFFIX.txt"
#    date +"%D %T    stored to file messages.$PROCESSED_DATE$FILE_SUFFIX.txt ($(cat "messages.$PROCESSED_DATE$FILE_SUFFIX.txt" | wc -l) lines)"
  done
}

function zip_filtered_data() {
  local file_gz="messages.$PROCESSED_DATE$FILE_SUFFIX.tar.gz"
  local file_txt="messages.$PROCESSED_DATE$FILE_SUFFIX.txt"
  # if empty - skip
  if [ -s "$file_txt" ]; then
    date +"%D %T file $file_txt is not empty. Creating $file_gz"
    tar zcf "$file_gz" "$file_txt"
    upload_file_to_gcs
    print_statistic
  else
    date +"%D %T file $file_txt is empty. skip"
  fi
}

function print_statistic() {
  # shellcheck disable=SC2002
  date +"%D %T Total: messages.$PROCESSED_DATE$FILE_SUFFIX.txt ($(cat "messages.$PROCESSED_DATE$FILE_SUFFIX.txt" | wc -l) lines)"
}

function cleanup() {
  rm -f ./messages.*.txt
  rm -f ./messages.*.tar.gz
}

function set_next_processed_date() {
  ## doesn't work in Alpine Linux
#  PROCESSED_DATE=`date -j -v+1d -f "%Y-%m-%d" "$PROCESSED_DATE" "+%Y-%m-%d"`
# shellcheck disable=SC2002
  PROCESSED_DATE=$(python -c "import datetime; print (datetime.datetime.strptime('$PROCESSED_DATE', '%Y-%m-%d') + datetime.timedelta(days=1)).strftime('%Y-%m-%d')")
}

function init_processed_date() {
  STATE_FILE=$(echo "state-$GCS_SOURCE_PATH-$GCS_DEST_PATH-$START_DATE-$END_DATE-$GREP_FILTER_CONDITION-$FILE_SUFFIX.txt" | sed 's/[^a-zA-Z0-9.-]//g')
  echo "STATE_FILE=$STATE_FILE"

  if [ ! -s "./$STATE_FILE" ]; then
    echo "File $STATE_FILE does not exist. Creating.."
    echo "$START_DATE" >"./$STATE_FILE"
  fi

  if [[ "$IS_KEEP_STATE" == "true" ]]; then
    echo "Take PROCESSED_DATE from $STATE_FILE file"
    # shellcheck disable=SC2006
    PROCESSED_DATE=`cat "./$STATE_FILE"`
  else
    echo "Reset PROCESSED_DATE to initial START_DATE"
    PROCESSED_DATE="$START_DATE"
  fi
}

function store_processed_date() {
  echo "$PROCESSED_DATE">"./$STATE_FILE"
}

function main() {
  echo "START_DATE=$START_DATE"
  echo "END_DATE=$END_DATE"
  echo "GCS_SOURCE_PATH=$GCS_SOURCE_PATH"
  echo "LOCAL_PATH=$LOCAL_PATH"
  echo "GCS_DEST_PATH=$GCS_DEST_PATH"
  echo "GREP_FILTER_CONDITION=$GREP_FILTER_CONDITION"
  echo "FILE_SUFFIX=$FILE_SUFFIX"
  echo "IS_KEEP_STATE=$IS_KEEP_STATE"

  gcloud auth activate-service-account --key-file /var/secrets/google/key.json

  date +"%D %T ## main"

  mkdir -p "$LOCAL_PATH"
  cd "$LOCAL_PATH"
  PROCESSED_DATE="$START_DATE"

  init_processed_date
  cleanup
  while [[ ! "$PROCESSED_DATE" > "$END_DATE" ]]; do
    echo -e $(date +"%D %T \033[0;31m## processed_date=$PROCESSED_DATE \033[0mend_date=$END_DATE")
    download_files_from_gcs
    filter_by_condition
    zip_filtered_data
    cleanup
    sleep 1
    set_next_processed_date
    store_processed_date
  done
  date +"%D %T ## Done."
  date +"%D %T ## going to sleep..."
  /usr/bin/tail -f /dev/null
}
