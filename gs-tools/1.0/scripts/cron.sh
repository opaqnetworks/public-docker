#!/usr/bin/env bash

if [ -z "$CRON_EXPRESSION" ]; then
    echo "Cron expression not provided. Using default."
    CRON_EXPRESSION="*/55 * * * *"
fi

if [ -z "$CRON_COMMAND" ]; then
    echo "Cron command not provided. Using default."
    CRON_COMMAND="python /scripts/tenants/main.py"
fi

CRON="$CRON_EXPRESSION $CRON_COMMAND > /proc/\$(cat /var/run/crond.pid)/fd/1 2>&1"
echo "Cron expression: $CRON"

echo "Updating crontab..."
crontab -l > mycronfile
echo "$CRON" >> mycronfile
crontab mycronfile && rm mycronfile
echo "Crontab updated."

echo "Starting cron daemon..."
/usr/sbin/crond -f