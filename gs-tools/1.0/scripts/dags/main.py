import logging
import logging.config
import os

import api
import config


def main():
    setup_logging()

    logger = logging.getLogger(__name__)
    logger.info("Starting application...")

    logger.info("Reading configuration from env vars...")

    if os.getenv("INTERNAL_API_URL", None):
        config.DEFAULT_CONFIG['internal_api_url'] = os.getenv("INTERNAL_API_URL")

    if os.getenv("PROVIDER", None):
        config.DEFAULT_CONFIG['provider'] = os.getenv("PROVIDER")

    logger.info("Application started successfully.")

    provider = config.DEFAULT_CONFIG['provider']
    internal_api_url = "%s/%s" % (config.DEFAULT_CONFIG['internal_api_url'], config.DEFAULT_CONFIG['api_version'])

    updated_dags_count = 0
    try:
        logger.info("Listing Dags...")
        # get list of dags
        scheduled_pipelines = api.get_dags(internal_api_url=internal_api_url)
        if len(scheduled_pipelines) > 0:
            for dag in scheduled_pipelines:
                api.update_dag(internal_api_url=internal_api_url, provider=provider, dag=dag)
                updated_dags_count = updated_dags_count + 1
        else:
            logger.info('No Dags found, nothing to update')

    except Exception as e:
        logger.error('Failed to update existing Dags due to: %s', e.message, exc_info=True)

    logger.info("Application completed. " + str(updated_dags_count) + " Dags updated \n")


def setup_logging():
    logging.config.dictConfig({
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
            },
        },
        'handlers': {
            'console': {
                'level': 'INFO',
                'class': 'logging.StreamHandler',
                'formatter': 'standard',
                "stream": "ext://sys.stdout"
            },
        },
        'loggers': {
            '': {
                'handlers': ['console'],
                'level': 'INFO',
                'propagate': True
            }
        },
        'root': {
            'level': 'INFO',
            'handlers': ['console']
        }
    })


if __name__ == '__main__':
    main()