import logging
from functools import wraps

import requests
import time
import re
from datetime import datetime


def retry(max_attempts=5, delay=5):
    def wrapper(func):

        @wraps(func)
        def wrapped(*args, **kwargs):
            logger = logging.getLogger(__name__)
            for i in xrange(1, max_attempts + 1):
                try:
                    return func(*args, **kwargs)
                except Exception as e:
                    logger.error('Error: %s', e.message, exc_info=1)
                    interval = i * delay
                    logger.warn('Retrying in %ss....', interval)
                    time.sleep(interval)
            if i == max_attempts:
                logger.warn('Reached max attempts.')

        return wrapped

    return wrapper


@retry()
def _get_data(url):
    return requests.get(url)


def update_dag(internal_api_url, provider, dag):
    """
    Updates only daily Dags via sending a request to internal-api
    """
    logger = logging.getLogger(__name__)
    match = re.search(provider.replace('.', '-') + '-(\w+)-greyspark-pipeline-complete-*', dag)
    if match:
        tenant = match.group(1)
        pipelines_url = '{}/pipelines/schedule'.format(internal_api_url)
        logger.info("Updating dag %s", dag)
        start_date = datetime.today().strftime('%Y-%m-%d')
        _schedule_pipeline(pipelines_url=pipelines_url,
                           provider=provider,
                           tenant=tenant,
                           internal_api_url=internal_api_url,
                           start_date=start_date)


def _schedule_pipeline(pipelines_url, provider, tenant, internal_api_url, start_date, end_date=None):
    """
    Schedules pipeline: triggers internal api which renders template, and sends request to Airflow rest client
    """
    logger = logging.getLogger(__name__)
    # retrieve the tenant config so that values can be pulled for scheduling the pipeline
    tenant_config_url = '{}/providers/{}/tenants/{}'.format(internal_api_url, provider, tenant)
    logger.debug('URL to get tenant %s is: %s' % (tenant, tenant_config_url))
    tenant_config = _get_data(tenant_config_url).json()
    logger.debug('Tenant configs\n%s' % tenant_config)
    logger.debug('Schedule pipeline for tenant: %s, provider: %s, start_date: %s, end_date: %s, url: %s with config %s', tenant, provider, start_date, end_date,
                 internal_api_url, tenant_config)

    multipart_data = {
        'providerId': (None, provider),
        'tenantId': (None, tenant),
        'templateId': (None, 'greyspark-pipeline-complete'),
        'startDate': (None, start_date),
        'configUrl': (None, internal_api_url),
        'sourceBucket': (None, tenant_config['sourceBucket']),
        'sourceBucketLogPath': (None, tenant_config['sourceBucketLogPath'])
    }
    if end_date:
        multipart_data.update({'endDate': (None, end_date)})
    response = _post_multipart_data(url=pipelines_url, multipart_data=multipart_data)

    if response is None:
        logger.error('Failed to schedule pipeline for tenant: %s', tenant)
    elif response.status_code == 200:
        logger.info('Successfully scheduled pipeline for tenant: %s', tenant)
    else:
        logger.error('Failed to schedule pipeline for tenant: %s, status code: %s, content: %s',
                     tenant, response.status_code, response.content)


@retry()
def _post_multipart_data(url, multipart_data):
    return requests.post(url, files=multipart_data)


def get_dags(internal_api_url, retries=3, delay=30):
    """
    Method to list Airflow Dags through internal api call
    """
    logger = logging.getLogger(__name__)
    list_dags_url = '{}/pipelines/list'.format(internal_api_url)
    logger.info(list_dags_url)
    response = _get_data(list_dags_url)
    dags = set()
    tries = 0
    while tries < retries:
        if response is None:
            logger.error('Failed to get dags from Airflow. Airflow webserver may be unavailable.')
        elif response.status_code != 200:
            logger.error('Failed to get dags from Airflow. Status code=%s, content=%s',
                         response.status_code, response.content)
        else:
            logger.debug('Dags from the airflow response: %s', response.json())
            dags = set(dag for dag in response.json()['dags'])
            break
        tries += 1
        time.sleep(delay)
    if not response:
        response.raise_for_status()
    logger.debug('Airflow has %s dags.', len(dags))
    return dags