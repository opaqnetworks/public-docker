'''
 HDFS utilities
'''
from snakebite.client import Client
from snakebite.errors import FileNotFoundException

def hadoop_client(host, port):
    '''
      Makes Hadoop client.

      :param host: Hadoop NameNode Host
      :type host: string
      :param port: Hadoop NameNode Port
      :type port: int

      :returns: hadoop client
    '''
    return Client(host, port)

def gs_data_path(provider, tenant, store, shred, name, date):
    '''
      Makes gs-data path.

       :param provider: Provider name
       :type provider: string
       :param tenant: Tenant name
       :type tenant: string
       :param store: Store name
       :type store: string
       :param shred: Shred name
       :type shred: string
       :param name: Property name
       :type name: string
       :param date: Date
       :type date: string

       :returns: path to gs-data for provider, tenant
    '''
    return '/{}/{}/{}/{}/{}/{}'.format(provider, tenant, store, shred, name, date)

def factstore_path(provider, tenant, store, shred, name, date):
    '''
      Makes facstore path.

      :param provider: Provider name
      :type provider: string
      :param tenant: Tenant name
      :type tenant: string
      :param store: Store name
      :type store: string
      :param shred: Shred name
      :type shred: string
      :param name: Fact name
      :type name: string
      :param date: Date
      :type date: string

      :returns: path to facts for provider, tenant
    '''
    return '/{}/{}/{}/{}/{}/partitionDay={}'.format(provider, tenant, store, shred, name, date)

def data_paths(provider, tenant, store, shred, properties, dates, func):
    '''
     Makes all paths for provider, tenant, properties and date range.

     :param provider: Provider name
     :type provider: string
     :param tenant: Tenant name
     :type tenant: string
     :param store: Store name
     :type store: string
     :param shred: Shred name
     :type shred: string
     :param properties: List of fact or property names
     :type properties: list
     :param dates: List of dates
     :type dates: list
     :param func: Function that creates path
     :type func: function

     :returns: a generator that creates paths
    '''
    for pname in properties:
        for date in dates:
            yield func(provider, tenant, store, shred, pname, date)

def clean_data(client, paths):
    '''
     Cleans data for paths.

     :param client: Hadoop client
     :type client: Client
     :param paths: Paths to clean data
     :type paths: list
    '''
    for path in paths:
        try:
            list(client.delete([path], recurse=True))
        except FileNotFoundException:
            print 'Path {} not found'.format(path)

def clean_gs_data(client, provider, tenant, dates):
    '''
      Cleans gs-data for provider, tenant and date ranges.

      :param client: Hadoop client
      :type client: Client
      :param provider: Provider name
      :type provider: string
      :param tenant: Tenant name
      :type tenant: string
      :param dates: List of dates to clean
      :type dates: list
    '''
    store = 'gs-data'
    shred = 'master-shred'

    properties = [
        'networkCommEdge',
        'networkSessionProperty',
        'threatDetectionEdge',
        'threatProperty',
        'threatResponse',
        'toolNetworkSessionEdge',
        'toolProperty',
        'toolResponseEdge',
        'toolThreatReportEdge']

    paths = list(data_paths(provider, tenant, store, shred, properties, dates, gs_data_path))
    clean_data(client, paths)


def clean_factstore(client, provider, tenant, dates):
    '''
      Cleans facts for provider, tenant and date range.

      :param client: Hadoop client
      :type client: Client
      :param provider: Provider name
      :type provider: string
      :param tenant: Tenant name
      :type tenant: string
      :param dates: List of dates to clean
      :type dates: list
    '''
    store = 'factstore'
    shred = 'master-shred'

    facts = [
        'DefenseEffectivenessIndexDaily',
        'DefenseEffectivenessIndexHourly',
        'DetectionsFromNewThreatsIndexDaily',
        'DetectionsFromNewThreatsIndexHourly',
        'DetectionsFromNewThreatsPerTypePerDomainDaily',
        'DetectionsFromNewThreatsPerTypePerDomainHourly',
        'HackabilityIndexDaily',
        'HackabilityIndexHourly',
        'LengthOfScoreHistoryIndexDaily',
        'LengthOfScoreHistoryIndexHourly',
        'OpportunityRiskIndexDaily',
        'OpportunityRiskIndexHourly',
        'SurfaceAreaIndexDaily',
        'SurfaceAreaIndexHourly',
        'SurfaceAreaPerToolTypeDaily',
        'SurfaceAreaPerToolTypeHourly',
        'TechnicalDebtIndexDaily',
        'TechnicalDebtIndexHourly',
        'TotalMaxSeverityPerToolTypeDaily',
        'TotalMaxSeverityPerToolTypeHourly',
        'TotalRedetectionCountPerToolTypePerMachineDaily',
        'TotalRedetectionCountPerToolTypePerMachineHourly',
        'TotalThreatCountPerThreatTypePerMachinePerDomainDaily',
        'TotalThreatCountPerThreatTypePerMachinePerDomainHourly',
        'TotalThreatCountPerThreatTypePerToolTypePerDomainDaily',
        'TotalThreatCountPerThreatTypePerToolTypePerDomainHourly',
        'TotalThreatCountPerToolTypeDaily',
        'TotalThreatCountPerToolTypeHourly',
        'TotalUniqueAssetsPerToolMetricDaily',
        'TotalUniqueAssetsPerToolMetricHourly',
        'TotalUniqueAssetsPerToolTypePerDomainPerMachineDaily',
        'TotalUniqueAssetsPerToolTypePerDomainPerMachineHourly',
        'TotalUniqueMachinesWithDefenseEffectivenessEventsPerToolTypeDaily',
        'TotalUniqueMachinesWithDefenseEffectivenessEventsPerToolTypeHourly',
        'TotalUniqueThreatsPerThreatTypePerDomainDaily',
        'TotalUniqueThreatsPerThreatTypePerDomainHourly',
        'TotalUniqueThreatsPerToolDaily',
        'TotalUniqueThreatsPerToolHourly',
        'WindowedTotalThreatCountPerToolTypeDaily',
        'WindowedTotalThreatCountPerToolTypeHourly',
        'WindowedTotalUniqueAssetsPerToolMetricDaily',
        'WindowedTotalUniqueAssetsPerToolMetricHourly',
        'WindowedTotalUniqueThreatsPerToolDaily',
        'WindowedTotalUniqueThreatsPerToolHourly']

    paths = list(data_paths(provider, tenant, store, shred, facts, dates, factstore_path))
    clean_data(client, paths)
