#!/usr/bin/env python
'''
 This scripts clean HDFS data for provider, tenant and date range.
'''
import argparse
import sys

import date_utils as du
import hadoop_utils as hu

def main():
    '''
      Clean gs-data, factstore for provider, tenant, date range
    '''
    parser = argparse.ArgumentParser(description='Clean HDFS data')
    parser.add_argument('--host', required=True, help='Hostname or IP address of the NameNode')
    parser.add_argument('--port', type=int, required=True, help='RPC Port of the NameNode')
    parser.add_argument('--provider', required=True, help='Provider name')
    parser.add_argument('--tenant', required=True, help='Tenant name')
    parser.add_argument('--startdate', required=True, type=du.valid_date, help='Start date')
    parser.add_argument('--enddate', required=True, type=du.valid_date, help='End date')

    args = parser.parse_args()
    host = args.host
    port = args.port
    provider = args.provider
    tenant = args.tenant
    startdate = args.startdate
    enddate = args.enddate

    if startdate > enddate:
        print("Error: start date > end date")
        sys.exit(1)

    client = hu.hadoop_client(host, port)
    print 'Created hadoop client: host={}, port={}'.format(host, port)

    dates = du.dates(startdate, enddate)

    hu.clean_gs_data(client, provider, tenant, dates)
    print "Cleaned gs-data: provider={}, tenant={}, dates={}".format(provider, tenant, dates)

    hu.clean_factstore(client, provider, tenant, dates)
    print "Cleaned facts: provider={}, tenant={}, dates={}".format(provider, tenant, dates)

    print("Done")


if __name__ == '__main__':
    main()
