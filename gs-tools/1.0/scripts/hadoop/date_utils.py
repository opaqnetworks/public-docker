'''
  Date utils.
'''

import argparse
from datetime import datetime, timedelta

def dates(start, end):
    '''
      Get list of string dates between two dates(inclusive).

      :param start: Start date
      :type start: datetime
      :param end: End time
      :type end: datetime

      :returns: list of string dates between start and end(inlusive)
    '''
    res = []
    if start <= end:
        total_days = end - start
        res = [(start + timedelta(days=x)).strftime("%Y-%m-%d") for x in range(0, (total_days.days+1))]
    return res

def valid_date(date):
    '''
      Checks if date is valid by applying format.

      :param date: Date to valid_date
      :type date: string

      :returns: datetime
    '''
    try:
        return datetime.strptime(date, "%Y-%m-%d")
    except ValueError:
        msg = "Not a valid date: '{0}'.".format(date)
        raise argparse.ArgumentTypeError(msg)
