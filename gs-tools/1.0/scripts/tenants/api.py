import logging
import os
import re
import time
from datetime import datetime
from functools import wraps

import boto
import boto.s3
import requests
from boto.s3.connection import OrdinaryCallingFormat


def retry(max_attempts=5, delay=5):
    def wrapper(func):

        @wraps(func)
        def wrapped(*args, **kwargs):
            logger = logging.getLogger(__name__)
            for i in xrange(1, max_attempts + 1):
                try:
                    return func(*args, **kwargs)
                except Exception as e:
                    logger.error('Error: %s', e.message, exc_info=1)
                    interval = i * delay
                    logger.warn('Retrying in %ss....', interval)
                    time.sleep(interval)
            if i == max_attempts:
                logger.warn('Reached max attempts.')

        return wrapped

    return wrapper


def dca_tenants(bucket_name):
    logger = logging.getLogger(__name__)
    logger.info('Getting DCA tenants...')

    conn = boto.connect_s3(aws_access_key_id=os.getenv('AWS_ACCESS_KEY_ID'),
                           aws_secret_access_key=os.getenv('AWS_SECRET_ACCESS_KEY'),
                           calling_format=OrdinaryCallingFormat())
    bucket = conn.lookup(bucket_name=bucket_name)
    if bucket is None:
        logger.warn('Bucket %s does not exist.', bucket_name)
        tenants = set()
    else:
        # strategy #1 - default search that assumes DCA folder structure
        # list all top-level paths in the bucket (tenant paths are at the top level)
        tenant_paths = bucket.list(prefix='', delimiter='/')
        for t in tenant_paths:
            name = t.name[:-1]
            logger.info('Tenant path: %s' % name)
        # list through all of the tenant paths to find those with 'raw' sub-paths
        tenants = set(p.name[:-1] for p in tenant_paths if _is_tenant(path=p.name, bucket=bucket))

    logger.info('DCA has %s tenants.', len(tenants))
    return tenants

def _is_tenant(path, bucket):
    logger = logging.getLogger(__name__)
    tenant_subpaths = bucket.list(prefix=path, delimiter='/')
    is_tenant = False
    for sp in tenant_subpaths:
        logger.debug('tenant subpath %s' % sp.name)
        if _istenant(sp.name):
            is_tenant = True
            break
    return is_tenant

def _istenant(key_name):
    return re.search('/raw/', key_name)


def _tenant_part(key_name):
    return key_name.split('/')[0]


def greyspark_tenants(url):
    logger = logging.getLogger(__name__)
    logger.info('Getting Greyspark tenants...')

    response = _get_data(url)
    active = set()
    inactive = set()

    if response is None:
        logger.error('Failed to get tenants from Greyspark. Greyspark is unavailable.')
    elif response.status_code != 200:
        logger.error('Failed to get tenants from Greyspark. Status code=%s, content=%s',
                     response.status_code, response.content)
    else:
        active, inactive = _process_api_response(response.json())

    logger.info('Greyspark has %s active tenant(s).', len(active))
    logger.info('Greyspark has %s inactive tenant(s).', len(inactive))
    return active, inactive


@retry()
def _get_data(url):
    return requests.get(url)


def _process_api_response(api_response):
    active = set(item['tenantId'] for item in api_response if item['active'])
    inactive = set(item['tenantId'] for item in api_response if not item['active'])
    return active, inactive


def get_difference(dca, greyspark):
    logger = logging.getLogger(__name__)
    tenants = dca.difference(greyspark)
    logger.info('DCA has %s tenants not found in Greyspark.', len(tenants))
    return tenants


def create_greyspark_tenants(url, provider, tenants, bucket):
    logger = logging.getLogger(__name__)

    if tenants:
        logger.info('Creating Greyspark tenants...')

        payload = _prepare_payload(provider=provider, tenants=tenants, bucket=bucket)
        logger.info('Payload to create tenants: %s' % payload)
        response = _post_data(url=url, payload=payload)

        if response is None:
            logger.error('Failed to create Greyspark tenants. Greyspark is unavailable.')
        elif response.status_code == 201:
            logger.info('Successfully created Greyspark tenants.')
        else:
            logger.error('Failed to create Greyspark tenants. Status code=%s, content=%s',
                         response.status_code, response.content)

    else:
        logger.info('Skipping Greyspark tenants creation....')


def _prepare_payload(provider, tenants, bucket):
    return [{'providerId': provider,
             'tenantId': tenant,
             'sourceBucket': bucket,
             'targetBucket': bucket + '/' + tenant,
             'active': True} for tenant in tenants]


@retry()
def _post_data(url, payload):
    return requests.post(url, json=payload, headers={'content-type': 'application/json'})


def schedule_pipeline_for_tenants(internal_api_url, provider, tenants, bucket, bucket_log_path):
    logger = logging.getLogger(__name__)

    pipelines_url = '{}/pipelines/schedule'.format(internal_api_url)
    logger.info("Pipelines url: %s", pipelines_url)

    if tenants:
        start_date = datetime.today().strftime('%Y-%m-%d')
        for tenant in tenants:
            _schedule_pipeline(pipelines_url=pipelines_url,
                               provider=provider,
                               tenant=tenant,
                               internal_api_url=internal_api_url,
                               start_date=start_date,
                               bucket=bucket,
                               bucket_log_path=bucket_log_path)
    else:
        logger.info('No tenants found. Skipping pipeline scheduling...')


def _schedule_pipeline(pipelines_url, provider, tenant, internal_api_url, start_date, bucket, bucket_log_path):
    logger = logging.getLogger(__name__)
    logger.info('Schedule pipeline for tenant: %s, provider: %s, date: %s, url: %s, bucket: %s, bucket log path: %s',
                tenant, provider, start_date, internal_api_url, bucket, bucket_log_path)

    multipart_data = {
        'providerId': (None, provider),
        'tenantId': (None, tenant),
        'templateId': (None, 'greyspark-pipeline-complete'),
        'startDate': (None, start_date),
        'configUrl': (None, internal_api_url),
        'sourceBucket': (None, bucket),
        'sourceBucketLogPath':(None, bucket_log_path)
        }

    response = _post_multipart_data(url=pipelines_url, multipart_data=multipart_data)

    if response is None:
        logger.error('Failed to schedule pipeline for tenant: %s', tenant)
    elif response.status_code == 200:
        logger.info('Successfully scheduled pipeline for tenant: %s', tenant)
    else:
        logger.error('Failed to schedule pipeline for tenant: %s, status code: %s, content: %s',
                     tenant, response.status_code, response.content)


@retry()
def _post_multipart_data(url, multipart_data):
    return requests.post(url, files=multipart_data)


def get_dags(internal_api_url, retries=3, delay=30):
    logger = logging.getLogger(__name__)
    list_dags_url = '{}/pipelines/list'.format(internal_api_url)
    response = _get_data(list_dags_url)
    dags = set()
    tries = 0
    while tries < retries:
        if response is None:
            logger.error('Failed to get dags from Airflow. Airflow webserver may be unavailable.')
        elif response.status_code != 200:
            logger.error('Failed to get dags from Airflow. Status code=%s, content=%s',
                         response.status_code, response.content)
        else:
            logger.debug('Dags from the airflow response: %s', response.json())
            dags = set(dag for dag in response.json()['dags'])
            break
        tries += 1
        time.sleep(delay)
    # if the airflow response code is still not valid after retries, throw an exception so that dags are not incorrectly scheduled
    if not response:
        response.raise_for_status()
    logger.info('Airflow has %s dags.', len(dags))
    return dags


def get_dags_difference(tenants, dags):
    logger = logging.getLogger(__name__)
    tenants_missing_dags = set()

    # filter out 'historical' dags
    scheduled_dags = [dag for dag in dags if '-ondemand-' not in dag and '-till-' not in dag]

    for t in tenants:
        tenant_match = False
        for d in scheduled_dags:
            if '-{}-'.format(t) in d:
                tenant_match = True
                break
        if not tenant_match:
            tenants_missing_dags.add(t)
    logger.info("There are %s tenants with no scheduled dag deployed: %s", len(tenants_missing_dags),
                tenants_missing_dags)
    return tenants_missing_dags


def pause_pipelines(internal_api_url, dags, tenants):
    logger = logging.getLogger(__name__)
    try:
        pause_dags = set()
        for d in dags:
            for t in tenants:
                if t in d:
                    pause_dags.add(d)

        logger.info("There are %s dags to pause", len(pause_dags))
        url = '{}/pipelines/pause'.format(internal_api_url)

        for dag in pause_dags:
            response = requests.get(url, params={'pipelineId': dag})
            if response is None:
                logger.error('Failed to pause dag: %s', dag)
            elif response.status_code == 200:
                logger.info('Successfully paused dag: %s', dag)
            else:
                logger.error('Failed to pause dag: %s, status code: %s, content: %s',
                             dag, response.status_code, response.content)
    except Exception as e:
        logger.error('Failed to pause dags for inactive tenants: %s', e.message, exc_info=True)