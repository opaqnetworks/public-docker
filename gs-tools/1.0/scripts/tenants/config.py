DEFAULT_CONFIG = {
    'internal_api_url': 'http://localhost:8080/api/v1.0',
    'provider': 'integration.fourvdev.com',
    's3_bucket': 'integration.fourvdev.com',
    's3_bucket_log_path': 'raw'
}
