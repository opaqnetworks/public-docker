import unittest

import api

class TestApi(unittest.TestCase):
    def test_tenants_that_have_historical_dags_are_scheduled(self):
        tenant_with_completed_historical_dag = 'qatenantap224'

        all_tenants = [tenant_with_completed_historical_dag, 'qatenantap', 'qatenantcp', 'qatenantdevsup', 'qatenantfrosty', 'qatenantnp', 'qatenantrk7570']
        historical_dags = ['fourvqa-greysparkcyber-com-{}-greyspark-pipeline-complete-ondemand-2017-11-13-till-2017-11-17'.format(tenant_with_completed_historical_dag)]

        tenants_to_schedule = api.get_dags_difference(all_tenants, historical_dags)
        self.assertEqual(len(tenants_to_schedule), len(all_tenants), 'tenants that have historical dags should be scheduled')

    def test_tenants_that_are_partially_matched_to_existing_dags_are_scheduled(self):
        partially_matched_tenant = 'qatenantap'

        all_tenants = [partially_matched_tenant, 'qatenantcp', 'qatenantdevsup', 'qatenantfrosty', 'qatenantnp', 'qatenantrk7570']
        dags = ['fourvqa-greysparkcyber-com-{}224-greyspark-pipeline-complete-scheduled'.format(partially_matched_tenant)]

        tenants_to_schedule = api.get_dags_difference(all_tenants, dags)
        self.assertEqual(len(tenants_to_schedule), len(all_tenants), 'tenants that are partially matched to existing dag should be scheduled')


if __name__ == '__main__':
    unittest.main()