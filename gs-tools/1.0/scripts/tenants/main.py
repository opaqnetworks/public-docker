#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import logging.config
import os

import api
import config


def main():
    setup_logging()

    logger = logging.getLogger(__name__)
    logger.info("Starting application...")

    logger.info("Reading configuration from env vars...")

    if os.getenv("INTERNAL_API_URL", None):
        config.DEFAULT_CONFIG['internal_api_url'] = os.getenv("INTERNAL_API_URL")

    if os.getenv("PROVIDER", None):
        config.DEFAULT_CONFIG['provider'] = os.getenv("PROVIDER")

        logger.info('Setting S3 bucket to provider: %s', os.getenv("PROVIDER"))
        config.DEFAULT_CONFIG['s3_bucket'] = os.getenv("PROVIDER")

    if os.getenv("S3_BUCKET", None):
        logger.info('Setting S3 bucket to: %s', os.getenv("S3_BUCKET"))
        config.DEFAULT_CONFIG['s3_bucket'] = os.getenv("S3_BUCKET")

    if os.getenv("S3_BUCKET_LOG_PATH", None):
        logger.info('Setting S3 bucket log path to: %s', os.getenv("S3_BUCKET_LOG_PATH"))
        config.DEFAULT_CONFIG['s3_bucket_log_path'] = os.getenv("S3_BUCKET_LOG_PATH")

    logger.info("Application started successfully.")

    provider = config.DEFAULT_CONFIG['provider']
    internal_api_url = config.DEFAULT_CONFIG['internal_api_url']
    bucket_name = config.DEFAULT_CONFIG['s3_bucket']
    bucket_log_path = config.DEFAULT_CONFIG['s3_bucket_log_path']

    tenants_url = '{}/providers/{}/tenants'.format(internal_api_url, provider)
    logger.info("Tenants url: %s", tenants_url)

    try:
        dca_tenants = api.dca_tenants(bucket_name=bucket_name)
        gs_tenants_active, gs_tenants_inactive = api.greyspark_tenants(url=tenants_url)

        tenants = api.get_difference(dca=dca_tenants, greyspark=gs_tenants_active)

        api.create_greyspark_tenants(url=tenants_url, provider=provider, tenants=tenants, bucket=bucket_name)
        api.schedule_pipeline_for_tenants(internal_api_url=internal_api_url, provider=provider, tenants=tenants,
                                          bucket=bucket_name, bucket_log_path=bucket_log_path)

        # schedule tenants in the DB that aren't yet scheduled and do it
        scheduled_pipelines = api.get_dags(internal_api_url=internal_api_url)
        missing_tenants = api.get_dags_difference(tenants=gs_tenants_active, dags=scheduled_pipelines)
        api.schedule_pipeline_for_tenants(internal_api_url=internal_api_url, provider=provider, tenants=missing_tenants,
                                          bucket=bucket_name, bucket_log_path=bucket_log_path)

        # pause inactive tenants
        api.pause_pipelines(internal_api_url=internal_api_url, dags=scheduled_pipelines,
                            tenants=gs_tenants_inactive)

    except Exception as e:
        logger.error('Failed to sync DCA tenants to Greyspark: %s', e.message, exc_info=True)

    logger.info("Application completed.\n")


def setup_logging():
    logging.config.dictConfig({
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
            },
        },
        'handlers': {
            'console': {
                'level': 'INFO',
                'class': 'logging.StreamHandler',
                'formatter': 'standard',
                "stream": "ext://sys.stdout"
            },
        },
        'loggers': {
            '': {
                'handlers': ['console'],
                'level': 'INFO',
                'propagate': True
            }
        },
        'root': {
            'level': 'INFO',
            'handlers': ['console']
        }
    })


if __name__ == '__main__':
    main()
