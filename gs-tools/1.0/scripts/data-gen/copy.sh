#!/usr/bin/env bash

DG_DESTINATION="${DG_DESTINATION:-/data-gen}"

echo "copy avro-tools-1.8.2.jar "
/bin/cp /scripts/data-gen/avro-tools-1.8.2.jar "$DG_DESTINATION/avro-tools-1.8.2.jar"

echo "copy extract_and_populate.sh"
/bin/cp /scripts/data-gen/extract_and_populate.sh "$DG_DESTINATION/extract_and_populate.sh"

echo "activate-service-account"
gcloud auth activate-service-account --key-file "$GOOGLE_APPLICATION_CREDENTIALS"

echo "copy data from $DG_GSC_DATA_PATH/* to $DG_DESTINATION/"
gsutil -m cp "$DG_GSC_DATA_PATH/*" "$DG_DESTINATION/"

echo .
echo .
echo "#### total data files :$(ls -Ubadq1 -- "$DG_DESTINATION/message"* | wc -l)"
