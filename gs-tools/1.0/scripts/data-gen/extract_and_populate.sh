#!/usr/bin/env bash

APP_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)"
cd "$APP_DIR" || (
    echo Error
    /usr/bin/tail -f /dev/null
)

AVRO_TOOLS='./avro-tools-1.8.2.jar'
DATAGEN_JAR='/usr/local/greyspark-cyber/lib/spark/datagen.jar'

echo "  JAVA_OPTS=$JAVA_OPTS"
echo "  AVRO_TOOLS=$AVRO_TOOLS"
echo "  DATAGEN_JAR=$DATAGEN_JAR"
echo "  DG_PARTNER_ID=$DG_PARTNER_ID"
echo "  DG_CUSTOMER_ID=$DG_CUSTOMER_ID"
echo "  DG_BOOTSTRAP_SERVER=$DG_BOOTSTRAP_SERVER"
echo "  DG_TOPIC=$DG_TOPIC"
echo "  DG_USE_AVRO=$DG_USE_AVRO"
echo "  DG_BATCH_SIZE=$DG_BATCH_SIZE"
echo "  DG_INTERVAL=$DG_INTERVAL"
echo "  DG_PRESERVE_ORDER=$DG_PRESERVE_ORDER"
echo "  DG_NO_DELAY=$DG_NO_DELAY"
echo "  DG_DELAY=$DG_DELAY"
date

function mute_log4j_for_avro() {
  #mute mis-configured log4j for avro-tools
  LOG4J_PROPS_FILE="/tmp/avro-tools.log4j.properties"
  if [[ ! -a "$LOG4J_PROPS_FILE" ]]; then
      echo "log4j.rootLogger=INFO, devnull" >> $LOG4J_PROPS_FILE
      echo "log4j.appender.devnull=org.apache.log4j.varia.NullAppender" >> $LOG4J_PROPS_FILE
  fi
  LOG_4J_PROPS="-Dlog4j.configuration=file:$LOG4J_PROPS_FILE"
}

function wait_for_kafka_topic() {
  local topic=$1

  java $JAVA_OPTS -cp "$DATAGEN_JAR" com.fourv.datagen.kafka.KafkaTopic \
    --bootstrap-server "$DG_BOOTSTRAP_SERVER" \
    --command wait-for-topic \
    --topic "$topic"
}

function generate_data() {
#  --bootstrap-server <value e.g. localhost:29092>
#                           bootstrap server's url pointing to valid Kafka setup
#  --topic <value i.e SourceEvents>
#                           Kafka topic name
#  --start-date <datetime e.g. 2018-01-10T00:00:00>
#                           start date to start data generation from. Optional. Default value is [2019-07-31T15:35:13]
#  --end-date <datetime e.g. 2018-01-10T00:00:00>
#                           end date of message generation. Must be not less than start date. Optional. Default value is [2019-08-01T15:35:13]. However due to <preserve-order> generation can be finished before end date is reached.
#  --logs-dir <path>        path to directory with test data. Both jar resources path and local folder are accessible. Optional. Default value is [/seedDir.2]
#  --files-manifest <file>  path file where every line is a search pattern. Lines examples: [messages.2019-04-01*.txt] OR [*.log] OR [some_particular_file.log]. Optional. Default value is [<None>]. Absence is equal to [*] pattern
#  --interval <int seconds>
#                           pause between batches in seconds. Optional. Default value is [1]. With <noDelay> it influence on log timestamps only. Without <no-nelay> it also determines actual generation speed.
#  --batch-size <int>       number of messages per interval. Optional. Default value is [1000]
#  --partner-id <value>     Comma separated set of partner id. Optional. Default value is testPartner
#  --customer-id <value>    Comma separated set of customer id. Optional. Default value is testCustomer
#  --preserve-order <true|false>
#                           if [true] lines order is preserved and logs will be populated ont more than once. Optional. Default value is [true]
#  --no-delay <true|false>  if true generator will push messages to Kafka as quickly as possible. Optional. Default value is [true]
#  --use-avro <true|false>  if true generator will push messages to Kafka in avro format. Optional. Default value is [false]
#  --delay <value>
#  --count <value>

  local folder=$1
  local date_part=$2

  java $JAVA_OPTS -cp "$DATAGEN_JAR" com.fourv.datagen.streaming.DataGenerator \
    --partner-id "$DG_PARTNER_ID" \
    --customer-id "$DG_CUSTOMER_ID" \
    --bootstrap-server "$DG_BOOTSTRAP_SERVER" \
    --topic "$DG_TOPIC" \
    --use-avro "$DG_USE_AVRO" \
    --logs-dir "$folder" \
    --batch-size "${DG_BATCH_SIZE}" \
    --interval "${DG_INTERVAL}" \
    --start-date "${date_part:2}T00:00:00" \
    --end-date "${date_part:2}T23:59:59" \
    --preserve-order "${DG_PRESERVE_ORDER}" \
    --no-delay "${DG_NO_DELAY}" \
    --delay "${DG_DELAY}"
}

mute_log4j_for_avro
wait_for_kafka_topic "SourceEvents"
wait_for_kafka_topic "UnknownEvents"

echo "#### Unarchive"
# files with specifically formatted names are expected
# like messages.2019-07-19T000201Z.tar.gz
for FILE in ./messages.????-??-??T??????Z.tar.gz; do
  printf '.'
  tar -zxf "$FILE"

  # extract date from filename
  SUBFOLDER="./${FILE:11:10}"
  if [[ ! -a "$SUBFOLDER" ]]; then
    mkdir "$SUBFOLDER"
  fi

  AVRO_FILE="$SUBFOLDER/${FILE%.tar.gz}.avro"
  mv messages.1.avro "$AVRO_FILE"
  printf "%s " "$AVRO_FILE" >> "./$SUBFOLDER/files.txt"
done
echo

JQ_REPLACEMENT=$(cat <<"EOF"
import fileinput, json

for line in fileinput.input():
  print json.loads(line)['payload']
EOF
)

for FOLDER in ./20??-??-??; do
  echo "#### $FOLDER"
  FILES=$(cat "$FOLDER/files.txt")

  # it is faster to concat and extract once than extract every single file
  echo "  concat"
  java $LOG_4J_PROPS $JAVA_OPTS -jar $AVRO_TOOLS concat $FILES "$FOLDER/messages.avro"

  echo "  extract"
  java $LOG_4J_PROPS $JAVA_OPTS -jar $AVRO_TOOLS tojson "$FOLDER/messages.avro" | \
    python -c "$JQ_REPLACEMENT" | \
    cut -d ' ' -f 3- > "$FOLDER/messages.csv"

  rm "$FOLDER"/.*crc
  rm "$FOLDER"/*.avro
  rm "$FOLDER"/*.txt

  # generate some data from seed (from jar resources)
  generate_data "/seedDir.2" "$FOLDER"

#  echo "#### logs count: $(wc -l "$FOLDER/messages.csv")"
  generate_data "$FOLDER" "$FOLDER"

  rm "$FOLDER"/*.csv
done

echo Done
date

echo  going to sleep...
/usr/bin/tail -f /dev/null
