'''
  S3 utils.
'''
import os
import sys
from boto.s3.connection import S3Connection
from boto.exception import NoAuthHandlerFound

def create_s3_client():
    '''
    Creates S3 client.
    '''
    aws_key = os.getenv("AWS_ACCESS_KEY_ID") # needed to access S3 Sample Data
    aws_secret = os.getenv("AWS_SECRET_ACCESS_KEY")

    #print 'aws key: {}'.format(aws_key)
    #print 'aws secret: {}'.format(aws_secret)

    client = None
    try:
        client = S3Connection(aws_key, aws_secret)
    except NoAuthHandlerFound:
        print 'Failed to create S3 connection.'
    return client

def list_keys(bucket, prefix):
    '''
    List keys for bucket that start with prefix.

    :param bucket: S3 bucket
    :type bucket: bucket
    :param prefix: Key prefix
    :type prefix: string

    :returns: list of keys that start with prefix
    '''
    keys = list(bucket.list(prefix=prefix))
    return [key.name.encode('utf-8') for key in keys]

def filter_by_date(keys, dates):
    '''
    Filter keys that contains dates.

    :parm keys: List of S3 keys to filter
    :type keys: list_keys
    :param dates: List of dates to check in keys
    :type dates: list

    :returns: list of keys that contains specified days in their names
    '''
    return [key for date in dates for key in keys if date in key]

def delete_keys(bucket, prefix, dates):
    '''
    Delete keys that contain specific dates in names

    :param bucket: S3 bucket
    :type bucket: bucket
    :param prefix: Key prefix
    :type prefix: string
    :param dates: List of dates
    :type dates: list
    '''
    keys = list_keys(bucket, prefix)
    filtered = filter_by_date(keys, dates)

    bucket.delete_keys(filtered)
    print "Deleted keys: bucket={}, prefix={}, dates={}.".format(bucket, prefix, dates)


def delete_data(client, provider_bucket, tenant_path, dates):
    '''
    Delete S3 bucket keys for provider, tenant, dates.

    :param client: S3 client
    :type client: S3Connection
    :param provider_bucket: Provider s3 buckets
    :type provider_bucket: string
    :param tenant_path: Tenant path
    :type tenant_path: string
    :param dates: List of dates to delete
    :type dates: List
    '''
    print 'Provider bucket: {}.'.format(provider_bucket)
    print 'Tenant path: {}.'.format(tenant_path)

    bucket = client.lookup(provider_bucket)
    if bucket is None:
        print "Can't lookup s3 bucket: {}. Check your credentials.".format(provider_bucket)
        sys.exit(1)

    delete_keys(bucket, tenant_path + '/gs-data/master-shred/', dates)
    delete_keys(bucket, tenant_path + '/factstore/master-shred/', dates)
    delete_keys(bucket, tenant_path + '/fact-staging/', dates)
