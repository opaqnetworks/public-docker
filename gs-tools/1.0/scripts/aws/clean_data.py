#!/usr/bin/env python
'''
  Clean S3 buckets for provider, tenant, date range.
'''
import argparse
import sys

import date_utils as du
import s3_utils as s3

def main():
    '''
      Clean S3 buckets for provider, tenant, date range.
    '''
    parser = argparse.ArgumentParser(description='Clean S3 Buckets')
    parser.add_argument('--provider_bucket', required=True, help='Provider bucket')
    parser.add_argument('--tenant_path', required=True, help='Tenant')
    parser.add_argument('--startdate', required=True, type=du.valid_date, help='Start date')
    parser.add_argument('--enddate', required=True, type=du.valid_date, help='End date')

    args = parser.parse_args()
    provider_bucket = args.provider_bucket
    tenant_path = args.tenant_path
    startdate = args.startdate
    enddate = args.enddate

    if startdate > enddate:
        print "Error: start date > end date"
        sys.exit(1)

    dates = du.dates(startdate, enddate)

    client = s3.create_s3_client()
    if client is None:
        print 'Failed to access AWS. Check your credentials.'
        sys.exit(1)
    print 'Created S3 client.'

    s3.delete_data(client, provider_bucket, tenant_path, dates)
    print 'Deleted S3 data.'
    print 'Done.'

if __name__ == '__main__':
    main()
