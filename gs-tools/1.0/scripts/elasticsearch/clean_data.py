#!/usr/bin/env python
'''
  Clean Elasticsearch indices for provider, tenant, date range.
'''
import argparse
import sys

import date_utils as du
import es_utils as es

def main():
    '''
      Clean Elasticsearch indices for provider, tenant, date range.
    '''
    parser = argparse.ArgumentParser(description='Clean ES indices')
    parser.add_argument('--host', required=True, help='Elasticsearch host')
    parser.add_argument('--port', type=int, required=True, help='Elasticsearch port')
    parser.add_argument('--username', required=True, help='Elasticsearch user')
    parser.add_argument('--password', required=True, help='Elasticsearch password')
    parser.add_argument('--provider', required=True, help='Provider name')
    parser.add_argument('--tenant', required=True, help='Tenant name')
    parser.add_argument('--startdate', required=True, type=du.valid_date, help='Start date')
    parser.add_argument('--enddate', required=True, type=du.valid_date, help='End date')

    args = parser.parse_args()
    host = args.host
    port = args.port
    user = args.username
    password = args.password
    provider = args.provider
    tenant = args.tenant
    startdate = args.startdate
    enddate = args.enddate

    if startdate > enddate:
        print "Error: start date > end date"
        sys.exit(1)

    client = es.create_es_client(host, port, user, password)
    print 'Created elasticsearch client: host={}, port={}'.format(host, port)

    dates = du.dates(startdate, enddate)

    indices = list(es.index_names(provider, tenant, dates))
    print 'Indices to delete: {}'.format(indices)

    es.delete_indices(client, indices)
    print 'Done'

if __name__ == '__main__':
    main()
