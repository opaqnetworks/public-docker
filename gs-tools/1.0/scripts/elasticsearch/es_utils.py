'''
  Elasticsearch utils.
'''

from elasticsearch import Elasticsearch
from elasticsearch.exceptions import ConnectionError

def create_es_client(host, port, user, password):
    '''
      Create Elasticsearch client.

      :param host: elasticsearch host
      :type host: string
      :param port: elasticsearch port
      :type port: int
      :param user: elasticsearch user
      :type user: string
      :param password: string
      :type password: string

      :returns: Elasticsearch client
    '''
    hosts = ['http://{}:{}@{}:{}/'.format(user, password, host, port)]
    return Elasticsearch(hosts=hosts)


def es_index(provider, tenant, date):
    '''
      Create index name for provider, tenant, date.

      :param provider: Provider name
      :type provider: string
      :param tenant: Tenant name
      :type tenant: string
      :param date: Date
      :type date: string

      :returns: index name
    '''
    return "events-{}-{}-{}".format(provider, tenant, date)

def index_names(provider, tenant, dates):
    '''
      Create index names for provider, tenant and date range.

       :param provider: Provider name
       :type provider: string
       :param tenant: Tenant name
       :type tenant: string
       :param dates: List of dates
       :type dates: list

       :returns: a generator of index names for specific dates
    '''
    for date in dates:
        yield es_index(provider, tenant, date)

def create_indices(client, names):
    '''
       Create indices on ES.

       :param client: Elasticsearch client
       :type client: Elasticsearch
       :param names: Index names
       :type names: list
    '''
    for name in names:
        try:
            client.indices.create(index=name)
            print 'Created index={}'.format(name)
        except ConnectionError:
            print "Failed to establish connection to ES"


def delete_indices(client, names):
    '''
      Delete indices from ES.

      :param client: Elasticsearch client
      :type client: Elasticsearch
      :param names: Index names
      :type names: list
    '''
    for name in names:
        try:
            client.indices.delete(index=name, ignore=[400, 404])
            print 'Deleted index={}'.format(name)
        except ConnectionError:
            print "Failed to establish connection to ES"
