import logging
import os

DEFAULT_OVERLAY_CONFIG = {
    'PROVIDER': 'opaq',
    'S3_BUCKET': 'fourv-artifacts',
    'S3_KEY_PREFIX':'data-collector/release/overlay/overlay.',
    'SYNC_PERIOD': 30,
    'INTERNAL_API_URL': 'http://localhost:8080/api/v1.0',
    'INTERNAL_API_PATH': 'upload_overlay'
}


def load_config_from_env():
    logger = logging.getLogger(__name__)
    logger.info("Reading configuration from env vars...")

    if os.getenv("PROVIDER", None):
        logger.info('Setting provider to: %s', os.getenv("PROVIDER"))
        DEFAULT_OVERLAY_CONFIG['PROVIDER'] = os.getenv("PROVIDER")

    if os.getenv("S3_BUCKET", None):
        logger.info('Setting S3 bucket to: %s', os.getenv("S3_BUCKET"))
        DEFAULT_OVERLAY_CONFIG['S3_BUCKET'] = os.getenv("S3_BUCKET")

    if os.getenv("S3_KEY_PREFIX", None):
        logger.info('Setting S3 key prefix to: %s', os.getenv("S3_KEY_PREFIX"))
        DEFAULT_OVERLAY_CONFIG['S3_KEY_PREFIX'] = os.getenv("S3_KEY_PREFIX")

    if os.getenv("SYNC_PERIOD", None):
        DEFAULT_OVERLAY_CONFIG['SYNC_PERIOD'] = int(os.getenv("SYNC_PERIOD"))

    if os.getenv("INTERNAL_API_URL", None):
        DEFAULT_OVERLAY_CONFIG['INTERNAL_API_URL'] = os.getenv("INTERNAL_API_URL")

    return DEFAULT_OVERLAY_CONFIG
