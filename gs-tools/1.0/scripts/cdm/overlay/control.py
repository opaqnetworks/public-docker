import logging
import logging.config
import os
import re
import time

import boto
import boto.s3
from boto.s3.connection import OrdinaryCallingFormat

import utils


def start_periodic_import(conf):
    logger = logging.getLogger(__name__)
    logger.info("Importing overlay data from DCA...")

    while True:
        logger.info('Start Importing...')
        logger = logging.getLogger(__name__)
        client = boto.connect_s3(aws_access_key_id=os.getenv('AWS_ACCESS_KEY_ID'),
                                 aws_secret_access_key=os.getenv('AWS_SECRET_ACCESS_KEY'),
                                 calling_format=OrdinaryCallingFormat())

        bucket = client.lookup(conf['S3_BUCKET'])
        if bucket is None:
            logger.warn('Bucket %s does not exist. Skipping import from DCA... ', conf['S3_BUCKET'])
            break

        import_data(bucket=bucket, conf=conf)

        logger.info('Sleeping %ss until next import...\n', conf['SYNC_PERIOD'])
        time.sleep(conf['SYNC_PERIOD'])


def import_data(bucket, conf):
    logger = logging.getLogger(__name__)

    keys = ([key for key in bucket if re.search(conf['S3_KEY_PREFIX'], key.name)])

    if not keys:
        logger.warn("No data found in S3 bucket, skipping import...")

    for k in keys:
        parts = k.name.split('/')
        filename = parts[-1] if len(parts) > 0 else ""
        if '.' in filename:
            k.get_contents_to_filename(filename)
            utils.upload_file(filename=filename,
                              internal_api_url=conf['INTERNAL_API_URL'],
                              internal_api_path=conf['INTERNAL_API_PATH'],
                              provider=conf['PROVIDER'])


def setup_logging():
    logging.config.dictConfig({
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
            },
        },
        'handlers': {
            'console': {
                'level': 'INFO',
                'class': 'logging.StreamHandler',
                'formatter': 'standard',
                "stream": "ext://sys.stdout"
            },
        },
        'loggers': {
            '': {
                'handlers': ['console'],
                'level': 'INFO',
                'propagate': True
            }
        },
        'root': {
            'level': 'INFO',
            'handlers': ['console']
        }
    })
