#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import logging.config

import config
import control


def main():
    control.setup_logging()

    logger = logging.getLogger(__name__)
    logger.info("Starting application...")

    conf = config.load_config_from_env()
    logger.info("Application started successfully.")

    control.start_periodic_import(conf)
    logger.info("Application completed.")


if __name__ == '__main__':
    main()
