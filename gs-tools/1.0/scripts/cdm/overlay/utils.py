import logging
import os
import time

import requests


def upload_file(filename, internal_api_url, internal_api_path, provider):
    """
    Upload file to internal API
    :param filename: file name to upload to internal api
    :param internal_api_url: internal api url
    :param internal_api_path: API path
    :return:
    """
    logger = logging.getLogger(__name__)

    logger.info('Uploading file %s to internal api ', filename)
    url = '{}/cdm/{}'.format(internal_api_url, internal_api_path)
    logger.info('Url: %s', url)

    files = {'file': open(filename, 'rb')}
    max_tries = 10
    delay = 5

    for n in range(max_tries):
        try:
            response = requests.post(url, files=files, data={'providerId': provider})
            if response.status_code == 200:
                logger.info("Successfully imported file")
            else:
                logger.warn("Import not successful")
            break
        except Exception as error:
            if n == max_tries - 1:
                logger.error('Failed to send request to internal api: %s', error.message, exc_info=True)
            else:
                time.sleep(delay)
                logger.warn('Failed contacting internal api, retrying in %s seconds...', delay)
    os.remove(filename)
