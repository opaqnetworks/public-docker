#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import logging.config

import config
import control


def main():
    control.setup_logging()

    logger = logging.getLogger(__name__)
    logger.info("Starting application...")

    config.load_config_from_env()
    logger.info("Application started successfully.")

    control.start_periodic_import(provider=config.DEFAULT_IMPORTER_CONFIG['provider'],
                                  hostname=config.DEFAULT_IMPORTER_CONFIG['pg_host'],
                                  username=config.DEFAULT_IMPORTER_CONFIG['pg_username'],
                                  password=config.DEFAULT_IMPORTER_CONFIG['pg_password'],
                                  database=config.DEFAULT_IMPORTER_CONFIG['pg_database'],
                                  bucket_name=config.DEFAULT_IMPORTER_CONFIG['s3_bucket'],
                                  sync_period=config.DEFAULT_IMPORTER_CONFIG['sync_period'],
                                  internal_api_url=config.DEFAULT_IMPORTER_CONFIG['internal_api_config_url'])
    logger.info("Application completed.")


if __name__ == '__main__':
    main()
