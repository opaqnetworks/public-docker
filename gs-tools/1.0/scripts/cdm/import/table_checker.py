import logging

import psycopg2

COMPLIANCE_SQL = "select " \
                 "* " \
                 "from security_compliance " \
                 "where provider_id=%s and tenant_id=%s"

COVERAGE_SQL = "select " \
               "* " \
               "from cyber_defense_object " \
               "where provider_id=%s and tenant_id=%s"

NETWORKS_SQL = "select " \
               "* " \
               "from network " \
               "where provider_id=%s and tenant_id=%s"


def check(provider, tenant, hostname, username, password, database, query):
    """
    Connects to database check if tables are empty
    """
    logger = logging.getLogger(__name__)
    connection = None
    cursor = None
    try:
        connection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database)
        cursor = connection.cursor()
        cursor.execute(query, (provider, tenant))
        rows_count = cursor.rowcount
        return rows_count
    except Exception as error:
        logger.error('Failed to import data: %s', error.message, exc_info=True)
        return -1
    finally:
        if cursor is not None:
            cursor.close()
        if connection is not None:
            connection.close()
