import logging
import logging.config
import os
import re
import time
from functools import partial

import boto
import boto.s3
from boto.s3.connection import OrdinaryCallingFormat

import config
import importer_util
import table_checker


def start_periodic_import(provider, hostname, username, password, database, bucket_name, sync_period, internal_api_url):
    """
    Starts periodic import (compliance data)
    """
    logger = logging.getLogger(__name__)
    logger.info("Importing compliance from DCA...")

    compliance_table_checker, compliance_importer = partials_pair(hostname=hostname,
                                                                  username=username,
                                                                  password=password,
                                                                  database=database,
                                                                  query=table_checker.COMPLIANCE_SQL,
                                                                  filename=config.COMPLIANCE_FILENAME,
                                                                  provider=provider,
                                                                  internal_api_url=internal_api_url)

    while True:
        logger.info('Start Importing...')
        logger = logging.getLogger(__name__)
        client = boto.connect_s3(aws_access_key_id=os.getenv('AWS_ACCESS_KEY_ID'),
                                 aws_secret_access_key=os.getenv('AWS_SECRET_ACCESS_KEY'),
                                 calling_format=OrdinaryCallingFormat())

        bucket = client.lookup(bucket_name)
        if bucket is None:
            logger.warn('Bucket %s does not exist. Skipping import from DCA... ', bucket_name)
        else:
            import_data(provider=provider, bucket=bucket, regexp_search="/compliance.txt",
                        importer=compliance_importer, api_path="upload_compliance",
                        check_table=compliance_table_checker, filename=config.COMPLIANCE_FILENAME)

        logger.info('Sleeping %ss until next import...\n', sync_period)
        time.sleep(sync_period)


def start_onetime_import(provider, hostname, username, password, database, bucket_name, internal_api_url):
    """
    Starts one time import (network and coverage data)
    """
    logger = logging.getLogger(__name__)
    logger.info("Importing network, coverage from DCA...")

    coverage_table_checker, coverage_importer = partials_pair(hostname=hostname,
                                                              username=username,
                                                              password=password,
                                                              database=database,
                                                              query=table_checker.COVERAGE_SQL,
                                                              filename=config.COVERAGE_FILENAME,
                                                              internal_api_url=internal_api_url,
                                                              provider=provider)

    networks_table_checker, networks_importer = partials_pair(hostname=hostname,
                                                              username=username,
                                                              password=password,
                                                              database=database,
                                                              query=table_checker.NETWORKS_SQL,
                                                              filename=config.NETWORKS_FILENAME,
                                                              internal_api_url=internal_api_url,
                                                              provider=provider)
    client = boto.connect_s3(aws_access_key_id=os.getenv('AWS_ACCESS_KEY_ID'),
                             aws_secret_access_key=os.getenv('AWS_SECRET_ACCESS_KEY'),
                             calling_format=OrdinaryCallingFormat())
    bucket = client.lookup(bucket_name)
    if bucket is None:
        logger.warn('Bucket %s does not exist. Skipping import from DCA... ', bucket_name)
    else:
        logger.info('Start importing network data...')
        import_data(provider=provider, bucket=bucket, regexp_search="/network.txt",
                    importer=networks_importer, api_path="upload_network",
                    check_table=networks_table_checker, filename=config.NETWORKS_FILENAME)
        logger.info('End importing network data')

        logger.info('Start importing coverage data...')
        import_data(provider=provider, bucket=bucket, regexp_search="/coverage.txt",
                    importer=coverage_importer, api_path="upload_coverage",
                    check_table=coverage_table_checker, filename=config.COVERAGE_FILENAME)
        logger.info('End importing coverage')


def import_data(provider, bucket, regexp_search, importer, api_path, check_table, filename):
    """
    Checks if there is data in table to decide if the import should happen
    """
    logger = logging.getLogger(__name__)
    founded_files = ([key for key
                      in bucket
                      if re.search(regexp_search, key.name)])
    if not founded_files:
        logger.warn("No data found in S3 bucket, skipping import...")
    for founded_file in founded_files:
        key = founded_file.name
        key_parts = key.split('/')
        if len(key_parts) < 3:
            logger.warn('No tenant found for file %s', key)
            continue
        tenant_name = key_parts[0]
        if tenant_name:
            table_row_count = check_table(provider=provider, tenant=tenant_name)
            if table_row_count == 0:
                file_key = bucket.get_key(key)
                file_key.get_contents_to_filename(filename)
                importer(api_path=api_path)
            else:
                logger.warn("Data exists in table or something went wrong during db connection, skipping import... ")


def partials_pair(hostname, username, password, database, query, filename, internal_api_url, provider):
    """
    Utility to instantiate check_db and importer (either for network or coverage)
    """
    check_db = partial(table_checker.check,
                       hostname=hostname,
                       username=username,
                       password=password,
                       database=database,
                       query=query)
    importer = partial(importer_util.import_data,
                       filename=filename,
                       internal_api_url=internal_api_url,
                       provider=provider)
    return check_db, importer


def setup_logging():
    logging.config.dictConfig({
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
            },
        },
        'handlers': {
            'console': {
                'level': 'INFO',
                'class': 'logging.StreamHandler',
                'formatter': 'standard',
                "stream": "ext://sys.stdout"
            },
        },
        'loggers': {
            '': {
                'handlers': ['console'],
                'level': 'INFO',
                'propagate': True
            }
        },
        'root': {
            'level': 'INFO',
            'handlers': ['console']
        }
    })
