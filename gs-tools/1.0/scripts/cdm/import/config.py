import logging
import os

COMPLIANCE_FILENAME = "compliance.txt"

COVERAGE_FILENAME = "coverage.txt"

NETWORKS_FILENAME = 'network.txt'

DEFAULT_IMPORTER_CONFIG = {
    'provider': 'andriyl',
    'pg_host': 'localhost',
    'pg_username': 'postgres',
    'pg_password': 'postgres',
    'pg_database': 'test',
    's3_bucket': 'spark-job-test',
    'sync_period': 30,
    'internal_api_config_url': 'http://localhost:8080/api/v1.0'
}


def load_config_from_env():
    logger = logging.getLogger(__name__)
    logger.info("Reading configuration from env vars...")
    if os.getenv("POSTGRES_HOST", None):
        DEFAULT_IMPORTER_CONFIG['pg_host'] = os.getenv("POSTGRES_HOST")

    if os.getenv("POSTGRES_USERNAME", None):
        DEFAULT_IMPORTER_CONFIG['pg_username'] = os.getenv("POSTGRES_USERNAME")

    if os.getenv("POSTGRES_PASSWORD", None):
        DEFAULT_IMPORTER_CONFIG['pg_password'] = os.getenv("POSTGRES_PASSWORD")

    if os.getenv("POSTGRES_DATABASE", None):
        DEFAULT_IMPORTER_CONFIG['pg_database'] = os.getenv("POSTGRES_DATABASE")

    if os.getenv("PROVIDER", None):
        DEFAULT_IMPORTER_CONFIG['provider'] = os.getenv("PROVIDER")
        DEFAULT_IMPORTER_CONFIG['s3_bucket'] = os.getenv("PROVIDER")

    if os.getenv("S3_BUCKET", None):
        logger.info('Setting S3 bucket to: %s', os.getenv("S3_BUCKET"))
        DEFAULT_IMPORTER_CONFIG['s3_bucket'] = os.getenv("S3_BUCKET")

    if os.getenv("SYNC_PERIOD", None):
        DEFAULT_IMPORTER_CONFIG['sync_period'] = int(os.getenv("SYNC_PERIOD"))

    if os.getenv("INTERNAL_API_CONFIG_URL", None):
        DEFAULT_IMPORTER_CONFIG['internal_api_config_url'] = os.getenv("INTERNAL_API_CONFIG_URL")
