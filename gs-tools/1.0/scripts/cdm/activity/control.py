import logging
import os
import pickle
import re
import time

import boto
import boto.s3
import psycopg2
import requests
from boto.s3.connection import OrdinaryCallingFormat
from boto.s3.key import Key

CDM_ACTIVITY_PATTERN = 'ds-activity'


def start_control_loop(provider, hostname, username, password, database, internal_api_url, bucket_name, sync_period):
    logger = logging.getLogger(__name__)
    logger.info('Starting control loop...\n')

    while True:

        try:
            conn = boto.connect_s3(aws_access_key_id=os.getenv('AWS_ACCESS_KEY_ID'),
                                   aws_secret_access_key=os.getenv('AWS_SECRET_ACCESS_KEY'),
                                   calling_format=OrdinaryCallingFormat())
            bucket = conn.lookup(bucket_name=bucket_name)
            if bucket is None:
                logger.warn('Bucket %s does not exist. Skipping import from DCA... ', bucket_name)
            else:
                logger.info('Checking for activity files...')
                objects = {key.name: key.etag for key
                           in bucket
                           if re.search(CDM_ACTIVITY_PATTERN, key.name)}

                if objects:
                    logger.info('Found activity files: %s', len(objects))

                    index_file = 'activity.index'
                    index_key_name = os.path.join('activation', index_file)

                    logger.info('Checking for index...')
                    index_key = bucket.get_key(index_key_name)
                    if index_key:
                        logger.info('Index found. Downloading ...')
                        index_key.get_contents_to_filename(index_file)

                        with open(index_file, 'rb') as f:
                            index = pickle.load(f)
                        logger.info('Index entries: %s', len(index))

                        logger.info('Checking for updated activity files in index...')
                        modified = {key: etag for key, etag
                                    in objects.items()
                                    if not index.has_key(key)
                                    or index[key] != etag}
                        logger.info('Found updated activity files: %s', len(modified))
                    else:
                        logger.info('Index not found.')
                        modified = objects  # no index, set to all

                    if len(modified) > 0:
                        existing_tenants = get_all_tenants(provider=provider, hostname=hostname, username=username,
                                                           password=password, database=database)
                        for tenant_id in existing_tenants:
                            filtered_items = {k: v for k, v in modified.iteritems() if
                                              re.search(tenant_id[0] + "/activity", k)}
                            if filtered_items:
                                sorted_by_timestamp = sorted(filtered_items.items(), key=lambda x: x[0], reverse=True)
                                latest_activity = sorted_by_timestamp[0] if sorted_by_timestamp else None

                                latest_key_name = latest_activity[0]
                                latest_filename = latest_key_name.split('/')[-1]
                                latest_key = bucket.get_key(latest_key_name)

                                logger.info('Downloading latest activity file: %s...', latest_filename)
                                latest_key.get_contents_to_filename(latest_filename)

                                logger.info('Uploading latest activity file to Greyspark...')
                                url = '{}/cdm/upload_activity'.format(internal_api_url)
                                logger.info('URL: %s', url)

                                files = {'file': open(latest_filename, 'rb')}
                                max_tries = 10
                                for n in range(max_tries):
                                    try:
                                        response = requests.post(url, files=files, data={'providerId': provider})
                                        if response.status_code == 200:
                                            logger.info("Successfully imported file")
                                        else:
                                            logger.warn("Import not successful")
                                        break
                                    except Exception as error:
                                        if n == max_tries - 1:
                                            logger.error('Failed to send request to internal api: %s', error.message,
                                                         exc_info=True)
                                        else:
                                            logger.warn('Failed contacting internal api, retrying in some seconds...')
                                            time.sleep(2)
                                os.remove(latest_filename)

                        if existing_tenants:
                            logger.info('Creating index...')
                            with open(index_file, 'wb') as f:
                                pickle.dump(objects, f, protocol=pickle.HIGHEST_PROTOCOL)

                            logger.info('Uploading index...')
                            key = Key(bucket)
                            key.key = index_key_name
                            key.set_contents_from_filename(index_file)

                        if os.path.exists(index_file):
                            os.remove(index_file)
                else:
                    logger.info('No activity files found.')

        except Exception as e:
            logger.error('Failed to import activity: %s', e.message, exc_info=True)

        logger.info('Sleeping %ss until next check...\n', sync_period)
        time.sleep(sync_period)


def get_all_tenants(provider, hostname, username, password, database):
    """
    Connects to database and gets all active tenant_ids
    """
    get_active_tenants_sql = "select " \
                             "tenant_id " \
                             "from tenant " \
                             "where provider_id=%s and active is True"
    logger = logging.getLogger(__name__)
    connection = None
    max_retries = 10
    for n in range(max_retries):
        try:
            connection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database)
            break
        except Exception as error:
            if n == max_retries - 1:
                logger.error('Error obtaining database connection: %s', error.message, exc_info=True)
                return []
            else:
                time.sleep(2)
                logger.warn('Failed contacting database, retrying in some seconds...')

    cursor = None
    try:
        cursor = connection.cursor()
        cursor.execute(get_active_tenants_sql, (provider,))
        return cursor.fetchall()
    except Exception as error:
        logger.error('Failed to get tenant data: %s', error.message, exc_info=True)
    finally:
        if cursor is not None:
            cursor.close()
        if connection is not None:
            connection.close()
