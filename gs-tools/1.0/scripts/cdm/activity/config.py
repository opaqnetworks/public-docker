DEFAULT_CONFIG = {
    'internal_api_url': 'http://localhost:8080/api/v1.0',
    'pg_host': 'localhost',
    'pg_username': 'postgres',
    'pg_password': 'postgres',
    'pg_database': 'test',
    'provider': 'integration.fourvdev.com',
    's3_bucket': 'integration.fourvdev.com',
    'sync_period': 30,
}
