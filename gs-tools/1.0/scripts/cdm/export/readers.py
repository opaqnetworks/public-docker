import psycopg2

COMPLIANCE_SQL = "select " \
                 "compliance_code, " \
                 "category, " \
                 "required, " \
                 "tenant_id " \
                 "from security_compliance " \
                 "where provider_id=%s"

COVERAGE_SQL = "select dd.domain," \
               "cdm.dca_val as category, " \
               "vendor, " \
               "name, " \
               "version, " \
               "date_implemented, " \
               "date_retired, " \
               "activity_override, " \
               "cdo.tenant_id " \
               "FROM cyber_defense_object cdo " \
               "JOIN cyber_defense_matrix cdm ON (cdo.category = cdm.code AND cdo.tenant_id = cdm.tenant_id " \
               "AND cdo.provider_id = cdm.provider_id) " \
               "JOIN deployed_domain dd ON (cdo.id = dd.object_id AND cdo.tenant_id = dd.tenant_id " \
               "AND cdo.provider_id = dd.provider_id) " \
               "WHERE cdo.provider_id=%s"

NETWORKS_SQL = "select " \
               "network.name, " \
               "subnet.cidr_val, " \
               "subnet.subnet_range_start, " \
               "subnet.subnet_range_end, " \
               "subnet.tenant_id " \
               "from network join subnet on (network.id = subnet.network_id) " \
               "where subnet.provider_id=%s and network.active"


def read_data(provider, hostname, username, password, database, query):
    connection = None
    cursor = None
    try:
        connection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database)
        cursor = connection.cursor()
        cursor.execute(query, (provider,))
        return cursor.fetchall()
    finally:
        if cursor is not None:
            cursor.close()
        if connection is not None:
            connection.close()
