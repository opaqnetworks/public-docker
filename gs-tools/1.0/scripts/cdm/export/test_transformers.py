import datetime
import unittest

import transformers


class TestTransformers(unittest.TestCase):
    def test_compliance__required_is_1_when_required_true(self):
        required = True
        result = transformers.compliance_record(['code1', 'cat1', required, 'tenant1'])

        required_idx = 3
        self.assertEqual(result[required_idx], 1)

    def test_compliance__required_is_0_when_required_false(self):
        required = False
        result = transformers.compliance_record(['code1', 'cat1', required, 'tenant1'])

        required_idx = 3
        self.assertEqual(result[required_idx], 0)

    def test_compliance_grouped_by_tenant(self):
        test = transformers.compliance_by_tenant([
            ['code11', 'cat11', True, 'tenant1'],
            ['code12', 'cat12', False, 'tenant1'],
            ['code13', 'cat13', True, 'tenant1'],
            ['code21', 'cat21', True, 'tenant2'],
            ['code22', 'cat22', False, 'tenant2'],
            ['code31', 'cat31', True, 'tenant3'],
            ['code32', 'cat32', False, 'tenant3'],

        ])

        # self.print_data(test)

        self.assertEqual(len(test['tenant1']), 3)
        self.assertEqual(len(test['tenant2']), 2)
        self.assertEqual(len(test['tenant3']), 2)

    def test_coverage__date_implemented_is_time_when_present(self):
        date_implemented = datetime.datetime(2006, 11, 21, 16, 30)
        result = transformers.coverage([
            ['domain1', 'cat1', 'vendor1', 'product1', 'ver1', date_implemented, None, True, 'tenant1']
        ])

        date_implemented_idx = 6
        self.assertEqual(result[0][date_implemented_idx], 1164126600)

    def test_coverage__date_implemented_is_empty_string_when_missed(self):
        date_implemented = None
        result = transformers.coverage([
            ['domain1', 'cat1', 'vendor1', 'product1', 'ver1', date_implemented, None, True, 'tenant1']
        ])

        date_implemented_idx = 6
        self.assertEqual(result[0][date_implemented_idx], '')

    def test_coverage__date_retired_is_time_when_present(self):
        date_retired = datetime.datetime(2006, 11, 21, 16, 30)
        result = transformers.coverage([
            ['domain1', 'cat1', 'vendor1', 'product1', 'ver1', None, date_retired, True, 'tenant1']
        ])

        date_retired_idx = 7
        self.assertEqual(result[0][date_retired_idx], 1164126600)

    def test_coverage__date_retired_is_empty_string_when_missed(self):
        date_retired = None
        result = transformers.coverage([
            ['domain1', 'cat1', 'vendor1', 'product1', 'ver1', None, date_retired, True, 'tenant1']
        ])

        date_retired_idx = 7
        self.assertEqual(result[0][date_retired_idx], '')

    def test_coverage__activity_override_is_1_when_activity_override_true(self):
        activity_override = True
        result = transformers.coverage([
            ['domain1', 'cat1', 'vendor1', 'product1', 'ver1', None, None, activity_override, 'tenant1']
        ])

        activity_override_idx = 8
        self.assertEqual(result[0][activity_override_idx], 1)

    def test_coverage__activity_override_is_0_when_activity_override_false(self):
        activity_override = False
        result = transformers.coverage([
            ['domain1', 'cat1', 'vendor1', 'product1', 'ver1', None, None, activity_override, 'tenant1']
        ])

        activity_override_idx = 8
        self.assertEqual(result[0][activity_override_idx], 0)

    def test_coverage_grouped_by_tenant(self):
        test = transformers.coverage_by_tenant([
            ['domain1', 'cat1', 'vendor1', 'product1', 'ver1', None, None, True, 'tenant1'],
            ['domain1', 'cat1', 'vendor1', 'product1', 'ver2', None, None, True, 'tenant1'],
            ['domain1', 'cat1', 'vendor1', 'product1', 'ver3', None, None, True, 'tenant1'],
            ['domain2', 'cat2', 'vendor2', 'product1', 'ver1', None, None, True, 'tenant2'],
            ['domain2', 'cat2', 'vendor2', 'product2', 'ver2', None, None, True, 'tenant2'],
            ['domain3', 'cat3', 'vendor3', 'product1', 'ver1', None, None, True, 'tenant3'],
            ['domain3', 'cat3', 'vendor3', 'product2', 'ver2', None, None, True, 'tenant3']
        ])

        # self.print_data(test)

        self.assertEqual(len(test['tenant1']), 3)
        self.assertEqual(len(test['tenant2']), 2)
        self.assertEqual(len(test['tenant3']), 2)

    def test_network__network_is_cidr_when_cidr_present(self):
        cidr = '192.168.1.0/24'
        result = transformers.networks_record(['net1', cidr, '192.168.1.0', '192.168.1.255', 'tenant1'])

        network_idx = 2
        self.assertEqual(result[network_idx], cidr)

    def test_network__network_is_range_when_cidr_missed(self):
        cidr = None
        range_start = '192.168.1.0'
        range_end = '192.168.1.255'
        result = transformers.networks_record(['net1', cidr, range_start, range_end, 'tenant1'])

        network_idx = 2
        self.assertEqual(result[network_idx], '{}-{}'.format(range_start, range_end))

    def test_network__grouped_by_tenant(self):
        test = transformers.networks_by_tenant([
            ['net11', '192.168.1.0/24', '192.168.1.0', '192.168.1.255', 'tenant1'],
            ['net11', None, '192.168.1.0', '192.168.1.255', 'tenant1'],
            ['net21', '192.168.1.0/24', '192.168.1.0', '192.168.1.255', 'tenant2'],
            ['net22', None, '192.168.1.0', '192.168.1.255', 'tenant2']
        ])

        # self.print_data(test)

        self.assertEqual(len(test['tenant1']), 2)
        self.assertEqual(len(test['tenant2']), 2)

    @staticmethod
    def print_data(data):
        for tenant, data in sorted(data.iteritems()):
            print "\nTenant: {}".format(tenant)
            for idx, val in enumerate(data):
                print "Row {} : {}".format(idx, val)


if __name__ == '__main__':
    unittest.main()
