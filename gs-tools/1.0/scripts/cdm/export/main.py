#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import logging.config
import os

import config
import control


def main():
    setup_logging()

    logger = logging.getLogger(__name__)
    logger.info("Starting application...")

    logger.info("Reading configuration from env vars...")
    if os.getenv("POSTGRES_HOST", None):
        config.DEFAULT_EXPORTER_CONFIG['pg_host'] = os.getenv("POSTGRES_HOST")

    if os.getenv("POSTGRES_USERNAME", None):
        config.DEFAULT_EXPORTER_CONFIG['pg_username'] = os.getenv("POSTGRES_USERNAME")

    if os.getenv("POSTGRES_PASSWORD", None):
        config.DEFAULT_EXPORTER_CONFIG['pg_password'] = os.getenv("POSTGRES_PASSWORD")

    if os.getenv("POSTGRES_DATABASE", None):
        config.DEFAULT_EXPORTER_CONFIG['pg_database'] = os.getenv("POSTGRES_DATABASE")

    if os.getenv("PROVIDER", None):
        config.DEFAULT_EXPORTER_CONFIG['provider'] = os.getenv("PROVIDER")
        logger.info('Setting S3 bucket to provider name: %s', os.getenv("PROVIDER"))
        config.DEFAULT_EXPORTER_CONFIG['s3_bucket'] = os.getenv("PROVIDER")

    if os.getenv("S3_BUCKET", None):
        logger.info('Setting S3 bucket to: %s', os.getenv("S3_BUCKET"))
        config.DEFAULT_EXPORTER_CONFIG['s3_bucket'] = os.getenv("S3_BUCKET")

    if os.getenv("SYNC_PERIOD", None):
        config.DEFAULT_EXPORTER_CONFIG['sync_period'] = int(os.getenv("SYNC_PERIOD"))

    logger.info("Application started successfully.")

    control.start_control_loop(provider=config.DEFAULT_EXPORTER_CONFIG['provider'],
                               hostname=config.DEFAULT_EXPORTER_CONFIG['pg_host'],
                               username=config.DEFAULT_EXPORTER_CONFIG['pg_username'],
                               password=config.DEFAULT_EXPORTER_CONFIG['pg_password'],
                               database=config.DEFAULT_EXPORTER_CONFIG['pg_database'],
                               bucket_name=config.DEFAULT_EXPORTER_CONFIG['s3_bucket'],
                               sync_period=config.DEFAULT_EXPORTER_CONFIG['sync_period'])
    logger.info("Application completed.")


def setup_logging():
    logging.config.dictConfig({
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
            },
        },
        'handlers': {
            'console': {
                'level': 'INFO',
                'class': 'logging.StreamHandler',
                'formatter': 'standard',
                "stream": "ext://sys.stdout"
            },
        },
        'loggers': {
            '': {
                'handlers': ['console'],
                'level': 'INFO',
                'propagate': True
            }
        },
        'root': {
            'level': 'INFO',
            'handlers': ['console']
        }
    })


if __name__ == '__main__':
    main()
