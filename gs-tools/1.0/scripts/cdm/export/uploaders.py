import logging
import os

import boto
import boto.s3
from boto.s3.key import Key
from boto.s3.connection import OrdinaryCallingFormat

import csv

COMPLIANCE_FILENAME = "gs-compliance.txt"
COMPLIANCE_HEADER = ['tenant', 'cdm', 'category', 'required']

COVERAGE_FILENAME = "gs-coverage.txt"
COVERAGE_HEADER = ['tenant', 'domain', 'category', 'vendor', 'product', 'version', 'date_implemented', 'date_retired',
                   'activity_override']

NETWORKS_FILENAME = 'gs-network.txt'
NETWORKS_HEADER = ['tenant', 'domain', 'network']


def upload_by_tenant(bucket_name, data, filename, header, key_prefix):
    logger = logging.getLogger(__name__)

    for tenant, data in sorted(data.iteritems()):
        try:
            logger.info("Uploading for tenant: %s", tenant)
            upload(bucket_name=bucket_name,
                   data=data,
                   filename=filename,
                   header=header,
                   key_prefix=os.path.join(tenant, key_prefix))
        except Exception as error:
            logger.error('Failed to upload data: %s', error.message, exc_info=True)


def upload(bucket_name, data, filename, header, key_prefix):
    logger = logging.getLogger(__name__)

    logger.info('Saving data to file: %s', filename)
    save_to_csv(data, filename=filename, header=header)
    logger.info('Data saved.')

    logger.info('Uploading file: %s to S3 bucket: %s', filename, bucket_name)
    key_name = os.path.join(key_prefix, filename)

    upload_to_s3(bucket_name=bucket_name, key_name=key_name, filename=filename)
    logger.info('File uploaded.')

    logger.info('Cleaning up...')
    os.remove(filename)


def save_to_csv(data, filename, header):
    with open(filename, 'w') as f:
        writer = csv.writer(f, delimiter=',')
        writer.writerow(header)
        for row in data:
            writer.writerow(row)


def upload_to_s3(bucket_name, key_name, filename):
    logger = logging.getLogger(__name__)
    client = boto.connect_s3(aws_access_key_id=os.getenv("AWS_ACCESS_KEY_ID"),
                             aws_secret_access_key=os.getenv("AWS_SECRET_ACCESS_KEY"),
                             calling_format=OrdinaryCallingFormat())

    logger.debug('Reading s3 bucket: %s', bucket_name)
    bucket = client.lookup(bucket_name)
    if bucket is None:
        logger.warn('Bucket %s does not exist. Creating it... ', bucket_name)
        bucket = client.create_bucket(bucket_name=bucket_name)

    logger.debug('Creating S3 key: %s', key_name)
    create_s3_key(bucket, key_name, filename)
    logger.debug('S3 Key created.')


def create_s3_key(bucket, key_name, file_name):
    key = Key(bucket)
    key.key = key_name
    key.set_contents_from_filename(file_name)
    return key
