import logging
import time
from functools import partial

import readers
import transformers
import uploaders


def start_control_loop(provider, hostname, username, password, database, bucket_name, sync_period):
    logger = logging.getLogger(__name__)
    logger.info("Starting control loop...")

    key_prefix = 'activity'

    compliance_reader, compliance_uploader = partials_pair(hostname=hostname,
                                                           username=username,
                                                           password=password,
                                                           database=database,
                                                           query=readers.COMPLIANCE_SQL,
                                                           bucket_name=bucket_name,
                                                           key_prefix=key_prefix,
                                                           filename=uploaders.COMPLIANCE_FILENAME,
                                                           header=uploaders.COMPLIANCE_HEADER,
                                                           by_tenant=True)

    coverage_reader, coverage_uploader = partials_pair(hostname=hostname,
                                                       username=username,
                                                       password=password,
                                                       database=database,
                                                       query=readers.COVERAGE_SQL,
                                                       bucket_name=bucket_name,
                                                       key_prefix=key_prefix,
                                                       filename=uploaders.COVERAGE_FILENAME,
                                                       header=uploaders.COVERAGE_HEADER,
                                                       by_tenant=True)

    networks_reader, networks_uploader = partials_pair(hostname=hostname,
                                                       username=username,
                                                       password=password,
                                                       database=database,
                                                       query=readers.NETWORKS_SQL,
                                                       bucket_name=bucket_name,
                                                       key_prefix=key_prefix,
                                                       filename=uploaders.NETWORKS_FILENAME,
                                                       header=uploaders.NETWORKS_HEADER,
                                                       by_tenant=True)

    while True:
        logger.info('Exporting compliance data...')
        export(provider=provider,
               reader=compliance_reader,
               transformer=transformers.compliance_by_tenant,
               uploader=compliance_uploader)

        logger.info('Exporting coverage data...')
        export(provider=provider,
               reader=coverage_reader,
               transformer=transformers.coverage_by_tenant,
               uploader=coverage_uploader)

        logger.info('Exporting networks data...')
        export(provider=provider,
               reader=networks_reader,
               transformer=transformers.networks_by_tenant,
               uploader=networks_uploader)

        logger.info('Sleeping %ss until next export...\n', sync_period)
        time.sleep(sync_period)


def export(provider, reader, transformer, uploader):
    logger = logging.getLogger(__name__)

    try:
        data = reader(provider=provider)
        if data:
            transformed = transformer(data=data)
            uploader(data=transformed)
            logger.info('Data exported.')
        else:
            logger.info('No data found. Skipping export.')
    except Exception as error:
        logger.error('Failed to export data: %s', error.message, exc_info=True)


def partials_pair(hostname,
                  username,
                  password,
                  database,
                  query,
                  bucket_name,
                  key_prefix,
                  filename,
                  header,
                  by_tenant=False):
    reader = partial(readers.read_data,
                     hostname=hostname,
                     username=username,
                     password=password,
                     database=database,
                     query=query)
    uploader = partial(uploaders.upload_by_tenant if by_tenant else uploaders.upload,
                       bucket_name=bucket_name,
                       key_prefix=key_prefix,
                       filename=filename,
                       header=header)
    return reader, uploader
