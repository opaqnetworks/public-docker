DEFAULT_EXPORTER_CONFIG = {
    'provider': 'default',
    'pg_host': 'gs-db',
    'pg_username': 'postgres',
    'pg_password': 'postgres',
    'pg_database': 'greyspark',
    's3_bucket': 'default',
    'sync_period': 30,
}
