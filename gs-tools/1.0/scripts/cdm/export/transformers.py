from datetime import datetime, timedelta, tzinfo
import logging
from collections import defaultdict


class UTC(tzinfo):
    """
    UTC timezone class to localize all datetimes from the DB to UTC time
    """

    def utcoffset(self, dt):
        return timedelta(0)

    def tzname(self, dt):
        return "UTC"

    def dst(self, dt):
        return timedelta(0)


utc = UTC()
epoch = datetime.utcfromtimestamp(0).replace(tzinfo=utc)


def unix_time_secs(dt):
    return (dt - epoch).total_seconds()


def unix_time_millis(dt):
    return (dt - epoch).total_seconds() * 1000


def compliance(data):
    return [compliance_record(d) for d in data]


def compliance_record(record):
    logger = logging.getLogger(__name__)
    # tenant, cdm, category, required
    result = []
    if len(record) == 4:
        (code, category, required, tenant) = record
        result = [
            tenant,
            code,
            category,
            1 if required else 0
        ]
    else:
        logger.warn('Bad record: %s', record)
    return result


def compliance_by_tenant(data):
    logger = logging.getLogger(__name__)
    # tenant, cdm, category, required
    data_by_tenant = defaultdict(list)
    for d in data:
        if len(d) == 4:
            (code, category, required, tenant) = d
            data_by_tenant[tenant].append([
                tenant,
                code,
                category,
                1 if required else 0
            ])
        else:
            logger.warn('Bad record: %s', d)
    return data_by_tenant


def coverage(data):
    logger = logging.getLogger(__name__)
    # tenant, domain, category, vendor, product, version, date_implemented, date_retired, activity_override
    result = []
    for d in data:
        if len(d) == 9:
            (domain, category, vendor, product, version, date_implemented, date_retired, activity_override, tenant) = d
            result.append([
                tenant,
                domain,
                category,
                vendor,
                product,
                version,
                int(unix_time_secs(date_implemented.replace(tzinfo=utc))) if isinstance(date_implemented, datetime) else "",
                int(unix_time_secs(date_retired.replace(tzinfo=utc))) if isinstance(date_retired, datetime) else "",
                1 if activity_override else 0
            ])
        else:
            logger.warn('Bad record: %s', d)
    return result


def coverage_by_tenant(data):
    logger = logging.getLogger(__name__)
    # tenant, domain, category, vendor, product, version, date_implemented, date_retired, activity_override
    data_by_tenant = defaultdict(list)
    for d in data:
        if len(d) == 9:
            (domain, category, vendor, product, version, date_implemented, date_retired, activity_override, tenant) = d
            data_by_tenant[tenant].append([
                tenant,
                domain,
                category,
                vendor,
                product,
                version,
                int(unix_time_secs(date_implemented.replace(tzinfo=utc))) if isinstance(date_implemented, datetime) else "",
                int(unix_time_secs(date_retired.replace(tzinfo=utc))) if isinstance(date_retired, datetime) else "",
                1 if activity_override else 0
            ])
        else:
            logger.warn('Bad record: %s', d)
    return data_by_tenant


def networks(data):
    return [networks_record(d) for d in data]


def networks_record(record):
    logger = logging.getLogger(__name__)
    # tenant, domain, network
    result = []
    if len(record) == 5:
        (network_name, cidr, ip_start, ip_end, tenant) = record
        result = [
            tenant,
            network_name,
            cidr if cidr else ip_start + '-' + ip_end]
    else:
        logger.warn('Bad record: %s', record)
    return result


def networks_by_tenant(data):
    logger = logging.getLogger(__name__)
    data_by_tenant = defaultdict(list)
    # tenant, domain, network
    for d in data:
        if len(d) == 5:
            (network_name, cidr, ip_start, ip_end, tenant) = d
            data_by_tenant[tenant].append([
                tenant,
                network_name,
                cidr if cidr else ip_start + '-' + ip_end])
        else:
            logger.warn('Bad record: %s', d)
    return data_by_tenant
